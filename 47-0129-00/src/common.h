
#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdio.h>
#include <list>
#include <string.h>
#include <hhmpi/settings.h>

#include "cgivars.h"
#include "htmllib.h"

extern std::string current_tab;

#define ARRAY_SIZE(array)		(sizeof(array)/sizeof(const char *))

#define COMPANY_DESC			"Flight Data Systems"
#define DEVICE_DESC			"HHMPI"
#define MAIN_PAGE_TITLE			"Configuration"
#define LOGIN_PAGE_TITLE		"Login"
#define OPERATION_STATUS_PAGE_TITLE	"Operation Status"
#define SITE_TITLE			DEVICE_DESC
#define WIDGET_LEFT_PROPORTION		25
#define WIDGET_RIGHT_PROPORTION		75

#define SYSTEM_INFORMATION_TAB		"System Information"
#define DEVICE_CONFIGURATION_TAB	"Device Configuration"
#define BACKUP_RESTORE_TAB		"Backup/Restore"
#define FILE_BROWSER_TAB		"File Browser"
#define ADMIN_TAB			"Admin"
#define LICENCE_TAB			"Licence"
#define SERVICE_TAB			"Service"
#define REMOTE_TAB			"Remote"

#define REF_DOWNLOAD			"download"
#define REF_UPLOAD			"upload"
#define REF_FILENAME			"filename"
#define REF_PAGE			"page"
#define REF_ACTION			"action"
#define REF_ACCESS			"access"
#define REF_TAB				"tab/tab"
#define REF_CURRENT_TAB			"current_tab"
#define REF_USERNAME			"username"
#define REF_PASSWORD			"password"
#define REF_SET				"set"
#define REF_CHECKBOX		"checkbox"
#define REF_DELETE			"delete"
#define REF_FORMAT			"format"
#define REF_BUTTON			"button"
#define REF_SESSION_ID			"session_id"
#define REF_MOUNT_POINT			"mount_point"

#define BTN_ACTION_SAVE			"Save"
#define BTN_ACTION_APPLY		"Apply"
#define BTN_ACTION_CANCEL		"Cancel"
#define BTN_ACTION_RETURN		"Return"

#define BTN_ACCESS_LOGIN		"Login"
#define BTN_ACCESS_LOGOUT		"Logout"


#define UPLOAD_DIRECTORY		"/tmp"




#endif
