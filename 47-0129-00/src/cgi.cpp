
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>


#include "cgivars.h"
#include "htmllib.h"
#include "common.h"

#include "page/page.h"

#include "page/page_login.h"
#include "main_menu.h"
//#include "page/page_operation_status.h"


int main(void)
{
	MainPage *main_page = new MainPage(MAIN_PAGE_TITLE);
	Page *page = new Page("base");

	page->Register(&login_page);
	page->Register(main_page);
//	page->Register(&operation_status_page);

	char **postvars = NULL; /* POST request data repository */
	char **getvars = NULL; /* GET request data repository */
	int page_method; /* POST = 1, GET = 0 */  
	
	page_method = getRequestMethod();
	
	if (page_method == POST)
	{
		getvars = getGETvars();
		postvars = getPOSTvars();
	}
	else if (page_method == GET)
		getvars = getGETvars();
	
	page->Action(postvars, page_method);

	cleanUp(page_method, getvars, postvars);
	
	fflush(stdout);

	delete main_page;
	delete page;

	return 0;
}
