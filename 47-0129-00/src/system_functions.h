
#ifndef __SYSTEM_FUNCTIONS_H
#define __SYSTEM_FUNCTIONS_H

#include "common.h"


#define SETTINGS_FILE			"hhmpi.conf.bkp"
#define SETTINGS_PATH			"/tmp/" SETTINGS_FILE
#define INTERNAL_PATH			"/mnt/internal/test"
#define MEMINFO_PATH			"/proc/meminfo"
#define PARTITIONS_PATH			"/proc/partitions"
#define HOSTNAME_PATH			"/proc/sys/kernel/hostname"
#define MPI_PATH			"/proc/mpi"

class SystemFunctions
{
	public:
		SystemFunctions(void);

		static int UploadFlash(char *filename);
		static int UploadAircraft(char *filename);
		static int UploadSSLCertificate(char *filename);
		static int UploadSFTPKey(char *filename);
		static int UploadSFTPPrivateKey(const char *filename, const char* server);
		static int UploadSFTPPublicKey(const char *filename, const char* server);
		static int UploadCustomEncryptionKey(char *filename);
		static int DownloadFlash(void);
		static int DownloadAircraft(void);
		static int DownloadSSLCertificate(void);
		static int DownloadSFTPKey(void);
		static int DownloadCustomEncryptionKey(void);
		static int DownloadSecurityConfiguration(void);
		static int SignalGUI(void);
		static int SignalDaemon(void);
		static int SendFile(const char *source_path, const char *target);
		static int Copy(const char *source, const char *target);
		static bool GetTotalMemory(std::string &str);
		static bool GetAvailableMemory(std::string &str);
		static bool GetTotalInternalStorage(std::string &str);
		static bool GetAvailableInternalStorage(std::string &str);
		static bool GetHostname(std::string &str);
		static bool GetMPIVersion(std::string &str);
		static bool GetIPAddress(std::string &str);
		static bool ResetDevice(void);
		static bool FactoryDefaults(void);
		static void check_upload_directory(void);
		static string GetMAC(const char *if_name);
		static int InstallOEMType(const char *oem_name);
		static bool CheckMounted(string path);
		static bool Remount(string mount_point, bool read_only, bool remount = true);
		static void DeleteFiles(string mount_point, string path, list<string>& delete_list);
		static void Format(string mount_point);
		bool CheckV2Config(string file);

};

extern SystemFunctions system_functions;

#endif
