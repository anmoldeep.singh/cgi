
#include "section_hardware_options.h"
#include "widget_checkbox.h"

HardwareOptions::HardwareOptions(string menu_name, bool expanded) : Section("Hardware Options", menu_name, expanded)
{
	widgets.push_back(new CheckBox("FDS40-5100 - Wireless Ethernet Support (wifi)", &Settings::Instance(), "feature.wifi.present"));
	//widgets.push_back(new CheckBox("Audio Support", &Settings::Instance(), "feature.audio.present"));

	//widgets.push_back(new CheckBox("CVR Codec Support (Hardware Playback)", &Settings::Instance(), "feature.codec.present"));
	//widgets.push_back(new CheckBox("ATA Card Support", &Settings::Instance(), "feature.ata.present"));
	//widgets.push_back(new CheckBox("Battery Pack Support", &Settings::Instance(), "feature.battery.present"));
}

