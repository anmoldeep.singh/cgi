#include <hhmpi/settings.h>
#include <sys/wait.h>

#include "section_remote.h"
#include "widget_spacer.h"
#include "widget_image.h"
#include "system_functions.h"



#define REF_REMOTE		"remote"
#define BTN_REMOTE_RESET		"Reset"
#define BTN_REMOTE_DEFAULTS	"Defaults"


class RemoteImageMap : public Widget
{
        public:
                RemoteImageMap(string name)
			: Widget(name, NULL, "")
		{
		}

                void Show(void)
		{
			
			printf("<script type=\"text/javascript\" src=\"jquery.js\"></script>");
			printf("<script type=\"text/javascript\">");
			printf("function keypress(key_string) {");
			printf("$.ajax({ type: \"GET\", url: \"keypress.cgi\", data: \"key=\" + key_string});");
			printf("}");
			printf("</script>");

			printf("<tr>");
		        printf("<td class=\"fieldcell\" width=\"%d%%\" colspan=\"2\">", m_right_proportion);

			printf(" \
<div style=\"text-align:center; width:608px; margin-left:auto; margin-right:auto;\">	\
<table cellpadding=\"0\" cellspacing=\"0\"> \
<tr><td colspan=\"3\"><img id=\"hhmpi_top\" src=\"hhmpiv3_top.jpg\" border=\"0\" width=\"848\" height=\"141\" alt=\"\"></td></tr>	\
<tr >\
<td><img id=\"hhmpi_mid_left\" src=\"hhmpiv3_mid_left.jpg\" usemap=\"#hhmpi_mid_left\" border=\"0\" width=\"174\" height=\"800\" alt=\"\"></td>	\
<td><img id=\"hhmpi_mid_center\" src=\"framebuffer.cgi\" border=\"0\" width=\"480\" height=\"800\" alt=\"\"></td>	\
<td><img id=\"hhmpi_mid_right\" src=\"hhmpiv3_mid_right.jpg\" border=\"0\" width=\"194\" height=\"800\" alt=\"\"></td>	\
</tr> \
<tr><td colspan=\"3\"><img id=\"hhmpi_bottom\" src=\"hhmpiv3_bottom.jpg\" usemap=\"#hhmpi_bottom\" border=\"0\" width=\"848\" height=\"320\" alt=\"\"></td></tr>	\
</table>	\
<!--<map id=\"_hhmpi_mid_left\" name=\"hhmpi_mid_left\">	\
<area shape=\"rect\" coords=\"128,20,214,89\" onclick=\"keypress('1');\" title=\"Shortcut #1\">		\
<area shape=\"rect\" coords=\"128,98,214,167\" onclick=\"keypress('2');\" title=\"Shortcut #2\">	\
<area shape=\"rect\" coords=\"128,177,214,246\" onclick=\"keypress('3');\" title=\"Shortcut #3\">	\
<area shape=\"rect\" coords=\"128,256,214,314\" onclick=\"keypress('4');\" title=\"Shortcut #4\">	\
</map> -->	\
<map id=\"_hhmpi_bottom\" name=\"hhmpi_bottom\">	\
<area shape=\"rect\" coords=\"328,108,503,187\" onclick=\"keypress('ok');\" title=\"OK\">	\
<!--<area shape=\"rect\" coords=\"135,192,206,261\" onclick=\"keypress('cancel');\" title=\"Cancel\">-->	\
<area shape=\"poly\" coords=\"283,71,410,46,543,71,475,111,358,111\" onclick=\"keypress('up');\" title=\"Up\">	\
<area shape=\"poly\" coords=\"477,110,543,73,613,126,613,168,542,217,477,178,505,147\" onclick=\"keypress('right');\" title=\"Right\">	\
<area shape=\"poly\" coords=\"355,183,468,183,539,223,413,249,289,222\" onclick=\"keypress('down');\" title=\"Down\">	\
<area shape=\"poly\" coords=\"283,72,346,111,318,142,349,179,283,219,223,157,223,122\" onclick=\"keypress('left');\" title=\"Left\">	\
</map>	\
</div>	\
			");

	        	printf("</td>");
		        printf("</tr>");

	}

        protected:
                void ReadSetting(void) {};
                void ApplySetting(void) {};
};

Remote::Remote(string menu_name, bool expanded) : Section("Remote", menu_name, expanded)
{
	widgets.push_back(new Spacer());
	widgets.push_back(new RemoteImageMap("HHMPI"));
	widgets.push_back(new Spacer());
}

void Remote::Show(char **postvars)
{
	Section::Show(postvars);
}

