
#ifndef __SECTION_CUSTOM_ENCRYPTION_H
#define __SECTION_CUSTOM_ENCRYPTION_H

#include "section.h"

class CustomEncryptionKey : public Section
{
	public:
		CustomEncryptionKey(string menu_name, bool expanded);
		void Show(char **postvars);
		void Apply(char **postvars);
};

#endif