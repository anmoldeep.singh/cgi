
#include "section_access.h"
#include "widget_checkbox.h"
#include "widget_edit_passcode.h"
#include "widget_radio.h"
#include "widget_spacer.h"
#include "widget_static_text.h"
#include "widget_horizontal_line.h"
#include "access.h"


/*
// Original implementation
AccessRestrictions::AccessRestrictions(string menu_name, bool expanded) : Section("Access Restrictions", menu_name, expanded)
{
	widgets.push_back(new Spacer());
	widgets.push_back(new StaticTextBox("Access Level for non-privileged user", "") );
	widgets.push_back(new Radio("Level 0 - Unrestricted access", "access.level", "0") );
	widgets.push_back(new Radio("Level 1 - Restrict persistant settings", "access.level", "1") );
	widgets.push_back(new Radio("Level 2 - Restrict persistant settings and aircraft list modification", "access.level", "2") );
	widgets.push_back(new Radio("Level 3 - Restrict access to Device Configuration", "access.level", "3") );
	widgets.push_back(new Radio("Level 4 - Restrict all access other than shortcuts", "access.level", "4") );
	widgets.push_back(new Spacer());
}
*/

AccessRestrictions::AccessRestrictions(string menu_name, bool expanded) : Section("Access Restrictions", menu_name, expanded)
{
	widgets.push_back(new Spacer());
	widgets.push_back(new CheckBox("Restrict Menus", &Settings::Instance(), "security.restrict_menus"));
	widgets.push_back(new EditPasscode("Passcode", &Settings::Instance(), "security.menus_passcode") );
	widgets.push_back(new HLine());
	widgets.push_back(new CheckBox("Restrict Config", &Settings::Instance(), "security.restrict_config"));
	widgets.push_back(new EditPasscode("Passcode", &Settings::Instance(), "security.config_passcode") );
	widgets.push_back(new HLine());
	widgets.push_back(new CheckBox("Restrict Shortcuts", &Settings::Instance(), "security.restrict_shortcuts"));
	widgets.push_back(new EditPasscode("Passcode", &Settings::Instance(), "security.shortcuts_passcode") );
	widgets.push_back(new HLine());
	widgets.push_back(new CheckBox("Restrict Bootup", &Settings::Instance(), "security.restrict_bootup"));
	widgets.push_back(new EditPasscode("Passcode", &Settings::Instance(), "security.bootup_passcode") );
	widgets.push_back(new Spacer());
}
