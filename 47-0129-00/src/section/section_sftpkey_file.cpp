
#include "section_sftpkey_file.h"
#include "widget_spacer.h"
#include "widget_horizontal_line.h"
#include "widget_file_select.h"
#include "widget_button.h"
#include "widget_combobox.h"
#include "widget_html_code.h"
#include "system_functions.h"
#include "access.h"
#include "page.h"

#define BTN_DOWNLOAD_SFTP_KEY		"Download SFTP Key"
#define BTN_UPLOAD_PRIVATE_SFTP_KEY	"Upload Private Key"
#define BTN_UPLOAD_PUBLIC_SFTP_KEY	"Upload Public Key"
#define REF_COMBO_SERVER_SELECT		"last_choosen"



SFTPKeyFile::SFTPKeyFile(string menu_name, bool expanded) : Section("SFTP Key", menu_name, expanded)
{
	uint32_t server_count = 0;
	SettingsMap& settings_map = Settings::Instance().GetSettingsMap();
	list<string> id_list;
	string previous_name;
	SettingsMap::iterator it;
	for (it = settings_map.begin(); it != settings_map.end(); it++)
	{
		string identifier = it->first;
		string value = it->second;

		if (identifier.substr(0, 4) != "ftp.")
				continue;

		size_t pos = identifier.substr(4).find(".");

		if (pos == string::npos)
				continue;

		string this_name = identifier.substr(4, pos);

		if (this_name != previous_name)
		{
				previous_name = this_name;
				id_list.push_back(this_name);
		}
	}

	list<string>::iterator jt;

	for (jt = id_list.begin(); jt != id_list.end(); jt++)
	{
		string& identifier = *jt;

		update_server_entry_list[server_count] = new char[ identifier.length() + 1 ];
		snprintf(update_server_entry_list[server_count], identifier.length() + 1, "%s", identifier.c_str());
		server_count++;
	}
	dummy_settings.String(REF_COMBO_SERVER_SELECT,"");
	
	widgets.push_back(new Spacer());
	widgets.push_back(new ComboBox("FTP Server", &dummy_settings, REF_COMBO_SERVER_SELECT, (const char **) update_server_entry_list, server_count));
	widgets.push_back(new HLine());
	//widgets.push_back(new Button("Download SFTP Key File", BTN_DOWNLOAD_SFTP_KEY, REF_DOWNLOAD));
	//widgets.push_back(new HLine());
	widgets.push_back(new FileSelect("Select File", REF_FILENAME));
	widgets.push_back(new Button("Upload Private Key", BTN_UPLOAD_PRIVATE_SFTP_KEY, REF_UPLOAD));
	widgets.push_back(new Button("Upload Public Key", BTN_UPLOAD_PUBLIC_SFTP_KEY, REF_UPLOAD));
	widgets.push_back(new Spacer());
}


void SFTPKeyFile::Show(char **postvars)
{
	printf("</form>");
	printf("<form action=\"" APPNAME "\" name=\"sftpkey_upload\" method=\"post\" enctype=\"multipart/form-data\">");
	PostPersistantVariables();
	Section::Show(postvars);
	printf("</form>");
	printf("<form action=\"" APPNAME "\" method=\"post\">");
}


void SFTPKeyFile::Apply(char **postvars)
{
	bool download = false;
	bool upload_private = false;
	bool upload_public = false;
	char *filename = NULL;
	char* file_server = NULL;
	for (int i = 0; postvars[i]; i += 2)
	{
		if (strcmp(postvars[i], REF_DOWNLOAD) == 0)
		{		
			if (strcmp(postvars[i+1], BTN_DOWNLOAD_SFTP_KEY) == 0)
				download = true;
		}

		if (strcmp(postvars[i], REF_UPLOAD) == 0)
		{
			if (strcmp(postvars[i+1], BTN_UPLOAD_PRIVATE_SFTP_KEY) == 0)
			{
				upload_private = true;
			}
			else if (strcmp(postvars[i+1], BTN_UPLOAD_PUBLIC_SFTP_KEY) == 0)
			{
				upload_public = true;
			}
		}

		if (strcmp(postvars[i], REF_FILENAME) == 0)
			filename = postvars[i+1];
		
		if (strcmp(postvars[i], REF_COMBO_SERVER_SELECT) == 0)
		{
			file_server = postvars[i+1];
			dummy_settings.String(REF_COMBO_SERVER_SELECT,file_server);
		}
	}

	if(download)
	{
		system_functions.DownloadSFTPKey();
	}
	else if(upload_private)
	{
		if(system_functions.UploadSFTPPrivateKey(filename,file_server))
			printf("Success!<br>");
		else
			printf("Failed to upload key!<br>");
	}
	else if(upload_public)
	{
		if(system_functions.UploadSFTPPublicKey(filename,file_server))
			printf("Success!<br>");
		else
			printf("Failed to upload key!<br>");
	}
}
