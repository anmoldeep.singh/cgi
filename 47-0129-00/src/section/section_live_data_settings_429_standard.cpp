
#include "section_live_data_settings_429_standard.h"
#include "widget_combobox.h"
#include "widget_textbox.h"
#include "widget_checkbox.h"
#include "widget_radio.h"
#include "widget_horizontal_line.h"


const char *bus_list[] = {
	"FDR",
	"DLR",
	"GMT",
	"OMS",
};

const char *sdi_list[] = {
	"0",
	"1",
	"2",
	"3",
	"ALL",
};

LiveDataSettings429Standard::LiveDataSettings429Standard(string menu_name, bool expanded) : Section("Live Data Settings : ARINC 429 Standard", menu_name, expanded)
{
	widgets.push_back(new ComboBox("Default BUS", &Settings::Instance(), "live.arinc429.standard.bus", bus_list, ARRAY_SIZE(bus_list)));
	widgets.push_back(new TextBox("Default Label", &Settings::Instance(), "live.arinc429.standard.label"));
	widgets.push_back(new ComboBox("Default SDI", &Settings::Instance(), "live.arinc429.standard.sdi", sdi_list, ARRAY_SIZE(sdi_list)));
	widgets.push_back(new HLine());
	widgets.push_back(new TextBox("Default MSB", &Settings::Instance(), "live.arinc429.standard.msb"));
	widgets.push_back(new TextBox("Default LSB", &Settings::Instance(), "live.arinc429.standard.lsb"));
	widgets.push_back(new HLine());
	widgets.push_back(new Radio("Display Hex", &Settings::Instance(), "live.arinc429.standard.format.radix", "Display Hex"));
	widgets.push_back(new Radio("Display Decimal", &Settings::Instance(), "live.arinc429.standard.format.radix", "Display Decimal"));
	widgets.push_back(new Radio("Display Octal", &Settings::Instance(), "live.arinc429.standard.format.radix", "Display Octal"));
	widgets.push_back(new Radio("Display Binary", &Settings::Instance(), "live.arinc429.standard.format.radix", "Display Binary"));
	widgets.push_back(new HLine());
	widgets.push_back(new Radio("No Bit Grouping", &Settings::Instance(), "live.arinc429.standard.format.grouping", "None"));
	widgets.push_back(new Radio("Group 4bit blocks (Hex)", &Settings::Instance(), "live.arinc429.standard.format.grouping", "4bits (Hex)"));
	widgets.push_back(new Radio("Group 3bit blocks (Octal)", &Settings::Instance(), "live.arinc429.standard.format.grouping", "3bits (Octal)"));
	widgets.push_back(new HLine());
	widgets.push_back(new CheckBox("Base Notation", &Settings::Instance(), "live.arinc429.standard.format.base_notation"));
}
