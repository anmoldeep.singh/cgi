
#include "section_flash_variables.h"
#include "widget_ro_text.h"

FlashVariables::FlashVariables(string menu_name, bool expanded) : Section("Flash Variables", menu_name, expanded)
{
	widgets.push_back(new ReadOnlyTextBox("Date Format", &Settings::Instance(), "clock.date_format"));
	widgets.push_back(new ReadOnlyTextBox("Time Format", &Settings::Instance(), "clock.time_format"));
	widgets.push_back(new ReadOnlyTextBox("Show Date", &Settings::Instance(), "clock.show_date"));
	widgets.push_back(new ReadOnlyTextBox("Show Time", &Settings::Instance(), "clock.show_time"));
	widgets.push_back(new ReadOnlyTextBox("Timezone", &Settings::Instance(), "clock.tz"));
	widgets.push_back(new ReadOnlyTextBox("Use Timezone", &Settings::Instance(), "clock.use_tz"));
//	widgets.push_back(new ReadOnlyTextBox("Button Beep", "control.btn_beep"));
//	widgets.push_back(new ReadOnlyTextBox("Error Beep", "control.err_beep"));
//	widgets.push_back(new ReadOnlyTextBox("Key Lock", "control.key_lock"));
//	widgets.push_back(new ReadOnlyTextBox("Display Auto-dim", "display.auto_dim"));
//	widgets.push_back(new ReadOnlyTextBox("Display Auto-dim Timeout, "display.auto_dim_timeout"));
//	widgets.push_back(new ReadOnlyTextBox("Display Auto-off", "display.auto_off"));
//	widgets.push_back(new ReadOnlyTextBox("Display Auto-off Timeout, "display.auto_off_timeout"));
//	widgets.push_back(new ReadOnlyTextBox("Display Gamma", "display.gamma"));
//	widgets.push_back(new ReadOnlyTextBox("Display Off when MPI", "display.mpi_off"));
//	widgets.push_back(new ReadOnlyTextBox("Display Off when MSD", "display.msd_off"));
	widgets.push_back(new ReadOnlyTextBox("File Append Aircraft", &Settings::Instance(), "file.append_aircraft"));
	widgets.push_back(new ReadOnlyTextBox("File Append Model", &Settings::Instance(), "file.append_model"));
	widgets.push_back(new ReadOnlyTextBox("File Append Tail", &Settings::Instance(), "file.append_tail"));
	widgets.push_back(new ReadOnlyTextBox("File Append Date", &Settings::Instance(), "file.append_date"));
	widgets.push_back(new ReadOnlyTextBox("File Append Time", &Settings::Instance(), "file.append_time"));
/*
ftp.ipaddr=192.168.0.1
ftp.login=anonymous
ftp.password=
ftp.remote_path=/teway=10.1.0.1
gatewayip=192.168.0.1"
ipaddr=10.1.20.77
live.base_notation=1
live.binary_grouping=3bits (Octal)
live.format=Display Octal
live.frame=1
live.word=1
memory_device=Internal
memory_device.priority_0=USB Stick
memory_device.priority_1=Compact Flash
memory_device.priority_2=SDCard
memory_device.priority_3=Internal
memory_device.selection=Ask for Device
mmc_boot1=mmc;fatload mmc 0:1 a0008000 hhmpi.bin;fatload mmc 0:1 a1800000 hhmpi.img;go a0008000
mmc_boot2=mmc;fatload mmc 0:2 a0008000 hhmpi.bin;fatload mmc 0:2 a1800000 hhmpi.img;go a0008000
mmc_boot3=mmc;fatload mmc 0:3 a0008000 hhmpi.bin;fatload mmc 0:3 a1800000 hhmpi.img;go a0008000
mmc_boot4=mmc;fatload mmc 0:4 a0008000 hhmpi.bin;fatload mmc 0:4 a1800000 hhmpi.img;go a0008000
net.dhcp_enable=0
net.enable=1
netmask=255.255.0.0
serverip=10.1.20.41
shortcut.1=Select Aircraft
shortcut.2=Download All
shortcut.3=Download Since Last
shortcut.4=Upload File (FTP)
stderr=serial
stdin=serial
stdout=serial
tftp.firmware=0
tftp_boot=tftpboot a0008000 hhmpi.bin;tftpboot a1800000 hhmpi.img;go a0008000
tftp_update=tftpboot a0008000 hhmpi_update.bin;tftpboot a1800000 hhmpi_update.img;go a0008000
update=run tftp_update
usb_device=None
version=U-Boot 1.1.6
baudrate=38400
bootargs=root=/dev/ram initrd=0xa1800000,4000k console=ttyS0,38400N8
bootcmd=run mmc_boot1
bootdelay=3
build=Feb 25 2009 12:38:23
ethaddr=00:50:C2:A1:F0:05
*/
}
