
#include "section_sslcertificate_file.h"
#include "widget_spacer.h"
#include "widget_horizontal_line.h"
#include "widget_file_select.h"
#include "widget_button.h"
#include "widget_html_code.h"
#include "system_functions.h"
#include "access.h"
#include "page.h"

#define BTN_DOWNLOAD_SSL_CERTIFICATE	"Download SSL Certificate"
#define BTN_UPLOAD_SSL_CERTIFICATE	"Upload SSL Certificate"


SSLCertificateFile::SSLCertificateFile(string menu_name, bool expanded) : Section("SSL Certificate", menu_name, expanded)
{
	widgets.push_back(new Spacer());
	widgets.push_back(new Button("Download SSL Certificate File", BTN_DOWNLOAD_SSL_CERTIFICATE, REF_DOWNLOAD));
	widgets.push_back(new HLine());
	widgets.push_back(new FileSelect("Select File", REF_FILENAME));
	widgets.push_back(new Button("Upload SSL Certificate File", BTN_UPLOAD_SSL_CERTIFICATE, REF_UPLOAD));
	widgets.push_back(new Spacer());
}


void SSLCertificateFile::Show(char **postvars)
{
	printf("</form>");
	printf("<form action=\"" APPNAME "\" name=\"sslcertificate_upload\" method=\"post\" enctype=\"multipart/form-data\">");
	PostPersistantVariables();
	Section::Show(postvars);
	printf("</form>");
	printf("<form action=\"" APPNAME "\" method=\"post\">");
}


void SSLCertificateFile::Apply(char **postvars)
{
	bool download = false;
	bool upload = false;
	char *filename = NULL;
	for (int i = 0; postvars[i]; i += 2)
	{
		if (strcmp(postvars[i], REF_DOWNLOAD) == 0)
		{		
			if (strcmp(postvars[i+1], BTN_DOWNLOAD_SSL_CERTIFICATE) == 0)
				download = true;
		}

		if (strcmp(postvars[i], REF_UPLOAD) == 0)
		{
			if (strcmp(postvars[i+1], BTN_UPLOAD_SSL_CERTIFICATE) == 0)
				upload = true;
		}

		if (strcmp(postvars[i], REF_FILENAME) == 0)
			filename = postvars[i+1];
	}

	if(download)
	{
		system_functions.DownloadSSLCertificate();
	}
	else if(upload)
	{
		if(system_functions.UploadSSLCertificate(filename))
			printf("Success!<br>");
		else
			printf("Failed to upload SSL Certificate!<br>");
	}
}
