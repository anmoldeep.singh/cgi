
#include "section_owner_information.h"
#include "widget_button.h"
#include "widget_spacer.h"
#include "widget_textbox.h"

OwnerInformation::OwnerInformation(string menu_name, bool expanded) : Section("Owner Information", menu_name, expanded)
{
	widgets.push_back(new TextBox("Device Description", &Settings::Instance(), "owner.desc"));
	widgets.push_back(new TextBox("Asset Number", &Settings::Instance(), "owner.asset"));
	widgets.push_back(new TextBox("Company Name", &Settings::Instance(), "owner.company"));
	widgets.push_back(new TextBox("Contact Person", &Settings::Instance(), "owner.contact"));
}
