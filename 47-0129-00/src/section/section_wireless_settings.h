
#ifndef __SECTION_WIRELESS_SETTINGS_H
#define __SECTION_WIRELESS_SETTINGS_H

#include "section.h"

class WirelessSettings : public Section
{
	public:
		WirelessSettings(string menu_name, bool expanded);
};

#endif