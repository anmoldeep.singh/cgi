
#ifndef __SECTION_H
#define __SECTION_H

#include "widget.h"
#include "common.h"

class Section
{
	public:
		Section(string name, string ref_name, bool expanded = false);
		virtual ~Section(void) {}

		virtual void Head(void);
		virtual void Show(char **postvars);
		virtual void Apply(char **postvars);

	protected:
		string m_name;
		string m_menu_name;
		
		widget_list_t widgets;
		bool m_expanded;
};


typedef list<Section*> section_list_t;

#endif
