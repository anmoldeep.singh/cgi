
#ifndef __SECTION_BOOT_VARIABLES_H
#define __SECTION_BOOT_VARIABLES_H

#include "section.h"

class BootVariables : public Section
{
	public:
		BootVariables(string menu_name, bool expanded);
};

#endif