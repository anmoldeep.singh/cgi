
#ifndef __SECTION_RECORDER_SUPPORT_H
#define __SECTION_RECORDER_SUPPORT_H

#include "section.h"

class RecorderSupport : public Section
{
	public:
		RecorderSupport(string menu_name, bool expanded);
};

#endif