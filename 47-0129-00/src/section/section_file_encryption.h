#ifndef __SECTION_FILE_ENCRYPTION_H
#define __SECTION_FILE_ENCRYPTION_H

#include "section.h"

class FileEncryption : public Section
{
	public:
		FileEncryption(string menu_name, bool expanded);
};

#endif
