
#ifndef __SECTION_USERS_H
#define __SECTION_USERS_H

#include "section.h"

#include "widget_static_text.h"


class Users : public Section
{
	public:
		Users(string menu_name, bool expanded);
		void Show(char **postvars);
		void Apply(char **postvars);

	protected:
		StaticTextBox *m_static_textbox;
		
};

#endif
