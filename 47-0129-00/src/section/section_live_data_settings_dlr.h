
#ifndef __SECTION_LIVE_DATA_SETTINGS_DLR_H
#define __SECTION_LIVE_DATA_SETTINGS_DLR_H

#include "section.h"

class LiveDataSettingsDLR : public Section
{
	public:
		LiveDataSettingsDLR(string menu_name, bool expanded);
};

#endif