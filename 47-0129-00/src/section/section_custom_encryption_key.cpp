
#include "section_custom_encryption_key.h"
#include "widget_spacer.h"
#include "widget_horizontal_line.h"
#include "widget_file_select.h"
#include "widget_button.h"
#include "widget_html_code.h"
#include "system_functions.h"
#include "access.h"
#include "page.h"

#define BTN_DOWNLOAD_CUSTOM_ENCRYPTION_KEY	"Download Encryption Key"
#define BTN_UPLOAD_CUSTOM_ENCRYPTION_KEY	"Upload Encryption Key"


CustomEncryptionKey::CustomEncryptionKey(string menu_name, bool expanded) : Section("Custom Encryption Key", menu_name, expanded)
{
	widgets.push_back(new Spacer());
	widgets.push_back(new Button("Download Encryption Key", BTN_DOWNLOAD_CUSTOM_ENCRYPTION_KEY, REF_DOWNLOAD));
	widgets.push_back(new HLine());
	widgets.push_back(new FileSelect("Select File", REF_FILENAME));
	widgets.push_back(new Button("Upload Encryption Key", BTN_UPLOAD_CUSTOM_ENCRYPTION_KEY, REF_UPLOAD));
	widgets.push_back(new Spacer());
}


void CustomEncryptionKey::Show(char **postvars)
{
	printf("</form>");
	printf("<form action=\"" APPNAME "\" name=\"custom_encryption_key_upload\" method=\"post\" enctype=\"multipart/form-data\">");
	PostPersistantVariables();
	Section::Show(postvars);
	printf("</form>");
	printf("<form action=\"" APPNAME "\" method=\"post\">");
}


void CustomEncryptionKey::Apply(char **postvars)
{
	bool download = false;
	bool upload = false;
	char *filename = NULL;
	for (int i = 0; postvars[i]; i += 2)
	{
		if (strcmp(postvars[i], REF_DOWNLOAD) == 0)
		{		
			if (strcmp(postvars[i+1], BTN_DOWNLOAD_CUSTOM_ENCRYPTION_KEY) == 0)
				download = true;
		}

		if (strcmp(postvars[i], REF_UPLOAD) == 0)
		{
			if (strcmp(postvars[i+1], BTN_UPLOAD_CUSTOM_ENCRYPTION_KEY) == 0)
				upload = true;
		}

		if (strcmp(postvars[i], REF_FILENAME) == 0)
			filename = postvars[i+1];
	}

	if(download)
	{
		system_functions.DownloadCustomEncryptionKey();
	}
	else if(upload)
	{
		if(system_functions.UploadCustomEncryptionKey(filename))
			printf("Success!<br>");
		else
			printf("Failed to upload encryption key!<br>");
	}
}
