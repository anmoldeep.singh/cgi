
#ifndef __SECTION_SSL_CERTIFICATE_FILE_H
#define __SECTION_SSL_CERTIFICATE_FILE_H

#include "section.h"

class SSLCertificateFile : public Section
{
	public:
		SSLCertificateFile(string menu_name, bool expanded);
		void Show(char **postvars);
		void Apply(char **postvars);
};

#endif