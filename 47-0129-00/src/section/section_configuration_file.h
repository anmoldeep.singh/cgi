
#ifndef __SECTION_CONFIGURATION_FILE_H
#define __SECTION_CONFIGURATION_FILE_H

#include "section.h"

class ConfigurationFile : public Section
{
	public:
		ConfigurationFile(string menu_name, bool expanded);
		void Show(char **postvars);
		void Apply(char **postvars);
};

#endif