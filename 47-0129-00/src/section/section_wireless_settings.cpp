
#include "section_wireless_settings.h"
#include "widget_checkbox.h"
#include "widget_radio.h"
#include "widget_spacer.h"
#include "widget_textbox.h"
#include "widget_static_text.h"
#include "widget_ro_text.h"
#include "widget_horizontal_line.h"
#include "widget_encryption_key.h"
#include "system_functions.h"


WirelessSettings::WirelessSettings(string menu_name, bool expanded) : Section("Wireless Settings", menu_name, expanded)
{
	widgets.push_back(new StaticTextBox("Hardware Address", SystemFunctions::GetMAC("wlan0").c_str()));
	widgets.push_back(new CheckBox("Enable Networking", &Settings::Instance(), "wifi.enable"));
	widgets.push_back(new Spacer());
	widgets.push_back(new TextBox("Access Point (ESSID)", &Settings::Instance(), "wifi.essid"));
	widgets.push_back(new Radio("Auto Configure (DHCP)", &Settings::Instance(), "wifi.dhcp_enable", "1"));
	widgets.push_back(new Radio("Manual Configure", &Settings::Instance(), "wifi.dhcp_enable", "0"));
	widgets.push_back(new HLine());
	widgets.push_back(new StaticTextBox("Manual Settings", ""));
	widgets.push_back(new TextBox("Static IP Address", &Settings::Instance(), "wifi.ipaddr"));
	widgets.push_back(new TextBox("Netmask", &Settings::Instance(), "wifi.netmask"));
	widgets.push_back(new TextBox("Gateway", &Settings::Instance(), "wifi.gatewayip"));
	widgets.push_back(new HLine());
	widgets.push_back(new CheckBox("Enable Encryption", &Settings::Instance(), "wifi.encryption_enabled"));

	Radio *radio_wep = new Radio("WEP", &Settings::Instance(), "wifi.encryption_type", "WEP");
	Radio *radio_wpa1 = new Radio("WPA1", &Settings::Instance(), "wifi.encryption_type", "WPA1");
	Radio *radio_wpa2 = new Radio("WPA2", &Settings::Instance(), "wifi.encryption_type", "WPA2");

	radio_wep->SetAdditional("onclick=\"hash_disable();\"");
	radio_wpa1->SetAdditional("onclick=\"hash_enable();\"");
	radio_wpa2->SetAdditional("onclick=\"hash_enable();\"");

	widgets.push_back(radio_wep);
	widgets.push_back(radio_wpa1);
	widgets.push_back(radio_wpa2);
	widgets.push_back(new EncryptionKey("Key", &Settings::Instance(), "wifi.key"));
}
