
#ifndef __SECTION_MEMORY_SETTINGS_H
#define __SECTION_MEMORY_SETTINGS_H

#include "section.h"

class MemorySettings : public Section
{
	public:
		MemorySettings(string menu_name, bool expanded);
		virtual void Apply(char **postvars);
};

#endif
