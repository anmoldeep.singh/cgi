
#ifndef __SECTION_TOOLS_H
#define __SECTION_TOOLS_H

#include "section.h"

class Tools : public Section
{
	public:
		Tools(string menu_name, bool expanded);
		void Show(char **postvars);
		void Apply(char **postvars);
};

#endif