
#include "section_clock_settings.h"
#include "widget_combobox.h"
#include "widget_checkbox.h"
#include "widget_timeselect.h"
#include "widget_dateselect.h"


const char *time_format_list[] = {
	"12 Hour",
	"24 Hour",
};

const char *date_format_list[] = {
	"YYYY/MM/DD",
	"DD/MM/YYYY",
	"DD/MM/YY",
	"MM/DD/YYYY",
	"MM/DD/YY",
};

const char *timezone_list[] = {
	"UTC: England",
	"+1: Europe",
	"+2: Central Europe",
	"+3: Moscow",
	"+4: Oman",
	"+5: Pakistan",
	"+6: India",
	"+7: Indonesia",
	"+8: West Australia",
	"+9: Japan",
	"+10: East Australia",
	"+11: Solmon Islands",
	"+12: Marshall Islands",
	"-11: Samoa. Midway",
	"-10: Hawaii",
	"-9: Alaska",
	"-8: US Pacific",
	"-7: US Mountain",
	"-6: US Central",
	"-5: US Eastern",
	"-4: Venezuela",
	"-3: Greenland",
	"-2: Mid-Atlantic",
	"-1: Cape Verda Is.",
};


ClockSettings::ClockSettings(string menu_name, bool expanded) : Section("Clock Settings", menu_name, expanded)
{
	widgets.push_back(new ComboBox("Time Format", &Settings::Instance(), "clock.time_format", time_format_list, ARRAY_SIZE(time_format_list)));
	widgets.push_back(new ComboBox("Date Format", &Settings::Instance(), "clock.date_format", date_format_list, ARRAY_SIZE(date_format_list)));
	widgets.push_back(new TimeSelect("Set Time", "clock.timeselect"));
	widgets.push_back(new DateSelect("Set Date", "clock.dateselect"));
//	widgets.push_back(new CheckBox("Use Timezones", &Settings::Instance(), "clock.use_tz"));
//	widgets.push_back(new ComboBox("Timezone", &Settings::Instance(), "clock.tz", timezone_list, ARRAY_SIZE(timezone_list)));
	widgets.push_back(new CheckBox("Display Date", &Settings::Instance(), "clock.show_date"));
	widgets.push_back(new CheckBox("Display Time", &Settings::Instance(), "clock.show_time"));
}

