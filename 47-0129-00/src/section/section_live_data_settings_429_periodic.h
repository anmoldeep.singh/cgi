
#ifndef __SECTION_LIVE_DATA_SETTINGS_429_PERIODIC_H
#define __SECTION_LIVE_DATA_SETTINGS_429_PERIODIC_H

#include "section.h"

class LiveDataSettings429Periodic : public Section
{
	public:
		LiveDataSettings429Periodic(string menu_name, bool expanded);
};

#endif