
#include "section_live_data_settings_717.h"
#include "widget_combobox.h"
#include "widget_textbox.h"
#include "widget_checkbox.h"
#include "widget_radio.h"
#include "widget_horizontal_line.h"



const char *subframe_list[] = {
	"1",
	"2",
	"3",
	"4",
	"ALL",
};


LiveDataSettings717::LiveDataSettings717(string menu_name, bool expanded) : Section("Live Data Settings : ARINC 717", menu_name, expanded)
{
	widgets.push_back(new CheckBox("Hide Advanced Settings (Viewing mode, MSB/LSB etc)", &Settings::Instance(), "live.arinc717.hide_advanced"));
	widgets.push_back(new HLine());
	widgets.push_back(new ComboBox("Default Sub-Frame", &Settings::Instance(), "live.arinc717.frame", subframe_list, ARRAY_SIZE(subframe_list)));
	widgets.push_back(new TextBox("Default Word Offset", &Settings::Instance(), "live.arinc717.word"));
	widgets.push_back(new HLine());
	widgets.push_back(new Radio("Display Hex", &Settings::Instance(), "live.arinc717.format.radix", "Display Hex"));
	widgets.push_back(new Radio("Display Decimal", &Settings::Instance(), "live.arinc717.format.radix", "Display Decimal"));
	widgets.push_back(new Radio("Display Octal", &Settings::Instance(), "live.arinc717.format.radix", "Display Octal"));
	widgets.push_back(new Radio("Display Binary", &Settings::Instance(), "live.arinc717.format.radix", "Display Binary"));
	widgets.push_back(new HLine());
	widgets.push_back(new Radio("No Bit Grouping", &Settings::Instance(), "live.arinc717.format.grouping", "None"));
	widgets.push_back(new Radio("Group 4bit blocks (Hex)", &Settings::Instance(), "live.arinc717.format.grouping", "4bits (Hex)"));
	widgets.push_back(new Radio("Group 3bit blocks (Octal)", &Settings::Instance(), "live.arinc717.format.grouping", "3bits (Octal)"));
	widgets.push_back(new HLine());
	widgets.push_back(new CheckBox("Base Notation", &Settings::Instance(), "live.arinc717.format.base_notation"));
}
