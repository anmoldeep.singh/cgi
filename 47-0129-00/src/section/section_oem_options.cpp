
#include "section_oem_options.h"
#include "widget_combobox.h"
#include "system_functions.h"



const char *oem_list[] = {
	"fds",
	"srvivr",
	"l3",
};

#define OEM_NAME_SETTING		"oem.name"

OEMOptions::OEMOptions(string menu_name, bool expanded) : Section("OEM Options", menu_name, expanded)
{
	widgets.push_back(new ComboBox("OEM Model", &Settings::Instance(), OEM_NAME_SETTING, oem_list, ARRAY_SIZE(oem_list)));
}

void OEMOptions::Apply(char **postvars)
{
	char *oem_name = NULL;

	for (int i = 0; postvars[i]; i += 2)
		if (strcmp(postvars[i], OEM_NAME_SETTING) == 0)
			if (Settings::Instance().String(OEM_NAME_SETTING) != postvars[i+1])
			{
				oem_name = postvars[i+1];
				break;
			}

	Section::Apply(postvars);

	if (oem_name)
	{
		printf("Changed OEM Model to \"%s\".\n", oem_name);
		SystemFunctions::InstallOEMType(oem_name);
	}
}

