
#ifndef __SECTION_FILE_SETTINGS_H
#define __SECTION_FILE_SETTINGS_H

#include "section.h"

class FileSettings : public Section
{
	public:
		FileSettings(string menu_name, bool expanded);
};

#endif