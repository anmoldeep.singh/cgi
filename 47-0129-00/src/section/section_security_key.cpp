#include <hhmpi/gui_config.h>
#include <hhmpi/idcheck.h>

#include "section_security_key.h"

#include "widget_textbox.h"
#include "widget_static_text.h"
#include "widget_button.h"
#include "system_functions.h"



#define BTN_DOWNLOAD_HHMPI_INFO		"Download Configuration"


SecurityKey::SecurityKey(string menu_name, bool expanded) : Section("Security Key", menu_name, expanded)
{
	string key_validation_string = "<font color=\"red\"><b>FAILED</b></font>";

	IDCheck id_check;

	if (id_check.SystemCheck(HHMPI_PUBLIC_KEY))
		key_validation_string = "<font color=\"green\"><b>PASSED</b></font>";

	widgets.push_back(new StaticTextBox("Key Validation", key_validation_string));
	widgets.push_back(new TextBox("Key", &Settings::Instance(), "key"));
	widgets.push_back(new Button("Download Security Configuration", BTN_DOWNLOAD_HHMPI_INFO, REF_DOWNLOAD));
}

void SecurityKey::Apply(char **postvars)
{
	bool download = false;

	for (int i = 0; postvars[i]; i += 2)
		if (strcmp(postvars[i], REF_DOWNLOAD) == 0 && strcmp(postvars[i+1], BTN_DOWNLOAD_HHMPI_INFO) == 0)
			download = true;

	if (download)
		system_functions.DownloadSecurityConfiguration();
	else
		Section::Apply(postvars);
}
