
#ifndef __SECTION_AIRCRAFT_SETTINGS_H
#define __SECTION_AIRCRAFT_SETTINGS_H

#include "section.h"

class AircraftSettings : public Section
{
	public:
		AircraftSettings(string menu_name, bool expanded);
};

#endif
