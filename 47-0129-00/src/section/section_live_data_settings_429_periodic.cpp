
#include "section_live_data_settings_429_periodic.h"
#include "widget_combobox.h"
#include "widget_textbox.h"
#include "widget_checkbox.h"
#include "widget_radio.h"
#include "widget_horizontal_line.h"


const char *frequency_list[] = {
	"1A Hz",
	"1B Hz",
	"2 Hz",
	"4 Hz",
	"8 Hz",
	"16 Hz",
	"32 Hz",
};

LiveDataSettings429Periodic::LiveDataSettings429Periodic(string menu_name, bool expanded) : Section("Live Data Settings : ARINC 429 Periodic", menu_name, expanded)
{
	widgets.push_back(new ComboBox("Default Frequency", &Settings::Instance(), "live.arinc429.periodic.freq", frequency_list, ARRAY_SIZE(frequency_list)));
	widgets.push_back(new TextBox("Default Word", &Settings::Instance(), "live.arinc429.periodic.word"));
	widgets.push_back(new HLine());
	widgets.push_back(new TextBox("Default MSB", &Settings::Instance(), "live.arinc429.periodic.msb"));
	widgets.push_back(new TextBox("Default LSB", &Settings::Instance(), "live.arinc429.periodic.lsb"));
	widgets.push_back(new HLine());
	widgets.push_back(new Radio("Display Hex", &Settings::Instance(), "live.arinc429.periodic.format.radix", "Display Hex"));
	widgets.push_back(new Radio("Display Decimal", &Settings::Instance(), "live.arinc429.periodic.format.radix", "Display Decimal"));
	widgets.push_back(new Radio("Display Octal", &Settings::Instance(), "live.arinc429.periodic.format.radix", "Display Octal"));
	widgets.push_back(new Radio("Display Binary", &Settings::Instance(), "live.arinc429.periodic.format.radix", "Display Binary"));
	widgets.push_back(new HLine());
	widgets.push_back(new Radio("No Bit Grouping", &Settings::Instance(), "live.arinc429.periodic.format.grouping", "None"));
	widgets.push_back(new Radio("Group 4bit blocks (Hex)", &Settings::Instance(), "live.arinc429.periodic.format.grouping", "4bits (Hex)"));
	widgets.push_back(new Radio("Group 3bit blocks (Octal)", &Settings::Instance(), "live.arinc429.periodic.format.grouping", "3bits (Octal)"));
	widgets.push_back(new HLine());
	widgets.push_back(new CheckBox("Base Notation", &Settings::Instance(), "live.arinc429.periodic.format.base_notation"));
}
