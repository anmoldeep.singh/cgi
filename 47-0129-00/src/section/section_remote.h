
#ifndef __SECTION_REMOTE_H
#define __SECTION_REMOTE_H

#include "section.h"

class Remote : public Section
{
	public:
		Remote(string menu_name, bool expanded);
		void Show(char **postvars);
};

#endif
