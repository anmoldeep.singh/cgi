
#ifndef __SECTION_OWNER_INFORMATION_H
#define __SECTION_OWNER_INFORMATION_H

#include "section.h"

class OwnerInformation : public Section
{
	public:
		OwnerInformation(string menu_name, bool expanded);
};

#endif