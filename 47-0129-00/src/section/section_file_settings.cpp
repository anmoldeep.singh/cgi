
#include "section_file_settings.h"
#include "widget_checkbox.h"
#include "widget_combobox.h"
#include "widget_textbox.h"
#include "widget_static_text.h"
#include "widget_horizontal_line.h"

const char *file_settings_list[] = {
	"Basic Filename",
	"Advanced Filename",
};

FileSettings::FileSettings(string menu_name, bool expanded) : Section("File Settings", menu_name, expanded)
{
	widgets.push_back(new ComboBox("Filename Settings", &Settings::Instance(), "file.menu_filename_settings", file_settings_list, ARRAY_SIZE(file_settings_list)));
	widgets.push_back(new HLine());
	widgets.push_back(new StaticTextBox("Basic Filename", ""));
	widgets.push_back(new CheckBox("Append Aircraft Manufacturer", &Settings::Instance(), "file.append_aircraft"));
	widgets.push_back(new CheckBox("Append Aircraft Model", &Settings::Instance(), "file.append_model"));
	widgets.push_back(new CheckBox("Append Aircraft Tail Number", &Settings::Instance(), "file.append_tail"));
	widgets.push_back(new CheckBox("Append Date", &Settings::Instance(), "file.append_date"));
	widgets.push_back(new CheckBox("Append Time", &Settings::Instance(), "file.append_time"));
	widgets.push_back(new CheckBox("Append IP Address", &Settings::Instance(), "file.append_ip_address"));
	widgets.push_back(new CheckBox("Append MAC Address", &Settings::Instance(), "file.append_mac_address"));
	widgets.push_back(new HLine());
	widgets.push_back(new StaticTextBox("Advanced Filename", ""));
	widgets.push_back(new TextBox("Advanced Style", &Settings::Instance(), "file.advanced_format_style"));
}

