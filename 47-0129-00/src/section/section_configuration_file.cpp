
#include "section_configuration_file.h"
#include "widget_spacer.h"
#include "widget_horizontal_line.h"
#include "widget_file_select.h"
#include "widget_button.h"
#include "widget_html_code.h"
#include "system_functions.h"
#include "access.h"
#include "page.h"

#define BTN_DOWNLOAD_CONFIGURATION	"Download Configuration"
#define BTN_UPLOAD_CONFIGURATION	"Upload Configuration"

ConfigurationFile::ConfigurationFile(string menu_name, bool expanded) : Section("Device Configuration", menu_name, expanded)
{
	widgets.push_back(new Spacer());
	widgets.push_back(new Button("Download Device Configuration", BTN_DOWNLOAD_CONFIGURATION, REF_DOWNLOAD));
	widgets.push_back(new HLine());
	widgets.push_back(new FileSelect("Select File", REF_FILENAME));
	widgets.push_back(new Button("Upload Device Configuration", BTN_UPLOAD_CONFIGURATION, REF_UPLOAD));
	widgets.push_back(new Spacer());
}


void ConfigurationFile::Show(char **postvars)
{
	printf("</form>");
	printf("<form action=\"" APPNAME "\" name=\"aircraft_upload\" method=\"post\" enctype=\"multipart/form-data\">");
	PostPersistantVariables();
	Section::Show(postvars);
	printf("</form>");
	printf("<form action=\"" APPNAME "\" method=\"post\">");
}


void ConfigurationFile::Apply(char **postvars)
{
	bool download = false;
	bool upload = false;
	char *filename = NULL;
	for (int i = 0; postvars[i]; i += 2)
	{
		if (strcmp(postvars[i], REF_DOWNLOAD) == 0)
		{
			if (strcmp(postvars[i+1], BTN_DOWNLOAD_CONFIGURATION) == 0)
				download = true;
		}

		if (strcmp(postvars[i], REF_UPLOAD) == 0)
		{
			if (strcmp(postvars[i+1], BTN_UPLOAD_CONFIGURATION) == 0)
				upload = true;
		}
	
		if (strcmp(postvars[i], REF_FILENAME) == 0)
			filename = postvars[i+1];
	}

	if(download)
	{
		system_functions.DownloadFlash();
	}
	else if(upload)
	{
		//printf("Upload Config File: '%s'", filename);
		if(system_functions.UploadFlash(filename))
			printf("Success!<br>");
		else
			printf("Failed to upload configuration!<br>");
	}
}
