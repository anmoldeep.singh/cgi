
#ifndef __SECTION_LIVE_DATA_SETTINGS_717_H
#define __SECTION_LIVE_DATA_SETTINGS_717_H

#include "section.h"

class LiveDataSettings717 : public Section
{
	public:
		LiveDataSettings717(string menu_name, bool expanded);
};

#endif