
#include "section_auto_update_settings.h"
#include "widget_combobox.h"
#include "widget_checkbox.h"
#include "widget_textbox.h"
#include "widget_numberbox.h"


char *update_server_entry_list[128];


AutoUpdateSettings::AutoUpdateSettings(string menu_name, bool expanded) : Section("Auto Update Settings", menu_name, expanded)
{
	uint32_t server_count = 0;

        SettingsMap& settings_map = Settings::Instance().GetSettingsMap();

        list<string> id_list;

        string previous_name;

        SettingsMap::iterator it;

        for (it = settings_map.begin();
                        it != settings_map.end();
                        it++)
        {
                string identifier = it->first;
                string value = it->second;

                if (identifier.substr(0, 4) != "ftp.")
                        continue;

                size_t pos = identifier.substr(4).find(".");

                if (pos == string::npos)
                        continue;

                string this_name = identifier.substr(4, pos);

                if (this_name != previous_name)
                {
                        previous_name = this_name;
                        id_list.push_back(this_name);
                }
        }

        list<string>::iterator jt;

        for (jt = id_list.begin();
                        jt != id_list.end();
                        jt++)
        {
                string& identifier = *jt;

		update_server_entry_list[server_count] = new char[ identifier.length() + 1 ];
		snprintf(update_server_entry_list[server_count], identifier.length() + 1, "%s", identifier.c_str());
		server_count++;
	}

	widgets.push_back(new CheckBox("Automatically Install Configuration Updates", &Settings::Instance(), "ftp.auto_configuration_update"));
	widgets.push_back(new CheckBox("Automatically Install Firmware Updates", &Settings::Instance(), "ftp.auto_firmware_update"));
	widgets.push_back(new ComboBox("Configuration Management Server 1", &Settings::Instance(), "ftp.update_ftp_server_1", (const char **) update_server_entry_list, server_count));
	widgets.push_back(new ComboBox("Configuration Management Server 2", &Settings::Instance(), "ftp.update_ftp_server_2", (const char **) update_server_entry_list, server_count));
	widgets.push_back(new NumberBox("Check for updates every # hours", &Settings::Instance(), "ftp.configuration_update_cycle"));
}

