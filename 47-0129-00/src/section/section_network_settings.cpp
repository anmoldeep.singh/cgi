
#include "section_network_settings.h"
#include "widget_checkbox.h"
#include "widget_radio.h"
#include "widget_spacer.h"
#include "widget_ftprename.h"
#include "widget_ftpcreate.h"
#include "widget_textbox.h"
#include "widget_static_text.h"
#include "widget_ro_text.h"
#include "widget_horizontal_line.h"
#include "widget_html_code.h"
#include "widget_button.h"
#include "lists.h"

#define BTN_ADD_FTP_SERVER	"Add FTP Server"
#define FTPCREATE			"ftpcreate"

NetworkSettings::NetworkSettings(string menu_name, bool expanded) : Section("Network Settings", menu_name, expanded)
{
	GenerateFTPWidgets();
}

void NetworkSettings::GenerateFTPWidgets(void)
{
	ftp_widgets.clear();

/*
	ftp_widgets.push_back(new StaticTextBox("TFTP Server Settings", ""));
	ftp_widgets.push_back(new HTMLCode("<tr><td></td><td>\
<input type=hidden name=\""REF_FILENAME"\" value=\""+identifier+"\">\
<input type=submit name=\""REF_ACTION"\" value=\""REF_DELETE"\" onClick=\"return confirm('Are you sure you want to delete this item?');\"> \
</td></tr>"));
*/

	ftp_widgets.push_back(new FTPCreate("FTP Server Name", &Settings::Instance(), FTPCREATE));
	ftp_widgets.push_back(new Button("", BTN_ADD_FTP_SERVER, REF_BUTTON));

	SettingsMap& settings_map = Settings::Instance().GetSettingsMap();

	list<string> id_list;

	string previous_name;

	SettingsMap::iterator it;

	for (it = settings_map.begin();
			it != settings_map.end();
			it++)
	{
		string identifier = it->first;
		string value = it->second;

		if (identifier.substr(0, 4) != "ftp.")
			continue;

		size_t pos = identifier.substr(4).find(".");

		if (pos == string::npos)
			continue;

		string this_name = identifier.substr(4, pos);

		if (this_name != previous_name)
		{
			previous_name = this_name;
			id_list.push_back(this_name);
		}
	}

	list<string>::iterator jt;

	for (jt = id_list.begin();
			jt != id_list.end();
			jt++)
	{
		string& identifier = *jt;

		ftp_widgets.push_back(new HTMLCode("<tr><td align=\"center\" width=\"100%%\" colspan=\"2\" class=\"fieldcell\">"));
		ftp_widgets.push_back(new HTMLCode("<table class=\"table_blue\" width=\"100%%\">"));
		FTPRename *ftp_name = new FTPRename("Server Name", &Settings::Instance(), identifier);
		ftp_name_widgets.push_back(ftp_name);
		ftp_widgets.push_back(ftp_name);
		ftp_widgets.push_back(new HTMLCode("<tr><td></td><td>\
<input type=hidden name=\""REF_FILENAME"\" value=\""+identifier+"\">\
<input type=submit name=\""REF_ACTION"\" value=\""REF_DELETE"\" onClick=\"return confirm('Are you sure you want to delete this item?');\"> \
</td></tr>"));
		//ftp_widgets.push_back(new TextBox("FTP Server", &Settings::Instance(), "ftp." + identifier + ".ipaddr"));
		ftp_widgets.push_back(new HLine());
		ftp_widgets.push_back(new StaticTextBox("FTP Type", ""));
		ftp_widgets.push_back(new Radio("FTP (port 21)", &Settings::Instance(), "ftp." + identifier + ".type", "FTP (port 21)"));
		ftp_widgets.push_back(new Radio("Secure FTP over SSL", &Settings::Instance(), "ftp." + identifier + ".type", "FTP over SSL"));
		ftp_widgets.push_back(new Radio("Secure FTP over SSH", &Settings::Instance(), "ftp." + identifier + ".type", "FTP over SSH"));
		ftp_widgets.push_back(new Radio("SFTP", &Settings::Instance(), "ftp." + identifier + ".type", "SFTP"));
		ftp_widgets.push_back(new HLine());
		//Certificate upload is in backup/restore tab
		//ftp_widgets.push_back(new StaticTextBox("SSL Certificate", ""));
		//ftp_widgets.push_back(new TextBox("Certificate Location", "ftp.ssl_certificate"));
		//ftp_widgets.push_back(new HLine());
		ftp_widgets.push_back(new StaticTextBox("SSL Mode", ""));
		ftp_widgets.push_back(new Radio("Implicit (ftps://)", &Settings::Instance(), "ftp." + identifier + ".ssl_mode", "Implicit (ftps://)"));
		ftp_widgets.push_back(new Radio("Explicit (ftp://)", &Settings::Instance(), "ftp." + identifier + ".ssl_mode", "Explicit (ftp://)"));
		ftp_widgets.push_back(new HLine());
		ftp_widgets.push_back(new StaticTextBox("SSL\\TLS Version", ""));
		ftp_widgets.push_back(new Radio("SSL v3", &Settings::Instance(), "ftp." + identifier + ".ssl_version", "SSL v3"));
		ftp_widgets.push_back(new Radio("TLS v1.0", &Settings::Instance(), "ftp." + identifier + ".ssl_version", "TLS v1.0"));
		ftp_widgets.push_back(new Radio("TLS v1.1", &Settings::Instance(), "ftp." + identifier + ".ssl_version", "TLS v1.1"));
		ftp_widgets.push_back(new Radio("TLS v1.2", &Settings::Instance(), "ftp." + identifier + ".ssl_version", "TLS v1.2"));
		ftp_widgets.push_back(new Radio("TLS v1.3", &Settings::Instance(), "ftp." + identifier + ".ssl_version", "TLS v1.3"));
		ftp_widgets.push_back(new HLine());
		ftp_widgets.push_back(new TextBox("FTP Server IP Address", &Settings::Instance(), "ftp." + identifier + ".ipaddr"));
		ftp_widgets.push_back(new TextBox("FTP Login", &Settings::Instance(), "ftp." + identifier + ".login"));
		ftp_widgets.push_back(new TextBox("FTP Password", &Settings::Instance(), "ftp." + identifier + ".password"));
		ftp_widgets.push_back(new TextBox("FTP Remote Path", &Settings::Instance(), "ftp." + identifier + ".remote_path"));
		ftp_widgets.push_back(new HLine());
		ftp_widgets.push_back(new HTMLCode("</table>"));
		ftp_widgets.push_back(new HTMLCode("</td></tr>"));
	}

	ftp_widgets.push_back(new StaticTextBox("TFTP Server Settings", ""));
	ftp_widgets.push_back(new TextBox("TFTP Server IP Address", &Settings::Instance(), "serverip"));
}

void NetworkSettings::DeleteFTP(string server_name)
{
	printf("<p class=\"operation_status_area\">");

	SettingsMap& settings_map = Settings::Instance().GetSettingsMap();

	list<string> id_list;

	SettingsMap::iterator it;

	for (it = settings_map.begin();
			it != settings_map.end();
			it++)
	{
		string identifier = it->first;
		string value = it->second;

		if (identifier.substr(0, 4) != "ftp.")
			continue;

		size_t pos = identifier.substr(4).find(".");

		if (pos == string::npos)
			continue;

		string this_name = identifier.substr(4, pos);

		//printf("this_name: %s\n", this_name.c_str());
		//printf("server_name: %s\n<br>", server_name.c_str());

		if (this_name == server_name)
			id_list.push_back(identifier);
	}

	list<string>::iterator jt;

	for (jt = id_list.begin();
			jt != id_list.end();
			jt++)
	{
		string& identifier = *jt;

		//printf("Removing %s\n", identifier.c_str());
		Settings::Instance().Remove(identifier);
	}

	Settings::Instance().Save(SECTION_HHMPI);
	Settings::Instance().SaveFlashForce();
	printf("Deleted FTP Server %s<br>", server_name.c_str());

	printf("</p>");
}

void NetworkSettings::CreateFTP(string server_name)
{
	printf("<p class=\"operation_status_area\">");

	//printf("Create %s\n", server_name.c_str());

	SettingsMap& settings_map = Settings::Instance().GetSettingsMap();

	list<string> id_list;

	string previous_name;

	SettingsMap::iterator it;

	for (it = settings_map.begin();
			it != settings_map.end();
			it++)
	{
		string identifier = it->first;
		string value = it->second;

		if (identifier.substr(0, 4) != "ftp.")
			continue;

		size_t pos = identifier.substr(4).find(".");

		if (pos == string::npos)
			continue;

		string this_name = identifier.substr(4, pos);

		if (this_name == server_name)
		{
			printf("Server already exists!<br>");
			printf("</p>");
			return;
		}
	}

	Settings& settings = Settings::Instance();

	settings.String("ftp." + server_name + ".ipaddr", "0.0.0.0");
	settings.String("ftp." + server_name + ".login", "user");
	settings.String("ftp." + server_name + ".password", "password");
	settings.String("ftp." + server_name + ".remote_path", "/");
	settings.String("ftp." + server_name + ".ssl_mode", "Implicit (ftps://)");
	settings.String("ftp." + server_name + ".ssl_version", "SSL v2");
	settings.String("ftp." + server_name + ".type", "FTP (port 21)");

	settings.Save(SECTION_HHMPI);
	printf("Added FTP Server %s<br>", server_name.c_str());

	printf("</p>");
}

void NetworkSettings::Apply(char **postvars)
{
	string filename, ftpcreate;
	bool del = false;
	bool add = false;

	for (int i = 0; postvars[i]; i += 2)
	{
		//printf("postvars[i]: %s<br>\n", postvars[i]);
		//printf("postvars[i+1]: %s<br>\n", postvars[i+1]);

		if (strcmp(postvars[i], REF_ACTION) == 0)
		{
			if (strcmp(postvars[i+1], REF_DELETE) == 0)
			{
				del = true;
				break;
			}
		}

		if (strcmp(postvars[i], REF_BUTTON) == 0)
			if (strcmp(postvars[i+1], BTN_ADD_FTP_SERVER) == 0)
			{
				add = true;
				break;
			}

		if (strcmp(postvars[i], REF_FILENAME) == 0)
			filename = postvars[i+1];

		if (strcmp(postvars[i], FTPCREATE) == 0)
			ftpcreate = postvars[i+1];
	}

	if (del)
	{
		//printf("Delete: %s\n", filename.c_str());
		DeleteFTP(filename);
		GenerateFTPWidgets();
	}

	if (add)
	{
		CreateFTP(ftpcreate);
		GenerateFTPWidgets();
	}

	{
		Lists<Widget> widget_list(ftp_name_widgets);
		Widget *widget = widget_list.FirstItem();
		while(widget)
		{
			widget->Set(postvars);
			widget = widget_list.NextItem();
		}
	}

	GenerateFTPWidgets();

	{
		Lists<Widget> widget_list(ftp_widgets);
		Widget *widget = widget_list.FirstItem();
		while(widget)
		{
			if (dynamic_cast<FTPRename *>(widget) == NULL)
				widget->Set(postvars);

			widget = widget_list.NextItem();
		}
	}
	Section::Apply(postvars);
}

void NetworkSettings::Show(char **postvars)
{
	GenerateFTPWidgets();

	Lists<Widget> widget_list(ftp_widgets);
	Widget *widget = widget_list.FirstItem();
	while(widget)
	{
		widgets.push_back(widget);
		widget = widget_list.NextItem();
	}

	Section::Show(postvars);
}

