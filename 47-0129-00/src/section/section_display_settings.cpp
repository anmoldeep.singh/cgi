
#include "section_display_settings.h"
#include "widget_checkbox.h"
#include "widget_combobox_brightness.h"

const char *display_brightness_list[] = {
	"1",
	"2",
	"3",
	"4",
	"5",
	"6",
	"7",
	"8",
	"9",
	"10",
};

	

DisplaySettings::DisplaySettings(string menu_name, bool expanded) : Section("Display Settings", menu_name, expanded)
{
	widgets.push_back(new CheckBox("Auto-Off", &Settings::Instance(), "display.auto_off"));
	widgets.push_back(new ComboBoxBrightness("Brightness Adjust", &Settings::Instance(), "display.brightness", display_brightness_list, ARRAY_SIZE(display_brightness_list)));
}

