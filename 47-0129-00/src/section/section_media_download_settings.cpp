#include "section_media_download_settings.h"
#include "widget_combobox.h"

const char *compression_list[] = {
	"0",
	"1",
	"2",
	"3",
	"4",
	"5",
	"6",
	"7",
	"8",
	"9",
};

MediaDownloadSettings::MediaDownloadSettings(string menu_name, bool expanded) : Section("Media Download Settings", menu_name, expanded)
{
	widgets.push_back(new ComboBox("Compression Level", &Settings::Instance(), "media_download.compression_level", compression_list, ARRAY_SIZE(compression_list)));
}
