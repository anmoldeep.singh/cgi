
#ifndef __SECTION_DEVICE_INFORMATION_H
#define __SECTION_DEVICE_INFORMATION_H

#include "section.h"

class DeviceInformation : public Section
{
	public:
		DeviceInformation(string menu_name, bool expanded);
};

#endif