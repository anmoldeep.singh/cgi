
#include "section_live_data_settings_dlr.h"
#include "widget_checkbox.h"
#include "widget_radio.h"
#include "widget_horizontal_line.h"


LiveDataSettingsDLR::LiveDataSettingsDLR(string menu_name, bool expanded) : Section("Live Data Settings : DLR", menu_name, expanded)
{
	widgets.push_back(new Radio("Display Hex", &Settings::Instance(), "live.dlr.format.radix", "Display Hex"));
	widgets.push_back(new Radio("Display Decimal", &Settings::Instance(), "live.dlr.format.radix", "Display Decimal"));
	widgets.push_back(new Radio("Display Octal", &Settings::Instance(), "live.dlr.format.radix", "Display Octal"));
	widgets.push_back(new Radio("Display Binary", &Settings::Instance(), "live.dlr.format.radix", "Display Binary"));
	widgets.push_back(new HLine());
	widgets.push_back(new Radio("No Bit Grouping", &Settings::Instance(), "live.dlr.format.grouping", "None"));
	widgets.push_back(new Radio("Group 4bit blocks (Hex)", &Settings::Instance(), "live.dlr.format.grouping", "4bits (Hex)"));
	widgets.push_back(new Radio("Group 3bit blocks (Octal)", &Settings::Instance(), "live.dlr.format.grouping", "3bits (Octal)"));
	widgets.push_back(new HLine());
	widgets.push_back(new CheckBox("Base Notation", &Settings::Instance(), "live.dlr.format.base_notation"));
}
