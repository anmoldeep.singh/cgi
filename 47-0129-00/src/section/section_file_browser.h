
#ifndef __SECTION_FILE_BROWSER_H
#define __SECTION_FILE_BROWSER_H

#include "section.h"

class FileBrowser : public Section
{
	public:
		FileBrowser(string name, string mount_point, string menu_name, bool expanded);
};

#endif
