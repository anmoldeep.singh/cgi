
#include "section_env_variables.h"
#include "widget_static_text.h"
#include "widget_ro_text.h"
#include "widget_textbox.h"
#include "access.h"

EnvVariables::EnvVariables(string menu_name, bool expanded) : Section("Environment Variables", menu_name, expanded)
{
	widgets.push_back(new TextBox("Boot Delay (bootdelay)", &Settings::Instance(), "bootdelay"));
	widgets.push_back(new TextBox("Standard Error (stderr)", &Settings::Instance(), "stderr"));
	widgets.push_back(new TextBox("Standard Input (stdin)", &Settings::Instance(), "stdin"));
	widgets.push_back(new TextBox("Standard Output (stdout)", &Settings::Instance(), "stdout"));
	widgets.push_back(new TextBox("TFTP Boot Command (tftp_boot)", &Settings::Instance(), "tftp_boot"));
	widgets.push_back(new TextBox("TFTP Update Command (tftp_update)", &Settings::Instance(), "tftp_update"));
	widgets.push_back(new TextBox("Default Update Command (update)", &Settings::Instance(), "update"));
	widgets.push_back(new TextBox("Console Baudrate (baudrate)", &Settings::Instance(), "baudrate"));
	widgets.push_back(new TextBox("Kernel Boot Arguments (bootargs)", &Settings::Instance(), "bootargs"));
	widgets.push_back(new TextBox("Default Boot Command (bootcmd)", &Settings::Instance(), "bootcmd"));
	widgets.push_back(new TextBox("MMC 1st Partition Boot Command (mmc_boot1)", &Settings::Instance(), "mmc_boot1"));
	widgets.push_back(new TextBox("MMC 2nd Partition Boot Command (mmc_boot2)", &Settings::Instance(), "mmc_boot2"));
	widgets.push_back(new TextBox("MMC 3rd Partition Boot Command (mmc_boot3)", &Settings::Instance(), "mmc_boot3"));
	widgets.push_back(new TextBox("MMC 4th Partition Boot Command (mmc_boot4)", &Settings::Instance(), "mmc_boot4"));

}
