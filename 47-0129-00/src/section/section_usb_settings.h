
#ifndef __SECTION_USB_SETTINGS_H
#define __SECTION_USB_SETTINGS_H

#include "section.h"

class UsbSettings : public Section
{
	public:
		UsbSettings(string menu_name, bool expanded);
};

#endif