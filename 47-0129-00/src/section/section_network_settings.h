
#ifndef __SECTION_NETWORK_SETTINGS_H
#define __SECTION_NETWORK_SETTINGS_H

#include "section.h"

class NetworkSettings : public Section
{
	public:
		NetworkSettings(string menu_name, bool expanded);

		void Apply(char **postvars);
		void Show(char **postvars);

		void GenerateFTPWidgets(void);
		void DeleteFTP(string server_name);
		void CreateFTP(string server_name);

	protected:
		widget_list_t ftp_name_widgets;
		widget_list_t ftp_widgets;
};

#endif