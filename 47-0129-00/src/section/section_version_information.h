
#ifndef __SECTION_VERSION_INFORMATION_H
#define __SECTION_VERSION_INFORMATION_H

#include "section.h"

class VersionInformation : public Section
{
	public:
		VersionInformation(string menu_name, bool expanded);
};

#endif