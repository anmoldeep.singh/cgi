
#include "section_wired_settings.h"
#include "widget_checkbox.h"
#include "widget_radio.h"
#include "widget_spacer.h"
#include "widget_textbox.h"
#include "widget_static_text.h"
#include "widget_ro_text.h"
#include "widget_horizontal_line.h"

WiredSettings::WiredSettings(string menu_name, bool expanded) : Section("Wired Settings", menu_name, expanded)
{
	widgets.push_back(new ReadOnlyTextBox("Hardware Address", &Settings::Instance(), "ethaddr"));
//	widgets.push_back(new CheckBox("Enable Networking", &Settings::Instance(), "net.enable"));
	widgets.push_back(new Spacer());
	widgets.push_back(new Radio("Auto Configure (DHCP)", &Settings::Instance(), "net.dhcp_enable", "1"));
	widgets.push_back(new Radio("Manual Configure", &Settings::Instance(), "net.dhcp_enable", "0"));
	widgets.push_back(new HLine());
	widgets.push_back(new StaticTextBox("Manual Settings", ""));
	widgets.push_back(new TextBox("Static IP Address", &Settings::Instance(), "ipaddr"));
	widgets.push_back(new TextBox("Netmask", &Settings::Instance(), "netmask"));
	widgets.push_back(new TextBox("Gateway", &Settings::Instance(), "gatewayip"));
}
