
#ifndef __SECTION_DISPLAY_SETTINGS_H
#define __SECTION_DISPLAY_SETTINGS_H

#include "section.h"

class DisplaySettings : public Section
{
	public:
		DisplaySettings(string menu_name, bool expanded);
};

#endif
