
#ifndef __SECTION_FLASH_VARIABLES_H
#define __SECTION_FLASH_VARIABLESN_H

#include "section.h"

class FlashVariables : public Section
{
	public:
		FlashVariables(string menu_name, bool expanded);
};

#endif