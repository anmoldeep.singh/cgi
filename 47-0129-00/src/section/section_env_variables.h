
#ifndef __SECTION_ENV_VARIABLES_H
#define __SECTION_ENV_VARIABLES_H

#include "section.h"

class EnvVariables : public Section
{
	public:
		EnvVariables(string menu_name, bool expanded);
};

#endif