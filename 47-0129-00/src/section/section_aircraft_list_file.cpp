
#include "section_aircraft_list_file.h"
#include "widget_spacer.h"
#include "widget_horizontal_line.h"
#include "widget_file_select.h"
#include "widget_button.h"
#include "widget_html_code.h"
#include "system_functions.h"
#include "access.h"
#include "page.h"

#define BTN_DOWNLOAD_AIRCRAFT_LIST	"Download Aircraft List"
#define BTN_UPLOAD_AIRCRAFT_LIST	"Upload Aircraft List"


AircraftListFile::AircraftListFile(string menu_name, bool expanded) : Section("Aircraft Tail Number List", menu_name, expanded)
{
	widgets.push_back(new Spacer());
	widgets.push_back(new Button("Download CSV File", BTN_DOWNLOAD_AIRCRAFT_LIST, REF_DOWNLOAD));
	widgets.push_back(new HLine());
	widgets.push_back(new FileSelect("Select File", REF_FILENAME));
	widgets.push_back(new Button("Upload CSV File", BTN_UPLOAD_AIRCRAFT_LIST, REF_UPLOAD));
	widgets.push_back(new Spacer());
}


void AircraftListFile::Show(char **postvars)
{
	printf("</form>");
	printf("<form action=\"" APPNAME "\" name=\"aircraft_upload\" method=\"post\" enctype=\"multipart/form-data\">");
	PostPersistantVariables();
	Section::Show(postvars);
	printf("</form>");
	printf("<form action=\"" APPNAME "\" method=\"post\">");
}


void AircraftListFile::Apply(char **postvars)
{
	bool download = false;
	bool upload = false;
	char *filename = NULL;
	for (int i = 0; postvars[i]; i += 2)
	{
		if (strcmp(postvars[i], REF_DOWNLOAD) == 0)
		{		
			if (strcmp(postvars[i+1], BTN_DOWNLOAD_AIRCRAFT_LIST) == 0)
				download = true;
		}

		if (strcmp(postvars[i], REF_UPLOAD) == 0)
		{
			if (strcmp(postvars[i+1], BTN_UPLOAD_AIRCRAFT_LIST) == 0)
				upload = true;
		}

		if (strcmp(postvars[i], REF_FILENAME) == 0)
			filename = postvars[i+1];
	}

	if(download)
	{
		system_functions.DownloadAircraft();
	}
	else if(upload)
	{
		//printf("Upload Aircraft File: '%s'", filename);
		if(system_functions.UploadAircraft(filename))
			printf("Success!<br>");
		else
			printf("Failed to upload Aircraft list!<br>");
	}
}
