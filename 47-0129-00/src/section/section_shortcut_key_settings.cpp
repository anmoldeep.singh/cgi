#include <hhmpi/menu_names.h>

#include "section_shortcut_key_settings.h"
#include "widget_combobox.h"

const char *shortcut_target_list[] = {
	MENU_AIRCRAFT_SETTINGS,
	MENU_CLOCK_SETTINGS,
	MENU_CONTROL_SETTINGS,
	MENU_DEVICE_CONFIGURATION,
	MENU_DOWNLOAD_OPERATIONS,
	MENU_FILE_SETTINGS,
	MENU_LIVE_DATA_SETTINGS,
	MENU_LIVE_DATA_VIEW,
	MENU_MANUAL_CONFIGURE,
	MENU_MEMORY_DEVICE,
	MENU_MEMORY_DEVICE_PRIORITY,
	MENU_MEMORY_SETTINGS,
	MENU_NETWORK_OPERATIONS,
	MENU_NETWORK_SETTINGS,
	MENU_FDR_OPERATIONS,
	MENU_SELECT_AIRCRAFT, 
	MENU_SELECT_NOSE,
	MENU_SELECT_TIMEZONE,
	MENU_STORAGE_OPERATIONS,
	MENU_UPLOAD_FILE,
	MENU_USB_SETTINGS,
	DLG_COPY_FILES,
	DLG_DELETE_FILES,
	DLG_FORMAT,
	DLG_INFORMATION,
	DLG_LIVE_DATA_VIEW,
	DLG_MEDIA_DOWNLOAD,
	DLG_RENEW_DHCP_ETH0,
	DLG_RENEW_DHCP_RAUSB0,
	DLG_SEND_FILES,

	DLG_DOWNLOAD_ALL,
	DLG_DOWNLOAD_SINCE_LAST,
	DLG_DOWNLOAD_TIME,
	DLG_DOWNLOAD_MARK,
	DLG_DOWNLOAD_SIZE,
	DLG_DOWNLOAD_DATE,
	DLG_DOWNLOAD_LAST_FLIGHT,
	DLG_CVR_DOWNLOAD_ALL,
	DLG_CVR_RAW_DOWNLOAD_ALL,
	DLG_CVR_DOWNLOAD_SINCE_LAST,
	DLG_CVR_DOWNLOAD_TIME,
	DLG_CVR_DOWNLOAD_MARK,
	DLG_CVR_DOWNLOAD_SIZE,
	DLG_CVR_DOWNLOAD_LAST_FLIGHT,
	DLG_DLR_DOWNLOAD_ALL,
	DLG_DLR_DOWNLOAD_SINCE_LAST,
	DLG_DLR_DOWNLOAD_TIME,
	DLG_DLR_DOWNLOAD_MARK,
	DLG_DLR_DOWNLOAD_SIZE,
	DLG_DLR_DOWNLOAD_LAST_FLIGHT,
	DLG_BIT_DOWNLOAD_ALL,
	DLG_BIT_DOWNLOAD_SINCE_LAST,
	DLG_BIT_DOWNLOAD_TIME,
	DLG_BIT_DOWNLOAD_LAST_FLIGHT,
};

ShortcutKeySettings::ShortcutKeySettings(string menu_name, bool expanded) : Section("Shortcut Key Settings", menu_name, expanded)
{
	widgets.push_back(new ComboBox("Shortcut 1", &Settings::Instance(), "shortcut.1", shortcut_target_list, ARRAY_SIZE(shortcut_target_list)));
	widgets.push_back(new ComboBox("Shortcut 2", &Settings::Instance(), "shortcut.2", shortcut_target_list, ARRAY_SIZE(shortcut_target_list)));
	widgets.push_back(new ComboBox("Shortcut 3", &Settings::Instance(), "shortcut.3", shortcut_target_list, ARRAY_SIZE(shortcut_target_list)));
	widgets.push_back(new ComboBox("Shortcut 4", &Settings::Instance(), "shortcut.4", shortcut_target_list, ARRAY_SIZE(shortcut_target_list)));
}
