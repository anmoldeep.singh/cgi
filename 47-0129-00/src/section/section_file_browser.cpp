
#include "section_file_browser.h"
#include "widget_file_entry.h"

FileBrowser::FileBrowser(string name, string mount_point, string menu_name, bool expanded) : Section(name, menu_name, expanded)
{
	widgets.push_back(new FileEntry(name, mount_point, &Settings::Instance(), ""));
}
