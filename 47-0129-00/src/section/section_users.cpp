
#include "section_users.h"
#include "widget_button.h"
#include "widget_spacer.h"
#include "widget_static_text.h"
#include "widget_edit_username.h"
#include "widget_edit_password.h"
#include "access.h"
#include "page.h"

#define REF_USERS			"users"
#define BTN_USERS_CHANGE_PASSWORD	"Change Password"

Users::Users(string menu_name, bool expanded) : Section("Users", menu_name, expanded)
{
	widgets.push_back(new Spacer());

	widgets.push_back(m_static_textbox = new StaticTextBox("Administrator User", _access.GetUsername()) );
	//widgets.push_back(new EditUsername("Username", _access.GetUsername()) );
	widgets.push_back(new EditPassword("Old Password", "old_password") );
	widgets.push_back(new EditPassword("New Password", "new_password") );
	widgets.push_back(new EditPassword("Repeat new Password", "repeat_password") );
	widgets.push_back(new Button("", BTN_USERS_CHANGE_PASSWORD, REF_USERS));
	widgets.push_back(new Spacer());

}

void Users::Show(char **postvars)
{
	m_static_textbox->SetText(_access.GetUsername());
	printf("</form>");
	printf("<form action=\"/%s\" name=\"users\" method=post>", APPNAME);
	printf("<input type=\"hidden\" name=\"" REF_ACTION "\" value=\"" REF_SET "\">");
	PostPersistantVariables();
	Section::Show(postvars);
	printf("</form>");
	printf("<form action=\"" APPNAME "\" method=\"post\">");
}


void Users::Apply(char **postvars)
{
	bool change_password = false;
	char *old_password = NULL;
	char *new_password = NULL;
	char *repeat_password = NULL;
	string current_password;
	for (int i = 0; postvars[i]; i += 2)
	{
		if (strcmp(postvars[i], REF_USERS) == 0)
		{
			if (strcmp(postvars[i+1], BTN_USERS_CHANGE_PASSWORD) == 0)
				change_password = true;
		}

		if (strcmp(postvars[i], "old_password") == 0)
			old_password = postvars[i+1];
		
		if (strcmp(postvars[i], "new_password") == 0)
			new_password = postvars[i+1];

		if (strcmp(postvars[i], "repeat_password") == 0)
			repeat_password = postvars[i+1];
	}

	if(change_password)
	{
		current_password = Settings::Instance().String("security.web_password");
		if(strcmp(old_password, current_password.c_str()) == 0)
		{
			if(strcmp(new_password, repeat_password) == 0)
			{
				Settings::Instance().String("security.web_password", new_password );
				printf("Changed Administrator Password!<br>");
				Settings::Instance().Save(SECTION_HHMPI);
			}
			else
				printf("New password and Repeat new Password do not match!<br>");
		}
		else
		{
			printf("Old Password incorrect!<br>");
		}
	}
}
