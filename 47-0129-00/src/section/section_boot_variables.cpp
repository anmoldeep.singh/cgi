
#include "section_boot_variables.h"
#include "widget_static_text.h"
#include "widget_ro_text.h"
#include "widget_textbox.h"
#include "access.h"

BootVariables::BootVariables(string menu_name, bool expanded) : Section("Boot Variables", menu_name, expanded)
{
	widgets.push_back(new TextBox("Boot Delay (bootdelay)", &Settings::Instance(), "bootdelay"));
/*
	widgets.push_back(new ReadOnlyTextBox("Standard Error (stderr)", "stderr"));
	widgets.push_back(new ReadOnlyTextBox("Standard Input (stdin)", "stdin"));
	widgets.push_back(new ReadOnlyTextBox("Standard Output (stdout)", "stdout"));
	widgets.push_back(new ReadOnlyTextBox("TFTP Boot Command (tftp_boot)", "tftp_boot"));
	widgets.push_back(new ReadOnlyTextBox("TFTP Update Command (tftp_update)", "tftp_update"));
	widgets.push_back(new ReadOnlyTextBox("Default Update Command (update)", "update"));
	widgets.push_back(new ReadOnlyTextBox("Console Baudrate (baudrate)", "baudrate"));
	widgets.push_back(new ReadOnlyTextBox("Kernel Boot Arguments (bootargs)", "bootargs"));
	widgets.push_back(new ReadOnlyTextBox("Default Boot Command (bootcmd)", "bootcmd"));
	widgets.push_back(new ReadOnlyTextBox("MMC 1st Partition Boot Command (mmc_boot1)", "mmc_boot1"));
	widgets.push_back(new ReadOnlyTextBox("MMC 2nd Partition Boot Command (mmc_boot2)", "mmc_boot2"));
	widgets.push_back(new ReadOnlyTextBox("MMC 3rd Partition Boot Command (mmc_boot3)", "mmc_boot3"));
	widgets.push_back(new ReadOnlyTextBox("MMC 4th Partition Boot Command (mmc_boot4)", "mmc_boot4"));
*/
}
