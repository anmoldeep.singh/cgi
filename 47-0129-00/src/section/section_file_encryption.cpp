#include "section_file_encryption.h"
#include "widget_checkbox.h"

FileEncryption::FileEncryption(string menu_name, bool expanded) : Section("File Encryption", menu_name, expanded)
{
	widgets.push_back(new CheckBox("FDS40-5103 - Use custom encryption key", &Settings::Instance(), "feature.encryption.custom_key"));
	widgets.push_back(new CheckBox("[0001] Dummy", &Settings::Instance(), "feature.cable.0001.encrypted"));
	widgets.push_back(new CheckBox("[0002] Loopback Test", &Settings::Instance(), "feature.cable.0002.encrypted"));
	widgets.push_back(new CheckBox("[0005] Generic Bi-Phase", &Settings::Instance(), "feature.cable.0005.encrypted"));
	widgets.push_back(new CheckBox("[0007] Recorder Emulator", &Settings::Instance(), "feature.cable.0007.encrypted"));
	widgets.push_back(new CheckBox("[0101] Honeywell SSFDR", &Settings::Instance(), "feature.cable.0101.encrypted"));
	widgets.push_back(new CheckBox("[0102] Honeywell SSFDR", &Settings::Instance(), "feature.cable.0102.encrypted"));
	widgets.push_back(new CheckBox("[0103] Honeywell SSCVR", &Settings::Instance(), "feature.cable.0103.encrypted"));
	widgets.push_back(new CheckBox("[0104] Honeywell HFR-5D", &Settings::Instance(), "feature.cable.0104.encrypted"));
	widgets.push_back(new CheckBox("[0105] Honeywell DVDR", &Settings::Instance(), "feature.cable.0105.encrypted"));
	widgets.push_back(new CheckBox("[0106] Honeywell UFDR", &Settings::Instance(), "feature.cable.0106.encrypted"));
	widgets.push_back(new CheckBox("[0107] Honeywell AR COMBI", &Settings::Instance(), "feature.cable.0107.encrypted"));
	widgets.push_back(new CheckBox("[0108] Honeywell HFR-5V", &Settings::Instance(), "feature.cable.0108.encrypted"));
	widgets.push_back(new CheckBox("[0201] L3 F1000", &Settings::Instance(), "feature.cable.0201.encrypted"));
	widgets.push_back(new CheckBox("[0202] L3 FA2100", &Settings::Instance(), "feature.cable.0202.encrypted"));
	widgets.push_back(new CheckBox("[0203] L3 FA5000 / FA5001", &Settings::Instance(), "feature.cable.0203.encrypted"));
	widgets.push_back(new CheckBox("[0204] L3 SRVIVR", &Settings::Instance(), "feature.cable.0204.encrypted"));
	widgets.push_back(new CheckBox("[0205] L3 A200S", &Settings::Instance(), "feature.cable.0205.encrypted"));
	widgets.push_back(new CheckBox("[0301] Smiths VADR 3-Card", &Settings::Instance(), "feature.cable.0301.encrypted"));
	widgets.push_back(new CheckBox("[0302] Smiths VADR 4-Card", &Settings::Instance(), "feature.cable.0302.encrypted"));
	widgets.push_back(new CheckBox("[0401] BASE MCR500 / SCR500 (active cable variant)", &Settings::Instance(), "feature.cable.0401.encrypted"));
	widgets.push_back(new CheckBox("[0402] BASE MCR500 / SCR500 (passive cable variant)", &Settings::Instance(), "feature.cable.0402.encrypted"));
	widgets.push_back(new CheckBox("[0501] Universal CVFDR", &Settings::Instance(), "feature.cable.0501.encrypted"));
	widgets.push_back(new CheckBox("[0601] FDS CPIFDR", &Settings::Instance(), "feature.cable.0601.encrypted"));
	widgets.push_back(new CheckBox("[0701] Lockheed L209", &Settings::Instance(), "feature.cable.0701.encrypted"));
	widgets.push_back(new CheckBox("[0801] Penny &amp; Giles MPFR", &Settings::Instance(), "feature.cable.0801.encrypted"));
	widgets.push_back(new CheckBox("[0802] Penny &amp; Giles 91005", &Settings::Instance(), "feature.cable.0802.encrypted"));
	widgets.push_back(new CheckBox("[0901] FDS DCU", &Settings::Instance(), "feature.cable.0901.encrypted"));
	widgets.push_back(new CheckBox("[0902] FDS DCU2", &Settings::Instance(), "feature.cable.0902.encrypted"));
	widgets.push_back(new CheckBox("[0903] FDS Sentry", &Settings::Instance(), "feature.cable.0903.encrypted"));
	widgets.push_back(new CheckBox("[10670001] L-3 u-QAR", &Settings::Instance(), "feature.cable.10670001.encrypted"));
}
