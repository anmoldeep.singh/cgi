
#ifndef __SECTION_LIVE_DATA_SETTINGS_429_STANDARD_H
#define __SECTION_LIVE_DATA_SETTINGS_429_STANDARD_H

#include "section.h"

class LiveDataSettings429Standard : public Section
{
	public:
		LiveDataSettings429Standard(string menu_name, bool expanded);
};

#endif