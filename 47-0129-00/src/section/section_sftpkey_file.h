
#ifndef __SECTION_SFTP_KEY_FILE_H
#define __SECTION_SFTP_KEY_FILE_H

#include "section.h"

class SFTPKeyFile : public Section
{
	public:
		SFTPKeyFile(string menu_name, bool expanded);
		void Show(char **postvars);
		void Apply(char **postvars);
	private:
		char *update_server_entry_list[128];
		Settings dummy_settings;
};

#endif