#include <stdint.h>

#include "section_auto_upload_settings.h"
#include "widget_combobox.h"
#include "widget_checkbox.h"



char *server_entry_list[128];


AutoUploadSettings::AutoUploadSettings(string menu_name, bool expanded) : Section("Auto Upload Settings", menu_name, expanded)
{
	uint32_t server_count = 0;

        SettingsMap& settings_map = Settings::Instance().GetSettingsMap();

        list<string> id_list;

        string previous_name;

        SettingsMap::iterator it;

        for (it = settings_map.begin();
                        it != settings_map.end();
                        it++)
        {
                string identifier = it->first;
                string value = it->second;

                if (identifier.substr(0, 4) != "ftp.")
                        continue;

                size_t pos = identifier.substr(4).find(".");

                if (pos == string::npos)
                        continue;

                string this_name = identifier.substr(4, pos);

                if (this_name != previous_name)
                {
                        previous_name = this_name;
                        id_list.push_back(this_name);
                }
        }

        list<string>::iterator jt;

        for (jt = id_list.begin();
                        jt != id_list.end();
                        jt++)
        {
                string& identifier = *jt;

		server_entry_list[server_count] = new char[ identifier.length() + 1 ];
		snprintf(server_entry_list[server_count], identifier.length() + 1, "%s", identifier.c_str());
		server_count++;
	}

	widgets.push_back(new CheckBox("Automatically Upload Data when Wired Network Available", &Settings::Instance(), "ftp.auto_upload"));
	widgets.push_back(new ComboBox("Upload to FTP Server", &Settings::Instance(), "ftp.auto_ftp_server", (const char **) server_entry_list, server_count));
	widgets.push_back(new CheckBox("Skip Uploaded Data Verification", &Settings::Instance(), "ftp.skip_data_verify"));
	widgets.push_back(new CheckBox("Automatically Erase after Upload", &Settings::Instance(), "ftp.auto_erase"));
}

