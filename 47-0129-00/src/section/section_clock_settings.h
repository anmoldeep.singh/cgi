
#ifndef __SECTION_CLOCK_SETTINGS_H
#define __SECTION_CLOCK_SETTINGS_H

#include "section.h"

class ClockSettings : public Section
{
	public:
		ClockSettings(string menu_name, bool expanded);
};

#endif