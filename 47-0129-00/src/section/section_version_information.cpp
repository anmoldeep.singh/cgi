#include <hhmpi/hhmpi_fpgarev.h>

#include "section_version_information.h"
#include "widget_static_text.h"
#include "widget_ro_text.h"
#include "system_functions.h"

VersionInformation::VersionInformation(string menu_name, bool expanded) : Section("Version Information", menu_name, expanded)
{
	struct fpga_revision info;

	widgets.push_back(new ReadOnlyTextBox("Hardware", &Settings::Instance(), "version.hw"));
	widgets.push_back(new StaticTextBox("FPGA", "r" + toString(hhmpi_fpga_get_revision(&info))));
	widgets.push_back(new StaticTextBox("Kernel + App (OS)", Settings::Instance().String("version.os")));
}
