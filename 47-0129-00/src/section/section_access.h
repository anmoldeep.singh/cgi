
#ifndef __SECTION_ACCESS_H
#define __SECTION_ACCESS_H

#include "section.h"

class AccessRestrictions : public Section
{
	public:
		AccessRestrictions(string menu_name, bool expanded);
};

#endif