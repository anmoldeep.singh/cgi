
#include "section.h"
#include "widget.h"
#include "lists.h"
//#include "buttons.h"

Section::Section(string name, string menu_name, bool expanded)
{
	m_name = name;
	m_menu_name = menu_name;
	m_expanded = expanded;
}

void Section::Head(void)
{
	Lists<Widget> widget_list(widgets);
	Widget *widget = widget_list.FirstItem();
	while(widget)
	{
		widget->Head();
		widget = widget_list.NextItem();
	}
}

void Section::Show(char **postvars)
{
	for (int i = 0; postvars[i]; i += 2)
	{
		if ( m_name == postvars[i] )
			if(strcmp(postvars[i+1], "+") == 0)
				m_expanded = true;
			if(strcmp(postvars[i+1], "-") == 0)
				m_expanded = false;
	}

	printf("<ul class=\"tree_expand\">");
	printf("<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" width=\"100%%\" align=\"center\">");
	printf("<tr>");
	printf("<td colspan=\"2\" width=\"100%%\" class=\"cell_heading_sml\">");

	if(m_expanded)
		printf("<input type=submit name=\"%s\" value=\"-\" class=\"button_logout\">", m_name.c_str());
	else
		printf("<input type=submit name=\"%s\" value=\"+\" class=\"button_logout\">", m_name.c_str());
	printf(" %s</td>", m_name.c_str());
	printf("</tr>");
	printf("</table>");
	printf("</ul>");
	if(m_expanded)
		printf("<ul class=\"tree_expand\">");
	else
		printf("<ul class=\"tree_collapse\">");
	printf("<table border=\"0\" cellpadding=\"4\" cellspacing=\"0\" width=\"100%%\" align=\"center\">");
	Lists<Widget> widget_list(widgets);
	Widget *widget = widget_list.FirstItem();
	while(widget)
	{
		widget->Show();
		widget = widget_list.NextItem();
	}
	printf("<tr><td align=\"center\" width=\"100%%\" colspan=\"2\" class=\"fieldcell\">");
	printf("</td></tr>");
	printf("</table>");
	printf("</ul>");
}


void Section::Apply(char **postvars)
{
	Lists<Widget> widget_list(widgets);
	Widget *widget = widget_list.FirstItem();
	while(widget)
	{
		widget->Set(postvars);
		widget = widget_list.NextItem();
	}
}


