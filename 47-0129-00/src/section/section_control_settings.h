
#ifndef __SECTION_CONTROL_SETTINGS_H
#define __SECTION_CONTROL_SETTINGS_H

#include "section.h"

class ControlSettings : public Section
{
	public:
		ControlSettings(string menu_name, bool expanded);
};

#endif