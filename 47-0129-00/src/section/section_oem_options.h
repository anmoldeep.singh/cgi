
#ifndef __SECTION_OEM_OPTIONS_H
#define __SECTION_OEM_OPTIONS_H

#include "section.h"

class OEMOptions : public Section
{
	public:
		OEMOptions(string menu_name, bool expanded);
		virtual void Apply(char **postvars);
};

#endif