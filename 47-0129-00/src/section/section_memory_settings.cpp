
#include <sstream>
#include "section_memory_settings.h"
#include "widget_combobox.h"
#include "widget_horizontal_line.h"

const char *memory_settings_list[] = {
	"Ask for Device",
	"Use Priority List",
	"Use Specific Device",
};


const char *memory_device_list[] = {
	"ATA Card",
	"CF Card",
	"Internal",
	"SD Card",
	"USB Flash Drive",
};

#define MEMORY_DEVICE_LIST_SIZE		(sizeof(memory_device_list)/sizeof(const char *))


MemorySettings::MemorySettings(string menu_name, bool expanded) : Section("Memory Settings", menu_name, expanded)
{
	widgets.push_back(new ComboBox("Memory Selection", &Settings::Instance(), "memory_device.selection", memory_settings_list, ARRAY_SIZE(memory_settings_list)));
	widgets.push_back(new HLine());
	widgets.push_back(new ComboBox("Specific Device", &Settings::Instance(), "memory_device", memory_device_list, ARRAY_SIZE(memory_device_list)));
	widgets.push_back(new HLine());
	for(unsigned int i = 0; i < MEMORY_DEVICE_LIST_SIZE; i++)
	{
		std::stringstream priority_name;
		std::stringstream priority_ref;
		priority_name << "Priority Device ";
		priority_name << (i+1);
		priority_ref << "memory_device.priority_";
		priority_ref << (i);
		widgets.push_back(new ComboBox(priority_name.str(), &Settings::Instance(), priority_ref.str(), memory_device_list, ARRAY_SIZE(memory_device_list)));
	}	
}

void MemorySettings::Apply(char **postvars)
{
	string memory_device_priority[MEMORY_DEVICE_LIST_SIZE];

	for (int i = 0; postvars[i]; i += 2)
        {
                //printf("postvars[i]: %s<br>\n", postvars[i]);
                //printf("postvars[i+1]: %s<br>\n", postvars[i+1]);

		for (int j = 0; j < MEMORY_DEVICE_LIST_SIZE; j++)
		{
			std::stringstream priority_ref;
			priority_ref << "memory_device.priority_" << (j);

			if (strcmp(postvars[i], priority_ref.str().c_str()) == 0)
				memory_device_priority[j] = postvars[i+1];
		}
        }

	bool duplicate = false;

	for (int j = 0; j < MEMORY_DEVICE_LIST_SIZE; j++)
	{
		int count = 0;

		for (int i = 0; i < MEMORY_DEVICE_LIST_SIZE; i++)
			if (memory_device_priority[i] == memory_device_priority[j])
				count++;

		if (count > 1)
			duplicate = true;
	}

	if (duplicate)
	{
		Settings& settings = Settings::Instance();
        	//settings.String("ftp." + server_name + ".ipaddr", "0.0.0.0");

		printf("<p class=\"operation_status_area\">");
        	printf("ERROR: Memory settings priority list contains duplicate devices, please correct and try again.");
	        printf("</p>");
	}
	else
        	Section::Apply(postvars);
}

