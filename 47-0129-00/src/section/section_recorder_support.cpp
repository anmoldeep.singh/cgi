
#include "section_recorder_support.h"
#include "widget_checkbox.h"

RecorderSupport::RecorderSupport(string menu_name, bool expanded) : Section("Recorder Manufacturer Support", menu_name, expanded)
{
	widgets.push_back(new CheckBox("[0001] Dummy", &Settings::Instance(), "feature.cable.0001"));
	widgets.push_back(new CheckBox("[0002] Loopback Test", &Settings::Instance(), "feature.cable.0002"));
	widgets.push_back(new CheckBox("[0005] Generic Bi-Phase", &Settings::Instance(), "feature.cable.0005"));
	widgets.push_back(new CheckBox("[0007] Recorder Emulator", &Settings::Instance(), "feature.cable.0007"));
	widgets.push_back(new CheckBox("[0101] FDS40-5022 - Honeywell SSFDR", &Settings::Instance(), "feature.cable.0101"));
//	widgets.push_back(new CheckBox("[0102] FDS40-5022 - Honeywell SSFDR", &Settings::Instance(), "feature.cable.0102"));
	widgets.push_back(new CheckBox("[0103] FDS40-5020 - Honeywell SSCVR", &Settings::Instance(), "feature.cable.0103"));
	widgets.push_back(new CheckBox("[0104] FDS40-5024 - Honeywell HFR-5D", &Settings::Instance(), "feature.cable.0104"));
	widgets.push_back(new CheckBox("[0105] FDS40-5023 - Honeywell DVDR", &Settings::Instance(), "feature.cable.0105"));
	widgets.push_back(new CheckBox("[0106] FDS40-5025 - Honeywell UFDR", &Settings::Instance(), "feature.cable.0106"));
	widgets.push_back(new CheckBox("[0107] FDS40-5026 - Honeywell AR COMBI", &Settings::Instance(), "feature.cable.0107"));
	widgets.push_back(new CheckBox("[0108] FDS40-5027 - Honeywell HFR-5V", &Settings::Instance(), "feature.cable.0108"));
	widgets.push_back(new CheckBox("[0201] FDS40-5014 - L3 F1000", &Settings::Instance(), "feature.cable.0201"));
	widgets.push_back(new CheckBox("[0202] FDS40-5011 - L3 FA2100 FDR", &Settings::Instance(), "feature.cable.0202.fdr"));
	widgets.push_back(new CheckBox("[0202] FDS40-5012 - L3 FA2100 CVR", &Settings::Instance(), "feature.cable.0202.cvr"));
	widgets.push_back(new CheckBox("[0203] FDS40-5015 - L3 FA5000 / FA5001", &Settings::Instance(), "feature.cable.0203"));
	widgets.push_back(new CheckBox("[0204] FDS40-5016 - L3 SRVIVR", &Settings::Instance(), "feature.cable.0204"));
	widgets.push_back(new CheckBox("[0205] FDS40-5031 - L3 A200S", &Settings::Instance(), "feature.cable.0205"));
	widgets.push_back(new CheckBox("[0301] FDS40-5050 - Smiths VADR 3-Card", &Settings::Instance(), "feature.cable.0301"));
	widgets.push_back(new CheckBox("[0302] FDS40-5050 - Smiths VADR 4-Card", &Settings::Instance(), "feature.cable.0302"));
	widgets.push_back(new CheckBox("[0401] FDS40-5040 - BASE MCR500 / SCR500 (active cable variant)", &Settings::Instance(), "feature.cable.0401"));
	widgets.push_back(new CheckBox("[0402] FDS40-5040 - BASE MCR500 / SCR500 (passive cable variant)", &Settings::Instance(), "feature.cable.0402"));
	widgets.push_back(new CheckBox("[0501] FDS40-5030 - Universal CVFDR", &Settings::Instance(), "feature.cable.0501"));
	widgets.push_back(new CheckBox("[0502] FDS40-5052 - UASC 1603 CVR Interface", &Settings::Instance(), "feature.cable.0502"));
	widgets.push_back(new CheckBox("[0601] FDS CPIFDR", &Settings::Instance(), "feature.cable.0601"));
	widgets.push_back(new CheckBox("[0701] FDS40-5051 - Lockheed L209", &Settings::Instance(), "feature.cable.0701"));
	widgets.push_back(new CheckBox("[0801] FDS40-5029 - Penny &amp; Giles MPFR", &Settings::Instance(), "feature.cable.0801"));
	widgets.push_back(new CheckBox("[0802] FDS40-5028 - Penny &amp; Giles 91005", &Settings::Instance(), "feature.cable.0802"));
	widgets.push_back(new CheckBox("[0901] FDS DCU", &Settings::Instance(), "feature.cable.0901"));
	widgets.push_back(new CheckBox("[0902] FDS DCU2", &Settings::Instance(), "feature.cable.0902"));
	widgets.push_back(new CheckBox("[0903] FDS40-5053 - FDS Sentry FDR", &Settings::Instance(), "feature.cable.0903.fdr"));
	widgets.push_back(new CheckBox("[0903] FDS40-5054 - FDS Sentry CVR", &Settings::Instance(), "feature.cable.0903.cvr"));
	widgets.push_back(new CheckBox("[10670001] FDS40-5017 - L-3 u-QAR", &Settings::Instance(), "feature.cable.10670001"));
}

