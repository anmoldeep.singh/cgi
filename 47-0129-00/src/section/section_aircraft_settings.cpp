
#include "section_aircraft_settings.h"
#include "widget_checkbox.h"

AircraftSettings::AircraftSettings(string menu_name, bool expanded) : Section("Aircraft Settings", menu_name, expanded)
{
	widgets.push_back(new CheckBox("Prompt for Tail Number", &Settings::Instance(), "aircraft.prompt"));
	widgets.push_back(new CheckBox("Force Selection of Aircraft Prior to Download", &Settings::Instance(), "aircraft.force"));
	widgets.push_back(new CheckBox("Unselect Aircraft after Successful Download", &Settings::Instance(), "aircraft.unselect"));
}

