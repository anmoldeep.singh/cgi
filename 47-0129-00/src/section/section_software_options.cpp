
#include "section_software_options.h"
#include "widget_checkbox.h"

SoftwareOptions::SoftwareOptions(string menu_name, bool expanded) : Section("Software Options", menu_name, expanded)
{
	if (Settings::Instance().Bool("feature.cable.0103"))
		widgets.push_back(new CheckBox("FDS40-5021 - SSCVR Wavefile Demultiplex", &Settings::Instance(), "feature.sscvr_decompression"));

	if (Settings::Instance().Bool("feature.cable.0202.cvr"))
		widgets.push_back(new CheckBox("FDS40-5013 - FA2100 Wavefile Demultiplex", &Settings::Instance(), "feature.fa2100_decompression"));

	if (Settings::Instance().Bool("feature.cable.0108"))
		widgets.push_back(new CheckBox("FDS40-5033 - HFR-5V Wavefile Demultiplex", &Settings::Instance(), "feature.hfr5v_decompression"));

	if (Settings::Instance().Bool("feature.cable.0801"))
		widgets.push_back(new CheckBox("FDS40-5034 - MPFR Wavefile Demultiplex", &Settings::Instance(), "feature.mpfr_decompression"));

	if (Settings::Instance().Bool("feature.cable.0105"))
		widgets.push_back(new CheckBox("FDS40-5036 - DVDR Wavefile Demultiplex", &Settings::Instance(), "feature.dvdr_decompression"));

	widgets.push_back(new CheckBox("FDS40-5102 - Engineering Units", &Settings::Instance(), "feature.eu.present"));
}

