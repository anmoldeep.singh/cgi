
#ifndef __SECTION_SECURITY_KEY_H
#define __SECTION_SECURITY_KEY_H

#include "section.h"

class SecurityKey : public Section
{
	public:
		SecurityKey(string menu_name, bool expanded);

		//void Show(char **postvars);
		void Apply(char **postvars);
};

#endif