
#include "section_usb_settings.h"
#include "widget_radio.h"


UsbSettings::UsbSettings(string menu_name, bool expanded) : Section("USB Settings", menu_name, expanded)
{
	widgets.push_back(new Radio("Mass Storage Device", &Settings::Instance(), "usb_device", "Mass Storage Device"));
	widgets.push_back(new Radio("MPI Device", &Settings::Instance(), "usb_device", "MPI Device"));
	widgets.push_back(new Radio("Auto select", &Settings::Instance(), "usb_device", "Auto"));
	widgets.push_back(new Radio("Ask on insertion", &Settings::Instance(), "usb_device", "Ask"));
	widgets.push_back(new Radio("Disabled", &Settings::Instance(), "usb_device", "None"));
}
