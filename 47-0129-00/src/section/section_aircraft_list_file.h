
#ifndef __SECTION_AIRCRAFT_LIST_FILE_H
#define __SECTION_AIRCRAFT_LIST_FILE_H

#include "section.h"

class AircraftListFile : public Section
{
	public:
		AircraftListFile(string menu_name, bool expanded);
		void Show(char **postvars);
		void Apply(char **postvars);
};

#endif