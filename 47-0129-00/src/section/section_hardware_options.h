
#ifndef __SECTION_HARDWARE_OPTIONS_H
#define __SECTION_HARDWARE_OPTIONS_H

#include "section.h"

class HardwareOptions : public Section
{
	public:
		HardwareOptions(string menu_name, bool expanded);
};

#endif