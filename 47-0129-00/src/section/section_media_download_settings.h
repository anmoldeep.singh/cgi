
#ifndef __SECTION_MEDIA_DOWNLOAD_SETTINGS_H
#define __SECTION_MEDIA_DOWNLOAD_SETTINGS_H

#include "section.h"

class MediaDownloadSettings : public Section
{
	public:
		MediaDownloadSettings(string menu_name, bool expanded);
};

#endif
