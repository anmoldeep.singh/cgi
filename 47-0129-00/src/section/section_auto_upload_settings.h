
#ifndef __SECTION_AUTO_UPLOAD_SETTINGS_H
#define __SECTION_AUTO_UPLOAD_SETTINGS_H

#include "section.h"

class AutoUploadSettings : public Section
{
	public:
		AutoUploadSettings(string menu_name, bool expanded);
};

#endif
