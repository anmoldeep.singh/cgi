#include <hhmpi/settings.h>

#include "section_tools.h"
#include "widget_button.h"
#include "widget_spacer.h"
#include "system_functions.h"
#include "page.h"

#define REF_TOOLS		"tools"
#define BTN_TOOLS_RESET		"Reset"
#define BTN_TOOLS_DEFAULTS	"Defaults"

Tools::Tools(string menu_name, bool expanded) : Section("Tools", menu_name, expanded)
{
	widgets.push_back(new Spacer());
	widgets.push_back(new Button("Reset Device", BTN_TOOLS_RESET, REF_TOOLS));
	widgets.push_back(new Spacer());
	widgets.push_back(new Button("Restore Factory Default Configuration", BTN_TOOLS_DEFAULTS, REF_TOOLS));
	widgets.push_back(new Spacer());

}

void Tools::Show(char **postvars)
{
	printf("</form>");
	printf("<form action=\"/%s\" name=\"tools\" method=post>", APPNAME);
	printf("<input type=\"hidden\" name=\"" REF_ACTION "\" value=\"" REF_SET "\">");
	PostPersistantVariables();
	Section::Show(postvars);
	printf("</form>");
	printf("<form action=\"" APPNAME "\" method=\"post\">");
}


void Tools::Apply(char **postvars)
{
	bool reset = false;
	bool defaults = false;
	for (int i = 0; postvars[i]; i += 2)
	{
		if (strcmp(postvars[i], REF_TOOLS) == 0)
		{
			if (strcmp(postvars[i+1], BTN_TOOLS_RESET) == 0)
				reset = true;
			else if (strcmp(postvars[i+1], BTN_TOOLS_DEFAULTS) == 0)
				defaults = true;
		}
	}

	if(reset)
	{
		printf("Reseting Device...<br>");
		printf("Refresh page after device reboots to login again.<br>");
		//sleep(1);
		if(!SystemFunctions::ResetDevice())
			printf("Error while trying to assert reset line - try manual reset.<br>");
	}
	else if(defaults)
	{
		printf("Restoring Factory Defaults...<br>");
		SystemFunctions::FactoryDefaults();
		printf("<br>A device reset is required to finalise operation...<br>");
	}
}
