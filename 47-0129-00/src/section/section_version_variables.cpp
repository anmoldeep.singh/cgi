
#include "section_version_variables.h"
#include "widget_static_text.h"
#include "widget_ro_text.h"
#include "widget_textbox.h"
#include "access.h"


VersionVariables::VersionVariables(string menu_name, bool expanded) : Section("Version Variables", menu_name, expanded)
{
	widgets.push_back(new TextBox("Hardware", &Settings::Instance(), "version.hw"));
}
