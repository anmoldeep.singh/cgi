
#ifndef __SECTION_SOFTWARE_OPTIONS_H
#define __SECTION_SOFTWARE_OPTIONS_H

#include "section.h"

class SoftwareOptions : public Section
{
	public:
		SoftwareOptions(string menu_name, bool expanded);
};

#endif
