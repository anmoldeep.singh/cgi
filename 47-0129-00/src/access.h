
#ifndef __ACCESS_H
#define __ACCESS_H

#include "common.h"

class Access
{
	public:
		Access(void);
		virtual ~Access(void) {}
		void SetUser(string username);
		bool Login(string username, string password);
		void Logout(void);
		void CreateSessionID(void);
		bool CheckSessionID(string session_id);
		void UpdateSession(void);
		void SetSessionID(string session_id);
		bool LoggedIn(void);
		bool Attempted(void);
		string GetUsername(void);
		string GetPassword(void);
		string GetSessionID(void);

	protected:
		bool m_logged_in;
		bool m_attempted;
		string m_username;
		string m_password;
		string admin_password;
		string m_session_id;
};

extern Access _access;

#endif
