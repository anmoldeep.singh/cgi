/* cgivars.c
 * (C) Copyright 2000, Moreton Bay (http://www.moretonbay.com).
 * see HTTP (www.w3.org) and RFC
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/stdcheaders.h>

#include "common.h"

#include "cgivars.h"

/* local function prototypes */
char hex2char(char *hex);
void unescape_url(char *url);
char x2c(char *what);

/* hex2char */
/* RFC */
char hex2char(char *hex) {
	char char_value;
	char_value = (hex[0] >= 'A' ? ((hex[0] & 0xdf) - 'A') + 10 : (hex[0] - '0'));
	char_value *= 16;
	char_value += (hex[1] >= 'A' ? ((hex[1] & 0xdf) - 'A') + 10 : (hex[1] - '0'));
	return char_value;
}

/* unescape_url */
/* RFC */
void unescape_url(char *url) {
	int n, k;
	for(n=0, k=0;url[k];++n, ++k) {
		if((url[n] = url[k]) == '%') {
			url[n] = hex2char(&url[k+1]);
			k += 2;
		}
	}
	url[n] = '\0';
}

char *strdup(char *string)
{
	char *temp = new char [ strlen(string) + 1];
	strcpy(temp, string);
	return temp;
}


/* getRequestMethod
 * retn:	from_method (GET or POST) on success,
 *			-1 on failure.  */
int getRequestMethod() {
	char *request_method;
	int form_method;

	request_method = getenv("REQUEST_METHOD");
	if(request_method == NULL)
		return -1;

	if (!strcmp(request_method, "GET") || !strcmp(request_method, "HEAD") ) {
		form_method = GET;
	} else if (!strcmp(request_method, "POST")) {
		form_method = POST;
	} else {
		/* wtf was it then?!! */
		return -1;
	}
	return form_method;
}


/* getGETvars
 * retn:	getvars */
char **getGETvars() {
	int i;
	char **getvars;
	char *getinput;
	char **pairlist;
	int paircount = 0;
	char *nvpair;
	char *eqpos;

	getinput = getenv("QUERY_STRING");
	if (getinput)
		getinput = strdup(getinput);

	/* Change all plusses back to spaces */
   	for(i=0; getinput && getinput[i]; i++)
		if(getinput[i] == '+')
			getinput[i] = ' ';

   	pairlist = (char **) malloc(256*sizeof(char **));
	paircount = 0;
   	nvpair = getinput ? strtok(getinput, "&") : NULL;
	while (nvpair) {
		pairlist[paircount++]= strdup(nvpair);
        	if(!(paircount%256))
			pairlist = (char **) realloc(pairlist,(paircount+256)*sizeof(char **));
       		nvpair = strtok(NULL, "&");
	}

   	pairlist[paircount] = 0;
   	getvars = (char **) malloc((paircount*2+1)*sizeof(char **));
	for (i= 0; i<paircount; i++) {
		if((eqpos=strchr(pairlist[i], '='))) {
       	    		*eqpos = '\0';
            		unescape_url(getvars[i*2+1] = strdup(eqpos+1));
   	    	} else {
			unescape_url(getvars[i*2+1] = strdup(""));
        	}
		unescape_url(getvars[i*2] = strdup(pairlist[i]));
    	}
   	getvars[paircount*2] = 0;
    	for(i=0;pairlist[i];i++)
		free(pairlist[i]);
	free(pairlist);
	if (getinput)
		free(getinput);
	return getvars;
}


/* getPOSTvars
 * retn:	postvars */
char **getPOSTvars() {
	int i;
	int content_length;
	char **postvars = NULL;
	char *postinput;
	char **pairlist;
	int paircount = 0;
	char *nvpair;
	char *eqpos;
	
	postinput = getenv("CONTENT_LENGTH");
	if (!postinput)
		exit(1);
	if(!(content_length = atoi(postinput)))
		exit(1);
	if(!(postinput = (char *) malloc(content_length+1)))
		exit(1);
	if (!fread(postinput, content_length, 1, stdin))
		exit(1);
	postinput[content_length] = '\0';

	char *content_type = getenv("CONTENT_TYPE");	

/*
	FILE *out = fopen("/tmp/log.txt", "wb");
	fprintf(out, "Content-Length: %d\n", content_length);
	fprintf(out, "Content-Type: %s\n", content_type);

   	for(i=0;i < content_length;i++)
		fprintf(out, "%c", postinput[i]);
	fclose(out);
*/

	pairlist = (char **) malloc(256*sizeof(char **));
	paircount = 0;

	if (strcmp("application/x-www-form-urlencoded", content_type) == 0)
	{
		for(i=0;postinput[i];i++)
			if(postinput[i] == '+')
				postinput[i] = ' ';
	
		nvpair = strtok(postinput, "&");
		while (nvpair) {
			pairlist[paircount++] = strdup(nvpair);
			if(!(paircount%256))
					pairlist = (char **) realloc(pairlist, (paircount+256)*sizeof(char **));
			nvpair = strtok(NULL, "&");
		}
		
		pairlist[paircount] = 0;
		postvars = (char **) malloc((paircount*2+1)*sizeof(char **));
		for(i = 0;i<paircount;i++) {
				if((eqpos = strchr(pairlist[i], '='))) {
						*eqpos= '\0';
					unescape_url(postvars[i*2+1] = strdup(eqpos+1));
				} else {
						unescape_url(postvars[i*2+1] = strdup(""));
			}
				unescape_url(postvars[i*2]= strdup(pairlist[i]));
		}
		postvars[paircount*2] = 0;
	
		for(i=0;pairlist[i];i++)
			free(pairlist[i]);
		free(pairlist);
		free(postinput);
	
		return postvars;
	}
	else
	if (strncmp("multipart/form-data; boundary=", content_type, 30) == 0)
	{
		unsigned int pair_count = 256;

		postvars = (char **) malloc((pair_count*2+1)*sizeof(char **));

		char *boundary = content_type + 30;
		char *buffer = postinput;

#ifdef MULTI_PART_DEBUG
		FILE *out2 = fopen("/tmp/www.log", "wb");
		fprintf(out2, "boundary: \"%s\"\n", boundary);
		fprintf(out2, "paircount: %d\n", paircount);
#endif

		while ((int) ((int) (buffer - postinput) + strlen(boundary)) < ((int) content_length))
		{
			char *header = strstr(buffer, boundary);

			if (!header)
				break;

			buffer += strlen(boundary);

			char *eoh = strstr(header, "\r\n\r\n");

			if (eoh)
			{
#ifdef MULTI_PART_DEBUG
				fprintf(out2, "eoh found: 4 LR CR LR CR\n");
#endif
				eoh[0] = eoh[1] = eoh[2] = eoh[3] = 0;
				buffer = eoh + 4;
			}

			char *next_header = buffer;
			while ((int) ((int) (next_header - postinput) + strlen(boundary)) < ((int) content_length) && strncmp(next_header, boundary, strlen(boundary)) != 0)
				next_header++;

			unsigned int len = content_length - (unsigned int) (buffer - postinput);
			if (next_header)
				len = (unsigned int) (next_header - buffer) - 4;
	
#ifdef MULTI_PART_DEBUG
			fprintf(out2, "len: %d\n", len);
#endif

			char *filename = strstr(header, " filename=\"");

			if (filename)
			{
				filename += 11;
				char *p = strstr(filename, "\n");
				if (p) *p = 0;
				p = strstr(filename, "\"");
				if (p) *p = 0;

#ifdef MULTI_PART_DEBUG
				fprintf(out2, "saving file: %s\n", filename);
#endif
				
				char fn[128];
				sprintf(fn, "%s/%s", UPLOAD_DIRECTORY, filename);
				FILE *dump = fopen(fn, "wb");
				if (dump)
				{
					for (unsigned int offset = 0; offset < len; )
					{
						int amount = (len - offset) > 128 ? 128 : (len - offset);
						offset += fwrite(buffer + offset, sizeof(unsigned char), amount, dump);
					}
					fclose(dump);
				}
			}

			char *name = strstr(header, " name=\"");

			if (name)
			{
				name += 7;
				char *p = strstr(name, "\n");
				if (p) *p = 0;
				p = strstr(name, "\"");
				if (p) *p = 0;

#ifdef MULTI_PART_DEBUG
				fprintf(out2, "paircount[%d]: name: %s\n", paircount, name);
#endif
				postvars[paircount*2 + 0] = strdup(name);

				if (filename == NULL)
				{
					p = strstr(buffer, "\n");
					if (p) *p = 0;
					p = strstr(buffer, "\r");
					if (p) *p = 0;
#ifdef MULTI_PART_DEBUG
					fprintf(out2, "paircount[%d]: value: %s\n", paircount, buffer);
#endif
					postvars[paircount*2 + 1] = strdup(buffer);
				}
				else
				{
#ifdef MULTI_PART_DEBUG
					fprintf(out2, "paircount[%d]: value: %s\n", paircount, filename);
#endif
					postvars[paircount*2 + 1] = strdup(filename);
				}

				paircount++;
			}

			buffer = next_header;
		}

#ifdef MULTI_PART_DEBUG
		fclose(out2);
#endif
	}
	else
		postvars = (char **) malloc((paircount*2+1)*sizeof(char **));

	postvars[paircount*2] = 0;


	return postvars;
}

/* cleanUp
 * free the mallocs */
int cleanUp(int form_method, char **getvars, char **postvars) {
	int i;

	if (postvars) {
		for(i=0;postvars[i];i++)
			free(postvars[i]);
		free(postvars);
	}
	if (getvars) {
		for(i=0;getvars[i];i++)
			free(getvars[i]);
		free(getvars);
	}

	return 0;
}
