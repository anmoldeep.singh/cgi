/* htmllib.c
 * HTML common library functions for the CGI programs. */

#include <stdio.h>
#include "htmllib.h"


void htmlHeader(const char *title) {
  printf("Content-type: text/html\n\r\n\r<HTML><HEAD><TITLE>%s</TITLE></HEAD>",
		  title);
}

void binaryHeader(const char *filename, unsigned int length)
{
	printf("Content-Disposition: attachment; filename=%s\r\n", filename);
	printf("Content-Type: application/x-unknown\r\n");
	printf("Content-Length: %d\r\n", length);
	printf("\r\n");
}

void htmlBody() {
    printf("<BODY>");
}

void htmlFooter() {
    printf("</BODY></HTML>");
}

void addTitleElement(const char *title) {
	printf("<H1>%s</H1>", title);
}
