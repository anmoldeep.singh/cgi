/* htmllib.h */

#ifndef _HTMLLIB_H
#define _HTMLLIB_H

/* function prototypes */
void htmlHeader(const char *title);
void binaryHeader(const char *filename, unsigned int length);
void htmlBody();
void htmlFooter();
void addTitleElement(const char *title);

#endif	/* !_HTMLLIB_H*/
