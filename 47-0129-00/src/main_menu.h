
#ifndef __PAGE_MAIN_H
#define __PAGE_MAIN_H

#include "page/page.h"
#include "tab/tab.h"

class MainPage : public Page
{
	public:
		MainPage(string name);
		virtual ~MainPage(void) {}

		virtual void Head(void);
		virtual void Show(char **postvars);
		virtual void Apply(char **postvars);

	private:
		tab_list_t tabs;
};

#endif
