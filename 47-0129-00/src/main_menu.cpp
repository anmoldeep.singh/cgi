
#include "main_menu.h"
#include "lists.h"
#include "access.h"
#include "tab/tab_system_information.h"
#include "tab/tab_device_configuration.h"
#include "tab/tab_backup_restore.h"
#include "tab/tab_file_browser.h"
#include "tab/tab_admin.h"
#include "tab/tab_licence.h"
#include "tab/tab_service.h"
#include "tab/tab_remote.h"

std::string current_tab = SYSTEM_INFORMATION_TAB;	


MainPage::MainPage(string name) : Page(name)
{
	tabs.push_back(new SystemInformationTab(MenuName(), true));
	tabs.push_back(new DeviceConfigurationTab(MenuName(), true));
	tabs.push_back(new BackupRestoreTab(MenuName(), true));
	tabs.push_back(new FileBrowserTab(MenuName(), true));
	tabs.push_back(new AdminTab(MenuName(), true));
	tabs.push_back(new LicenceTab(MenuName(), true));
	tabs.push_back(new ServiceTab(MenuName(), true));
	tabs.push_back(new RemoteTab(MenuName(), true));
}

void MainPage::Head(void)
{
	Lists<Tab> tab_list(tabs);
	Tab *tab = tab_list.FirstItem();
	while(tab)
	{
		if(tab->GetName() == current_tab)
			tab->Head();

		tab = tab_list.NextItem();
	}
}

void MainPage::Show(char **postvars)
{
	printf("<table border=\"0\" cellpadding=\"5\" cellspacing=\"0\" width=\"98%%\" align=\"center\">");
	printf("<tr><td valign=\"center\" align=\"left\" colspan=\"2\">");
	printf("<table border=\"0\" cellpadding=\"1\" cellspacing=\"2\" align=\"left\">");
	printf("<tr>");
	Lists<Tab> tab_list(tabs);
	Tab *tab = tab_list.FirstItem();
	while(tab)
	{
		if((tab->GetReference() == REF_SERVICE_TAB || tab->GetReference() == REF_REMOTE_TAB) && _access.GetUsername() != "fdsservice")
		{
			tab = tab_list.NextItem();
			continue;
		}
		printf("<td class=\"cell_tab\" align=\"center\">");
		printf("<input type=submit name=\"" REF_TAB "\" value=\"%s\" class=\"buttons\">", tab->GetName().c_str());
		printf("</td>");

		tab = tab_list.NextItem();
	}
	printf("</tr>");
	printf("</table>");

	tab = tab_list.FirstItem();
	while(tab)
	{
		if(tab->GetName() == current_tab)
			tab->Show(postvars);

		tab = tab_list.NextItem();
	}

	printf("</td></tr></table>");
}


void MainPage::Apply(char **postvars)
{
	Lists<Tab> tab_list(tabs);
	Tab *tab = tab_list.FirstItem();
	while(tab)
	{
		if(tab->GetName() == current_tab)
			tab->Apply(postvars);
		tab = tab_list.NextItem();
	}
}

