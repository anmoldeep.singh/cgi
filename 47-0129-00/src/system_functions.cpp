
#include <hhmpi/gui_config.h>
#include "system_functions.h"
#include "common.h"

#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/dir.h>
#include <sys/vfs.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <linux/if.h>
#include <string.h>
#include <sys/mount.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <linux/if.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <unistd.h>


#define SYS_FUNC_DEBUG

#define SFR_PATH		"/dev/sfr"
#define SFR_RESET_COMMAND	"0x80000000:0x0"

SystemFunctions system_functions;


SystemFunctions::SystemFunctions(void)
{
}


int SystemFunctions::UploadFlash(char *filename)
{
	check_upload_directory();

	string source = string(UPLOAD_DIRECTORY) + string("/") + string(filename);

	if (!system_functions.CheckV2Config(source))
	{
		printf("This config file is not suitable for MOD 5 (or newer) hardware platforms<br>\n");
		return 0;
	}

	if (!Settings::Instance().LoadFile(source.c_str(), SECTION_HHMPI))
	{
		system("sync");
		//printf("Error loading settings<br>\n");
		return 0;
	}

	Settings::Instance().SaveFlashForce();

	remove(source.c_str());
	system("sync");
	return 1;
}


int SystemFunctions::UploadAircraft(char *filename)
{
	string source = string(UPLOAD_DIRECTORY) + string("/") + string(filename);

	check_upload_directory();

	struct stat file_info;

	if (stat(source.c_str(), &file_info) < 0 || !S_ISREG(file_info.st_mode))
		return 0;

	if(!Copy(source.c_str(), AIRCRAFT_FILE_PATH))
	{
		system("sync");
		return 0;
	}
	remove(source.c_str());
	system("sync");
	return 1;
}


int SystemFunctions::UploadSSLCertificate(char *filename)
{
	string source = string(UPLOAD_DIRECTORY) + string("/") + string(filename);

	check_upload_directory();

	struct stat file_info;

	if (stat(source.c_str(), &file_info) < 0 || !S_ISREG(file_info.st_mode))
		return 0;

	if(!Copy(source.c_str(), CURL_CRT_PATH))
	{
		system("sync");
		return 0;
	}
	remove(source.c_str());
	system("sync");
	return 1;
}

int SystemFunctions::UploadSFTPKey(char *filename)
{
	string source = string(UPLOAD_DIRECTORY) + string("/") + string(filename);

	check_upload_directory();

	struct stat file_info;

	if (stat(source.c_str(), &file_info) < 0 || !S_ISREG(file_info.st_mode))
		return 0;

	if(!Copy(source.c_str(), (string(CURL_SFTP_KEYS_DIR) +  string("/") + string(filename)).c_str()))
	{
		system("sync");
		return 0;
	}
	remove(source.c_str());
	system("sync");
	return 1;
}

int SystemFunctions::UploadSFTPPrivateKey(const char *filename, const char* server)
{
	string source = string(UPLOAD_DIRECTORY) + string("/") + string(filename);

	check_upload_directory();

	struct stat file_info;

	if (stat(source.c_str(), &file_info) < 0 || !S_ISREG(file_info.st_mode))
		return 0;

	if(!Copy(source.c_str(), (string(CURL_SFTP_KEYS_DIR) +  string("/") + string(server)).c_str()))
	{
		system("sync");
		return 0;
	}
	remove(source.c_str());
	system("sync");
	return 1;
}

int SystemFunctions::UploadSFTPPublicKey(const char *filename, const char* server)
{
	string source = string(UPLOAD_DIRECTORY) + string("/") + string(filename);

	check_upload_directory();

	struct stat file_info;

	if (stat(source.c_str(), &file_info) < 0 || !S_ISREG(file_info.st_mode))
		return 0;

	if(!Copy(source.c_str(), (string(CURL_SFTP_KEYS_DIR) +  string("/") + string(server) + string(".pub")).c_str()))
	{
		system("sync");
		return 0;
	}
	remove(source.c_str());
	system("sync");
	return 1;
}

int SystemFunctions::UploadCustomEncryptionKey(char *filename)
{
	string source = string(UPLOAD_DIRECTORY) + string("/") + string(filename);

	check_upload_directory();

	struct stat file_info;

	if (stat(source.c_str(), &file_info) < 0 || !S_ISREG(file_info.st_mode))
		return 0;

	if(!Copy(source.c_str(), CUSTOM_ENCRYPT_KEY_PATH))
	{
		system("sync");
		return 0;
	}
	remove(source.c_str());
	system("sync");
	return 1;
}

int SystemFunctions::SignalGUI(void)
{
	string directory = "/proc";

	DIR *DirPtr = opendir(directory.c_str());
	
	if (DirPtr == NULL)
	{
		//printf("Unable to access %s\n", directory.c_str());
		return 0;
	}

	int hhmpi_gui_pid = -1;

	while (hhmpi_gui_pid == -1)
	{
		struct direct *DirEntryPtr = readdir(DirPtr);

		if (DirEntryPtr == 0)
		{
			closedir(DirPtr);
			break;
		}

		if (strcmp(DirEntryPtr->d_name,".") == 0 ||
			strcmp(DirEntryPtr->d_name,"..") == 0)
				continue;

		if (DirEntryPtr->d_name[0] >= '0' && DirEntryPtr->d_name[0] <= '9')
		{
			string path = directory + string("/") + string(DirEntryPtr->d_name) + "/cmdline";

			FILE *in = fopen(path.c_str(), "rb");

			if (in == NULL)
				continue;

			char buffer[48];
			if (fgets(buffer, sizeof(buffer), in) > 0)
				if (strcmp(buffer, "hhmpi_gui") == 0)
					hhmpi_gui_pid = atoi(DirEntryPtr->d_name);

			fclose(in);
		}
	}

	if (hhmpi_gui_pid != -1)
		kill(hhmpi_gui_pid, SIGALRM);
	return 1;
}


int SystemFunctions::SignalDaemon(void)
{
	string directory = "/proc";

	DIR *DirPtr = opendir(directory.c_str());
	
	if (DirPtr == NULL)
	{
		//printf("Unable to access %s\n", directory.c_str());
		return 0;
	}

	int hhmpi_daemon_pid = -1;

	while (hhmpi_daemon_pid == -1)
	{
		struct direct *DirEntryPtr = readdir(DirPtr);

		if (DirEntryPtr == 0)
		{
			closedir(DirPtr);
			break;
		}

		if (strcmp(DirEntryPtr->d_name,".") == 0 ||
			strcmp(DirEntryPtr->d_name,"..") == 0)
				continue;

		if (DirEntryPtr->d_name[0] >= '0' && DirEntryPtr->d_name[0] <= '9')
		{
			string path = directory + string("/") + string(DirEntryPtr->d_name) + "/cmdline";

			FILE *in = fopen(path.c_str(), "rb");

			if (in == NULL)
				continue;

			char buffer[48];
			if (fgets(buffer, sizeof(buffer), in) > 0)
				if (strcmp(buffer, "hhmpi_daemon") == 0)
					hhmpi_daemon_pid = atoi(DirEntryPtr->d_name);

			fclose(in);
		}
	}

	if (hhmpi_daemon_pid != -1)
		kill(hhmpi_daemon_pid, SIGALRM);
	return 1;
}


int SystemFunctions::DownloadFlash(void)
{
	Settings& settings = Settings::Instance();
	settings.SaveFileForce(SETTINGS_PATH, SECTION_HHMPI);
	SendFile(SETTINGS_PATH, SETTINGS_FILE);
	remove(SETTINGS_PATH);
	return 1;
}

int SystemFunctions::DownloadAircraft(void)
{
	SendFile(AIRCRAFT_FILE_PATH, AIRCRAFT_FILE_BACKUP);
	return 1;
}

int SystemFunctions::DownloadSSLCertificate(void)
{
	SendFile(CURL_CRT_PATH, CURL_CRT_PATH);
	return 1;
}

int SystemFunctions::DownloadSFTPKey(void)
{
	SendFile(CURL_SFTP_KEYS_DIR "/key", "key");
	return 1;
}

int SystemFunctions::DownloadCustomEncryptionKey(void)
{
	SendFile(CUSTOM_ENCRYPT_KEY_PATH, "custom_encrypt_key.pem");
	return 1;
}

int SystemFunctions::DownloadSecurityConfiguration(void)
{
	string serial_number = Settings::Instance().String("serial");

	string filename = "hhmpi_security_" + serial_number + ".conf";
	string filepath = "/tmp/" + filename;

	FILE *out = fopen(filepath.c_str(), "wb");

	if (out)
	{
		fprintf(out, "key=%s\n", Settings::Instance().String("key").c_str());
		fprintf(out, "sn=%s\n", serial_number.c_str());

		fprintf(out, "ethaddr=%s\n", Settings::Instance().String("ethaddr").c_str());

		SettingsMap& settings_map = Settings::Instance().GetSettingsMap();

		SettingsMap::iterator it;

		for (it = settings_map.begin();
			it != settings_map.end();
			it++)
        {
			string identifier = it->first;
			string value = it->second;

			if (identifier.substr(0, 8) == "feature.")
				fprintf(out, "%s=%s\n", identifier.c_str(), value.c_str());
		}

#ifndef AA_LEGACY_BUILD
		// Add id chip UUIDs' into the hash list
		const int DEV_COUNT = 3;
		const char * chipID[DEV_COUNT] = {
			"/sys/bus/w1/devices/w1_bus_master3/w1_master_slaves",
			"/sys/bus/w1/devices/w1_bus_master4/w1_master_slaves",
			"/sys/bus/w1/devices/w1_bus_master5/w1_master_slaves"
		};
#else
		// Needed for future build 47-0154-04 as AA won't have HW keys logged
		const int DEV_COUNT = 1;
		const char * chipID[DEV_COUNT] = { "/sys/bus/w1/devices/w1_bus_master2/w1_master_slaves" };
#endif

		for (int i = 0; i < DEV_COUNT; i++)
		{
			FILE *in = fopen(chipID[i], "rb");

			if (in)
			{

				char buffer[1024];

				while (fgets(buffer, sizeof(buffer), in)) {
					string id_chip_uuid = buffer;
					TrimRight(id_chip_uuid, string("\n"));
					fprintf(out, "%s\n", id_chip_uuid.c_str());
				}

				fclose(in);
			}
		}
		fclose(out);
	}

	SendFile(filepath.c_str(), filename.c_str());
	remove(filepath.c_str());
	return 1;
}

int SystemFunctions::Copy(const char *source, const char *target)
{
	bool error = false;

#ifdef SYS_FUNC_DEBUG
	printf("Copying from %s to %s\n<br>", source, target);
#endif

	unsigned char fio_buffer[128];

	FILE *in = fopen(source, "rb");

	if (in == NULL)
	{
#ifdef SYS_FUNC_DEBUG
		printf("Could not open file for reading: '%s'<br>", source);
#endif
		return 0;
	}

	FILE *out = fopen(target, "wb");
	
	if (out == NULL)
	{
#ifdef SYS_FUNC_DEBUG
		printf("Could not open file for writing: '%s'<br>", target);
#endif
		fclose(in);
		return 0;
	}

	while (!error)
	{
		int rd_result = fread(fio_buffer, 1, 128, in);

		if (rd_result <= 0)
			break;

		int wr_result = fwrite(fio_buffer, 1, rd_result, out);

		if (wr_result <= 0)
			break;

		if (wr_result != rd_result)
		{
#ifdef SYS_FUNC_DEBUG
			printf("Error writing to detination!<br>\n");
#endif
			error = true;
			break;
		}
	}

	if (ferror(in) || ferror(out))
		error = true;

	fclose(in);
	fclose(out);

	if (error)
	{
		// Delete the half completed copy
		remove(target);
	}
#ifdef SYS_FUNC_DEBUG
	printf("Copy complete!<br>\n");
#endif
	if(error)
		return 0;

	return 1;
}


int SystemFunctions::SendFile(const char *source_path, const char *target)
{
	FILE *in = fopen(source_path, "rb");

	if (!in) return 0;

	fseek(in, 0, SEEK_END);
	unsigned int length = ftell(in);
	fseek(in, 0, SEEK_SET);

	binaryHeader(target, length);

	unsigned char *buffer = new unsigned char [128];

	while (true)
	{
		int result = fread(buffer, sizeof(unsigned char), 128, in);

		if (result <= 0)
			break;

		fwrite(buffer, sizeof(unsigned char), result, stdout);
	}

	delete buffer;

	fclose(in);
	fflush(stdout);
	return 1;
}


bool SystemFunctions::GetTotalMemory(std::string &str)
{
	bool error = false;
	string tmp;
	int startpos;
	int endpos;

	FILE *in = fopen(MEMINFO_PATH, "rt");
	if (!in) return false;

	char buffer[64];

	// Parse lines
	while(fgets(buffer, sizeof(buffer), in) && !error)
	{
		if(buffer[0] == 0)
			error = true;
		else
		{
			tmp = buffer;
			if(tmp.compare(0, 8, "MemTotal") == 0)
			{
				tmp = tmp.substr(tmp.find(':')+1);
				startpos = tmp.find_first_not_of(" \t");
				endpos = tmp.find_last_not_of(" \t\n\r");
				str = tmp.substr(startpos, endpos-startpos+1 );
				break;
			}
		}
	}

	fclose(in);

	return !error;
}

bool SystemFunctions::GetAvailableMemory(std::string &str)
{
	bool error = false;
	string tmp;
	int startpos;
	int endpos;

	FILE *in = fopen(MEMINFO_PATH, "rt");
	if (!in) return false;

	char buffer[64];

	// Parse lines
	while(fgets(buffer, sizeof(buffer), in) && !error)
	{
		if(buffer[0] == 0)
			error = true;
		else
		{
			tmp = buffer;
			if(tmp.compare(0, 7, "MemFree") == 0)
			{
				tmp = tmp.substr(tmp.find(':')+1);
				startpos = tmp.find_first_not_of(" \t");
				endpos = tmp.find_last_not_of(" \t\n\r");
				str = tmp.substr(startpos, endpos-startpos+1 );
				break;
			}
		}
	}

	fclose(in);

	return !error;
}


bool SystemFunctions::GetTotalInternalStorage(std::string &str)
{
	char scratch[1024] = "";
	FILE *fp = popen("df /mnt/internal | tail -1 | tr -s ' ' | cut -d' ' -f2 2>/dev/null", "r");

	if (fp == NULL)
		return false;

	uint32_t kb_total = 0;
	fscanf(fp, "%u", &kb_total);
	pclose(fp);

	if (kb_total >= 1024*1024)
		snprintf(scratch, sizeof(scratch), "%u MB", kb_total/1024);
	else
		snprintf(scratch, sizeof(scratch), "%u kB", kb_total);

	str = scratch;

	return true;
}


bool SystemFunctions::GetAvailableInternalStorage(std::string &str)
{
	char scratch[1024] = "";       
	FILE *fp = popen("df /mnt/internal | tail -1 | tr -s ' ' | cut -d' ' -f3 2>/dev/null", "r");

        if (fp == NULL) 
                return false;

	uint32_t kb_used = 0;
	fscanf(fp, "%u", &kb_used);
	pclose(fp);

	fp = popen("df /mnt/internal | tail -1 | tr -s ' ' | cut -d' ' -f2 2>/dev/null", "r");

        if (fp == NULL) 
                return false;

	uint32_t kb_total = 0;
	fscanf(fp, "%u", &kb_total);
	pclose(fp);

	uint32_t kb_available = kb_total - kb_used;

	if (kb_available >= 1024*1024)
                snprintf(scratch, sizeof(scratch), "%u MB", kb_available/1024);
        else
                snprintf(scratch, sizeof(scratch), "%u kB", kb_available);

        str = scratch;

        return true;

}

bool SystemFunctions::GetHostname(std::string &str)
{
	bool error = false;
	FILE *in = fopen(HOSTNAME_PATH, "rt");
	if (!in) return false;

	char buffer[32];

	// Parse lines
	while(fgets(buffer, sizeof(buffer), in) && !error)
	{
		if(buffer[0] == 0)
			error = true;
		else
		{
			str = buffer;
		}
	}

	fclose(in);

	return !error;
}


bool SystemFunctions::GetMPIVersion(std::string &str)
{
	bool error = false;
	string tmp;
	int startpos;
	int endpos;

	FILE *in = fopen(MPI_PATH, "rt");
	if (!in) return false;

	char buffer[64];

	// Parse lines
	while(fgets(buffer, sizeof(buffer), in) && !error)
	{
		if(buffer[0] == 0)
			error = true;
		else
		{
			tmp = buffer;
			if(tmp.compare(0, 17, "FW Version String") == 0)
			{
				tmp = tmp.substr(tmp.find(':')+1);
				startpos = tmp.find_first_not_of(" \t'");
				endpos = tmp.find_last_not_of(" \t\n\r'");
				str = tmp.substr(startpos, endpos-startpos+1 );
				break;
			}
		}
	}

	fclose(in);

	return !error;
}


bool SystemFunctions::GetIPAddress(std::string &str)
{
	struct ifreq ifr;
	struct sockaddr_in *sin = (struct sockaddr_in *) &ifr.ifr_addr;

	memset(&ifr, 0, sizeof ifr);

	int sfd = socket(AF_INET, SOCK_STREAM, 0);

	if (sfd < 0)
	{
		perror("socket()");
		return false;
	}

	strcpy(ifr.ifr_name, "eth0");
	sin->sin_family = AF_INET;

	if (ioctl(sfd, SIOCGIFADDR, &ifr) == 0)
			str = inet_ntoa(sin->sin_addr);
	else
			str = "None";

	return true;
}

bool SystemFunctions::ResetDevice(void)
{
	system("/sbin/reboot -d 2 &");
	return true;
}

bool SystemFunctions::FactoryDefaults(void)
{
        string altbootcmd = Settings::Instance().String("altbootcmd");
        string baudrate = Settings::Instance().String("baudrate");
        string bootargs = Settings::Instance().String("bootargs");
        string bootcmd = Settings::Instance().String("bootcmd");
        string bootdelay = Settings::Instance().String("bootdelay");
        string bootfile = Settings::Instance().String("bootfile");
        string ethaddr = Settings::Instance().String("ethaddr");
        string mmc_boot = Settings::Instance().String("mmc_boot");
        string nand_boot = Settings::Instance().String("nand_boot");
        string recbootcmd = Settings::Instance().String("recbootcmd");
        string splashimage = Settings::Instance().String("splashimage");
        string splashsize = Settings::Instance().String("splashsize");
        string stderr = Settings::Instance().String("stderr");
        string stdin = Settings::Instance().String("stdin");
        string stdout = Settings::Instance().String("stdout");
	string owner_desc = Settings::Instance().String("owner.desc");
	string owner_asset = Settings::Instance().String("owner.asset");
	string owner_company = Settings::Instance().String("owner.company");
	string owner_contact = Settings::Instance().String("owner.contact");

        if (!Settings::Instance().LoadFile("/etc/hhmpi.conf.defaults", SECTION_HHMPI))
                return false;

        Settings::Instance().String("altbootcmd", altbootcmd);
        Settings::Instance().String("baudrate", baudrate);
        Settings::Instance().String("bootargs", bootargs);
        Settings::Instance().String("bootcmd", bootcmd);
        Settings::Instance().String("bootdelay", bootdelay);
        Settings::Instance().String("bootfile", bootfile);
        Settings::Instance().String("ethaddr", ethaddr);
        Settings::Instance().String("mmc_boot", mmc_boot);
        Settings::Instance().String("nand_boot", nand_boot);
        Settings::Instance().String("recbootcmd", recbootcmd);
        Settings::Instance().String("splashimage", splashimage);
        Settings::Instance().String("splashsize", splashsize);
        Settings::Instance().String("stderr", stderr);
        Settings::Instance().String("stdin", stdin);
        Settings::Instance().String("stdout", stdout);
	Settings::Instance().String("owner.desc", owner_desc);
	Settings::Instance().String("owner.asset", owner_asset);
	Settings::Instance().String("owner.company", owner_company);
	Settings::Instance().String("owner.contact", owner_contact);
        Settings::Instance().SaveFlashForce();

	return true;
}


void SystemFunctions::check_upload_directory(void)
{
	struct stat file_info;

	if (stat(UPLOAD_DIRECTORY, &file_info))
		mkdir(UPLOAD_DIRECTORY, S_IRWXU | S_IRWXG | S_IRWXO);

	system("sync");
}

string SystemFunctions::GetMAC(const char *if_name)
{
	struct ifreq ifr;
	
	int fd = socket(AF_INET, SOCK_DGRAM, 0);

	ifr.ifr_addr.sa_family = AF_INET;
	sprintf(ifr.ifr_name, if_name);
	
	if (ioctl(fd, SIOCGIFHWADDR, &ifr) < 0)
	{
		close(fd);
		return string("Not Available");
	}
	
	close(fd);
	
	char mac_string[32];

	snprintf(mac_string, sizeof(mac_string), "%.2X:%.2X:%.2X:%.2X:%.2X:%.2X\n",
			(unsigned char)ifr.ifr_hwaddr.sa_data[0],
			(unsigned char)ifr.ifr_hwaddr.sa_data[1],
			(unsigned char)ifr.ifr_hwaddr.sa_data[2],
			(unsigned char)ifr.ifr_hwaddr.sa_data[3],
			(unsigned char)ifr.ifr_hwaddr.sa_data[4],
			(unsigned char)ifr.ifr_hwaddr.sa_data[5]);

	return string(mac_string);
}

int SystemFunctions::InstallOEMType(const char *oem_name)
{
	string command = "/bin/hhmpi_splash install " + string(oem_name);
	system(command.c_str());
	return 1;
}

bool SystemFunctions::CheckMounted(string path)
{
        bool mounted = false;

        FILE *in = fopen("/proc/mounts", "rb");

        if (in == NULL)
                return false;

        char buffer[256];

        while (fgets(buffer, sizeof(buffer), in) > 0)
                if (strstr(buffer, path.c_str()))
                {
                        mounted = true;
                        break;
                }

        fclose(in);

        return mounted;
}


bool SystemFunctions::Remount(string mount_point, bool read_only, bool remount)
{
	if (mount_point == "/mnt/internal")
	{
		if (mount("ubi0:internal", "/mnt/internal", "ubifs",  MS_MGC_VAL | (remount ? MS_REMOUNT : 0) | (read_only ? MS_RDONLY : 0), NULL) < 0)
			return false;
	}

	if (mount_point == "/mnt/usbmem")
	{
		if (mount("/dev/sda1", "/mnt/usbmem", "vfat",  MS_MGC_VAL | (remount ? MS_REMOUNT : 0) | (read_only ? MS_RDONLY : 0), NULL) < 0)
			return false;
	}

	if (mount_point == "/mnt/sdcard")
	{
		if (mount("/dev/mmcblk0p1", "/mnt/sdcard", "vfat",  MS_MGC_VAL | (remount ? MS_REMOUNT : 0) | (read_only ? MS_RDONLY : 0), NULL) < 0)
			return false;
	}

	return true;
}

void SystemFunctions::DeleteFiles(string mount_point, string path, list<string>& delete_list)
{
	if (!Remount(mount_point, false))
        {
		printf("<p class=\"operation_status_area\">");
		printf("ERROR: Unable to remount storage to read-write<br>\n");
		printf("</p>");
		return;
	}

	list<string>::iterator it;

	for (it = delete_list.begin();
			it != delete_list.end();
			it++)
	{
		string& filename = *it;

		string file = path + filename;

		if (remove(file.c_str()) < 0)
		{
			printf("<p class=\"operation_status_area\">");
			printf("Error deleting %s<br>\n", filename.c_str());
			printf("</p>");
		}
	}

	if (!Remount(mount_point, true))
	{
		printf("<p class=\"operation_status_area\">");
		printf("ERROR: Unable to mount storage to read-only<br>\n");
		printf("</p>");
		return;
	}
}


void SystemFunctions::Format(string mount_point)
{
	printf("<p class=\"operation_status_area\">");

	if (umount(mount_point.c_str()) < 0)
	{
		printf("ERROR: Unable to unmount internal storage<br>\n");
		printf("</p>");
		return;
	}

	int ret = 0;

	if (mount_point == "/mnt/internal")
		ret = system("/usr/sbin/mkfs.ubifs -y /dev/ubi0_0");

        if (mount_point == "/mnt/usbmem")
		ret = system("/usr/sbin/mkfs.vfat -I -F 32 /dev/sda1");

        if (mount_point == "/mnt/sdcard")
		ret = system("/usr/sbin/mkfs.vfat -I -F 32 /dev/mmcblk0p1");

	perror("result");

	if (ret != 0)
		printf("WARNING: Failure in waiting for format to complete<br>\n");

	if (!Remount(mount_point, true, false))
	{
		printf("ERROR: Unable to mount storage<br>\n");
		printf("</p>");
		return;
	}
		
	printf("Format Complete<br>\n");
	printf("</p>");
}

bool SystemFunctions::CheckV2Config(string file)
{
	FILE *in = fopen(file.c_str(), "rb");

	if (in == NULL)
		return false;

	const char string_v1[] = "\"bootargs\",\"root=/dev/ram initrd=0xa1800000,4000k console=ttyS0,38400N8\"";

	char buffer[1024];
	while(fgets(buffer, sizeof(buffer), in))
	{
		if (strncmp(buffer, string_v1, strlen(string_v1)) == 0)
		{
			fclose(in);
			return false;
			break;
		}
	}

	fclose(in);
	return true;
}

