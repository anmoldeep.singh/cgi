
#include "page_operation_status.h"

OperationStatusPage operation_status_page(OPERATION_STATUS_PAGE_TITLE);


OperationStatusPage::OperationStatusPage(string name) : Page(name)
{
}

void OperationStatusPage::Show(char **postvars)
{
	bool success = false;
	printf("<table border=\"0\" cellpadding=\"10\" cellspacing=\"0\" width=\"50%%\" align=\"center\">");
	printf("<tr>");
	printf("<td colspan=\"2\" width=\"100%%\" class=\"text_body\">");
	printf("<br>");
	printf("</td>");
	printf("</tr>");

	printf("<tr>");
	printf("<td colspan=\"2\" class=\"incorrect\">Operation %s</td>", success ? "Successful" : "Failed");
	printf("</tr>");
	printf("<tr><td class=\"buttons\">");
	printf("<input type=\"hidden\" name=\"" REF_PAGE "\" value=\"%s\">", MAIN_PAGE_TITLE); 
	printf("<input type=\"submit\" name=\"" REF_ACTION "\" value=\"%s\">", BTN_ACTION_RETURN);
	printf("</td>");
	printf("</tr>");

	printf("</table>");
}
