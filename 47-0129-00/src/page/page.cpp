#include <curl/curl.h>
#include <hhmpi/gui_config.h>
#include <hhmpi/idcheck.h>

#include "page.h"
#include "access.h"
#include "page_template.h"
#include "system_functions.h"

map<string, Page *> page_map;


size_t write_data(uint8_t *ptr, size_t size, size_t nmemb, uint8_t *crc)
{
	int length = size * nmemb;

	if (length == 16)
	{
		for (int i = 0; i < 8; i++)
		{
			int value;
			string hash_string;
			hash_string += ptr[i*2+0];
			hash_string += ptr[i*2+1];
			sscanf(hash_string.c_str(), "%x", &value);
			crc[i] = value;
		}
	}

	return nmemb;
}

void PostPersistantVariables(void)
{
	printf("<input type=\"hidden\" name=\"username\" value=\"%s\">", _access.GetUsername().c_str());
	printf("<input type=\"hidden\" name=\"current_tab\" value=\"%s\">", current_tab.c_str());
	printf("<input type=\"hidden\" name=\"session_id\" value=\"%s\">", _access.GetSessionID().c_str());
}

Page::Page(string name)
{
	Page::name = name;
}

Page::~Page(void)
{
}

void Page::Register(Page *page)
{
	page_map[page->MenuName()] = page;
}

void Page::NotFound(string page_name)
{
	page_template.Header("Error");
	printf("%s not found!<br>\n", page_name.c_str());
	page_template.Footer();
}

void Page::Action(char **postvars, int page_method)
{
	string page_name = MAIN_PAGE_TITLE;
	string argument;
	bool login = false;
	bool logout = false;
	bool apply = false;
	bool save = false;
	bool reset = false;
	bool upload = false;
	bool download = false;
	bool set = false;
	bool del = false;
	bool format = false;
	bool button = false;
	string username;
	string password;
	string session_id;
	bool session_valid = false;

	if (page_method == POST)
	{
		for (int i = 0; postvars[i]; i += 2)
		{
			if (strcmp(postvars[i], REF_PAGE) == 0)
				page_name = postvars[i+1];

			if (strcmp(postvars[i], REF_DOWNLOAD) == 0)
				download = true;
			else if (strcmp(postvars[i], REF_UPLOAD) == 0)
				upload = true;
			
			if (strcmp(postvars[i], REF_ACTION) == 0)
			{
				if (strcmp(postvars[i+1], BTN_ACTION_APPLY) == 0)
					apply = true;
				else if (strcmp(postvars[i+1], BTN_ACTION_SAVE) == 0)
					save = true;
				else if (strcmp(postvars[i+1], BTN_ACTION_CANCEL) == 0)
					reset = true;
				else if (strcmp(postvars[i+1], REF_SET) == 0)
					set = true;
				else if (strcmp(postvars[i+1], REF_DELETE) == 0)
					del = true;
				else if (strcmp(postvars[i+1], REF_FORMAT) == 0)
					format = true;
			}
			
			if (strcmp(postvars[i], REF_ACCESS) == 0)
			{
				if (strcmp(postvars[i+1], BTN_ACCESS_LOGIN) == 0)
					login = true;
				else if (strcmp(postvars[i+1], BTN_ACCESS_LOGOUT) == 0)
					logout = true;
			}

			if (strcmp(postvars[i], REF_TAB) == 0)
				current_tab = postvars[i+1];
			else if (strcmp(postvars[i], REF_CURRENT_TAB) == 0)
				current_tab = postvars[i+1];

			if (strcmp(postvars[i], REF_USERNAME) == 0)
				username = postvars[i+1];

			if (strcmp(postvars[i], REF_PASSWORD) == 0)
				password = postvars[i+1];

			if (strcmp(postvars[i], REF_SESSION_ID) == 0)
				session_id = postvars[i+1];

			if (strcmp(postvars[i], REF_BUTTON) == 0)
				button = true;
		}
	}

	_access.SetSessionID(session_id);

	if(!logout && login)
	{
		_access.Login(username, password);
	}
	else	
	if(logout)
	{
		_access.Logout();
	}
	else
		_access.SetUser(username);

	Page *page = NULL;

	page = page_map[page_name];

	if (page == NULL)
	{
		NotFound(page_name);
		return;
	}

	if (download)
	{
		page->Apply(postvars);
		return;
	}
	
	page_template.Header(page->MenuName());

	if(_access.LoggedIn() && !logout) //if logged-in and not logging out, make login information persisntant
	{
		printf("<input type=\"hidden\" name=\"" REF_USERNAME "\" value=\"%s\">", _access.GetUsername().c_str());
		printf("<input type=\"hidden\" name=\"" REF_SESSION_ID "\" value=\"%s\">", _access.GetSessionID().c_str());
		_access.UpdateSession();
		session_valid = true;
	}

	printf("<input type=\"hidden\" name=\"" REF_CURRENT_TAB "\" value=\"%s\">", current_tab.c_str());

	

	if(apply)
	{
		printf("<p class=\"operation_status_area\">");
		printf("Applying saved settings...<br>");
		system_functions.SignalGUI();
		system_functions.SignalDaemon();
		printf("</p>");
	}
	else if(save)
	{
		printf("<p class=\"operation_status_area\">");
		printf("Saving settings to flash...<br>");
		page->Apply(postvars);
		Settings::Instance().Save(SECTION_HHMPI | SECTION_SERVICE);
		printf("</p>");

	      	// Update our session timestamp particularly if we changed the date or time
		if (session_valid)
                	_access.UpdateSession();

	}
	else if(reset)
	{
		printf("<p class=\"operation_status_area\">Cancel: Reset page to stored settings.</p>");
	}
	else if(del)
	{
		//printf("<p class=\"operation_status_area\">");
		page->Apply(postvars);
		//printf("</p>");
	}
	else if(format)
	{
		page->Apply(postvars);
	}
	else if (button)
		page->Apply(postvars);

	if(logout)
	{
		printf("<p class=\"operation_status_area\">");
		printf("Logged out!<br>");
		printf("</p>");
	}

	if (upload)
	{
		printf("<p class=\"operation_status_area\">");
		printf("Uploading file...<br>");
		page->Apply(postvars);
		printf("</p>");
	}

	if (set)
	{
		printf("<p class=\"operation_status_area\">");
		page->Apply(postvars);
		printf("</p>");
	}

	if(_access.Attempted())
	{
		printf("<p class=\"operation_status_area\">Username and/or Password is incorrect!</p>");
	}

	if(!_access.LoggedIn())
	{
		page = page_map[LOGIN_PAGE_TITLE]; //force login page if not logged in.
	}


	IDCheck id_check;

	if (!id_check.SystemCheck(HHMPI_PUBLIC_KEY))
	{
		string ip;
		system_functions.GetIPAddress(ip);

		if (ip.substr(0, 10) == "192.168.0.")
		{
			uint8_t crc[8];
			
			if (id_check.SystemHash(crc))
			{
				string hash = id_check.CRCString(crc);
				//printf("IP: %s\n", ip.c_str());

				string url = "http://192.168.0.60/keygen/keygen_webauto.cgi?hash=" + hash;

				CURL *curl = curl_easy_init();

				uint8_t encrypted_crc[8];
	
				if (curl)
				{
					curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
					curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
					curl_easy_setopt(curl, CURLOPT_WRITEDATA, encrypted_crc);
					curl_easy_setopt(curl, CURLOPT_TIMEOUT_MS, 2000);
					curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT_MS, 2000);
					CURLcode res = curl_easy_perform(curl);
					curl_easy_cleanup(curl);

					if (res == 0)
					{
						string encrypted_hash = id_check.CRCString(encrypted_crc);
						Settings::Instance().String("key", encrypted_hash);
						Settings::Instance().Save(SECTION_SERVICE);

						printf("<p class=\"operation_status_area\">");
						printf("Updated security key to %s\n", encrypted_hash.c_str());
						printf("</p>");
					}
					//else
					//	printf("curl error: %s\n", curl_easy_strerror(res));
				}
			}
		}
	}
		

	page->Head();
	page->Show(postvars);

	page_template.Footer();
}

const char *Page::MenuName(void)
{
	return name.c_str();
}
