
#ifndef __PAGE_H
#define __PAGE_H

#include "common.h"


void PostPersistantVariables(void);

class Page
{
	public:
		Page(string name);
		virtual ~Page(void);

		virtual void Register(Page *page);
	
		virtual void NotFound(string page_name);

		virtual void Action(char **postvars, int page_method);

		virtual const char *MenuName(void);

		virtual void Upload(char **postvars) { };
		virtual void Download(const char *file) { };
		
		virtual void Head(void) { };
		virtual void Show(char **postvars) { };
		virtual void Apply(char **postvars) { };


	protected:
		string name;

};

#endif


// Include menu headers here

#include "page_login.h"
#include "main_menu.h"
