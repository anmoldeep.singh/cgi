
#ifndef __PAGE_OPERATION_STATUS_H
#define __PAGE_OPERATION_STATUS_H

#include "page.h"

class OperationStatusPage : public Page
{
	public:
		OperationStatusPage(string name);
		virtual ~OperationStatusPage(void) {}

		virtual void Show(char **postvars);
};

extern OperationStatusPage operation_status_page;

#endif
