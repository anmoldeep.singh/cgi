
#include "page_template.h"
#include "system_functions.h"
#include "access.h"

#define LOGO_FILE "logo.gif"
PageTemplate page_template;


PageTemplate::PageTemplate(void)
{
}

void PageTemplate::Header(string name, string title)
{
	string oem_name = Settings::Instance().String("oem.name");
	string logo_file = "logo_" + oem_name + ".gif";
	string css_file = "template_" + oem_name + ".css";
	string url = "Flight Data Systems";
	string desc = "http://www.flightdata.aero";

	if (oem_name == "l3")
	{
		desc = "L-3 Aviation Recorders PI/2";
		url = "http://www.l-3ar.com";
	}
	else if (oem_name == "srvivr")
	{
		desc = "L-3 SRVIVR";
		url = "http://www.l-3com.com";
	}

	string access_info = "Not Logged in.";
	if(_access.LoggedIn())
		access_info = "Logged in as " + _access.GetUsername();
	printf("Content-type: text/html\n\n");
	printf("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">");
	printf("<html>");
	printf("<head>");
	printf("<meta http-equiv=\"Content-Language\" content=\"en-au\" />");
	printf("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />");
	printf("<meta http-equiv=\"TITLE\" content=\"%s - %s\" />", title.c_str(), name.c_str());
	printf("<title>%s - %s</title>", title.c_str(), name.c_str());
	printf("<link rel=\"stylesheet\" type=\"text/css\" media=\"screen, projection\" href=\"%s\" />", css_file.c_str());
	printf("</head>");

	printf("<body bgcolor=\"#ffffff\" marginwidth=\"0\" marginheight=\"0\" leftmargin=\"0\" topmargin=\"0\">");
	printf("<form action=\"/%s\" method=\"post\">", APPNAME);
	printf("<table border=\"0\" cellpadding=\"10\" cellspacing=\"0\" width=\"100%%\" align=\"center\">");
	printf("<tr>");
	printf("<td class=\"cell_heading\" valign=\"center\">&nbsp</td>");
	printf("<td class=\"cell_heading_logo\" valign=\"top\"><a class=\"image_link\" href=\"%s\" ><img src=\"%s\" alt=\"FDS\" border=\"0\" /></a></td>", url.c_str(), logo_file.c_str());
	printf("<td class=\"cell_heading\" valign=\"center\" width=\"100%%\">%s</td>", title.c_str());
	printf("<td class=\"cell_login_status\" valign=\"center\">%s", access_info.c_str());
	if(_access.LoggedIn())
	{
		printf("&nbsp");
		printf("<input type=\"hidden\" name=\"" REF_ACCESS "\" value=\"logout\">");
		printf("<input type=\"hidden\" name=\"" REF_PAGE "\" value=\"%s\">", MAIN_PAGE_TITLE);
		printf("<input type=submit name=\"" REF_ACCESS "\" value=\"%s\" class=\"button_logout\">", BTN_ACCESS_LOGOUT);
	}
	printf("</td>");
	printf("</tr>");
	printf("</table>");
	printf("<table border=\"0\" cellpadding=\"5\" cellspacing=\"0\" width=\"98%%\" align=\"center\">");
	printf("<tr><td class=\"cell_info\">");
	printf("%s", desc.c_str());
	printf("</td></tr>");
	printf("</table>");
}

void PageTemplate::Footer(string footer)
{
	printf("</form>");
	printf("<table border=\"0\" cellpadding=\"10\" cellspacing=\"0\" width=\"100%%\" align=\"center\">");
	printf("<tr>");
	printf("<td class=\"main_space\">&nbsp;</td>");
	printf("<td class=\"main_right\" valign=\"top\" align=\"right\"></td>");
	printf("</tr>");
	printf("<tr>");
	printf("<td colspan=\"3\" valign=\"top\">%s&nbsp;</td>", footer.c_str());
	printf("</tr>");
	printf("</table>");
	printf("</body>");
	printf("</html>");
}
