
#ifndef __PAGE_LOGIN_H
#define __PAGE_LOGIN_H

#include "page.h"

class LoginPage : public Page
{
	public:
		LoginPage(string name);
		virtual ~LoginPage(void) {}

		virtual void Show(char **postvars);
};

extern LoginPage login_page;

#endif
