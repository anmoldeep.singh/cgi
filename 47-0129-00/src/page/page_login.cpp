
#include "page_login.h"
#include "access.h"

LoginPage login_page(LOGIN_PAGE_TITLE);


LoginPage::LoginPage(string name) : Page(name)
{
}

void LoginPage::Show(char **postvars)
{
	printf("<form action=\"/%s\" name=\"login\" method=\"post\">", APPNAME);
	printf("<table border=\"0\" cellpadding=\"10\" cellspacing=\"0\" align=\"center\">");
	printf("<tr>");
	printf("<td colspan=\"2\" class=\"text_body\">");
	printf("<br>");
	printf("</td>");
	printf("</tr>");

	printf("<tr>");
	printf("<td class=\"labelcell_login\">Username:</td>");
	printf("<td class=\"fieldcell_login\">");
	printf("<input type=\"text\" name=\"" REF_USERNAME "\" value=\"\">");
	printf("</td>");
	printf("</tr>");

	printf("<tr>");
	printf("<td class=\"labelcell_login\">Password:</td>");
	printf("<td class=\"fieldcell_login\">");
	printf("<input type=\"password\" name=\"" REF_PASSWORD "\" value=\"\">");
	printf("</td>");
	printf("</tr>");

	printf("<tr>");
	printf("<td class=\"labelcell_login\"></td>");
	printf("<td class=\"fieldcell_login\">");
	printf("<input type=\"hidden\" name=\"" REF_PAGE "\" value=\"%s\">", MAIN_PAGE_TITLE);
	printf("<input type=submit name=\"" REF_ACCESS "\" value=\"" BTN_ACCESS_LOGIN "\" class=\"buttons\">");
	printf("</td>");
	printf("</tr>");
	printf("</table>");
	printf("</form>");
}
