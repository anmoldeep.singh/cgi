#ifndef __PAGE_TEMPLATE_H
#define __PAGE_TEMPLATE_H

#include "common.h"

class PageTemplate
{
	public:
		PageTemplate(void);
		virtual ~PageTemplate(void) {}

		void Header(string name = "", string title = Settings::Instance().String("product.name"));
		void Footer(string footer = "");
};

extern PageTemplate page_template;

#endif
