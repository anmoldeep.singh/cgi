

#ifndef _LISTS_H_
#define _LISTS_H_

#include <list>

using std::list;


template<class T> class Lists
{
private:
	typedef list< T *>           			ListsType;
	typedef typename list< T *>::iterator	ListsIterType;

	ListsType& m_list;
	ListsIterType m_it;

public:
	Lists(ListsType& _list) : m_list(_list) {}
	~Lists() {}

	T *FirstItem(void)
	{
		m_it = m_list.begin();
		if(m_it != m_list.end())
			return *m_it;
		return NULL;
	}

	T *LastItem(void)
	{
		return m_list.back();
	}

	T *NextItem(void)
	{
		m_it++;
		if(m_it != m_list.end())
		{
			return *m_it;
		}
		return NULL;
	}

};

#endif

