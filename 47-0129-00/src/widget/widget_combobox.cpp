
#include "widget_combobox.h"

ComboBox::ComboBox(string name, Settings *settings, string ref_name, const char **string_list, int choices) : Widget(name, settings, ref_name)
{
	m_string_list = string_list;
	m_choices = choices;
}


void ComboBox::Show(void)
{
	ReadSetting();

	printf("<tr>");
	printf("<td class=\"labelcell\" width=\"%d%%\">%s:</td>", m_left_proportion, m_name.c_str());
	printf("<td class=\"fieldcell\" width=\"%d%%\">", m_right_proportion);
	printf("<select name=\"%s\" id=\"%s\">", m_ref_name.c_str(), m_ref_name.c_str());
	//for(int i = 0; i < sizeof(m_string_list)/sizeof(const char *); i++)
	for(int i = 0; i < m_choices; i++)
	{
		printf("<option %s value=\"%s\" >%s</option>", (m_selection == m_string_list[i] ? "selected" : ""), m_string_list[i], m_string_list[i]);
	}
	printf("</select>");
	printf("</td>");
	printf("</tr>");
}


void ComboBox::SetSelection(string selection)
{
	m_selection = selection;
}


string ComboBox::GetSelection(void)
{
	return m_selection;
}


void ComboBox::ReadSetting(void)
{
	printf("<p class=\"hidden_operation_status_area\">");
	if(m_settings) m_selection = m_settings->String(m_ref_name);
	printf("</p>");
}


void ComboBox::ApplySetting(void)
{
	if(m_settings) m_settings->String( m_ref_name, m_selection );
}
