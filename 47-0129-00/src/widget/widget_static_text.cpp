
#include "widget_static_text.h"

StaticTextBox::StaticTextBox(string name, string text) : Widget(name, NULL, "")
{
	SetText(text);
}

void StaticTextBox::SetText(string text)
{
	m_text = text;
}

void StaticTextBox::Show(void)
{
	printf("<tr>");
	printf("<td class=\"labelcell\" width=\"%d%%\">%s:</td>", m_left_proportion, m_name.c_str());
	printf("<td class=\"fieldcell\" width=\"%d%%\">", m_right_proportion);
	printf("%s", m_text.c_str());
	printf("</td>");
	printf("</tr>");
}


