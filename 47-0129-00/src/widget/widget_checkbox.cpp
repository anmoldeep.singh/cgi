
#include "widget_checkbox.h"

CheckBox::CheckBox(string name, Settings *settings, string ref_name) : Widget(name, settings, ref_name)
{
}

void CheckBox::Show(void)
{
	ReadSetting();

	printf("<tr>");
	printf("<td class=\"labelcell\" width=\"%d%%\">%s:</td>", m_left_proportion, m_name.c_str());
	printf("<td class=\"fieldcell\" width=\"%d%%\">", m_right_proportion);
	printf("<input type=\"checkbox\" name=\"%s\" value=\"checked\" %s>", m_ref_name.c_str(), (checked ? "checked" : "") );
	printf("</td>");
	printf("</tr>");
}


void CheckBox::Set(char **postvars)
{
	//Default to unchecked because we only get notified on checked boxes
	if(m_settings) m_settings->String( m_ref_name, "0" );

	for (int i = 0; postvars[i]; i += 2)
	{
		if (m_ref_name == postvars[i])
		{
			if(strcmp(postvars[i+1], "checked") == 0)
			{
				if(m_settings) m_settings->String( m_ref_name, "1" );
			}
//			printf("<p>Checkbox '%s' = '%s'</p>", postvars[i], postvars[i+1]);
		}
	}
}


void CheckBox::Checked(bool select)
{
	checked = select;
}


void CheckBox::Toggle(void)
{
	checked ^= checked;
}


bool CheckBox::IsChecked(void)
{
	return checked;
}


void CheckBox::ReadSetting(void)
{
	printf("<p class=\"hidden_operation_status_area\">");
	if(m_settings) checked = m_settings->String(m_ref_name) == "1" ? true : false;
	printf("</p>");
}


void CheckBox::ApplySetting(void)
{
	if(m_settings) m_settings->Int( m_ref_name, (int)checked );
}
