
#ifndef __WIDGET_DATESELECT_H
#define __WIDGET_DATESELECT_H

#include "widget_combobox.h"


class DateSelect : public Widget
{
	public:
		DateSelect(string name, string ref_name);

		virtual void Set(char **postvars);
                void Show(void);

        protected:
		virtual void ReadSetting(void);
                virtual void ApplySetting(void);
};

#endif
