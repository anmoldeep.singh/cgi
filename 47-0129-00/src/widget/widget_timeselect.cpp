
#include "widget_timeselect.h"

#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>


TimeSelect::TimeSelect(string name, string ref_name) : 
	Widget(name, NULL, ref_name)
{
}

void TimeSelect::Set(char **postvars)
{
        for (int i = 0; postvars[i]; i += 2)
                if (m_ref_name == postvars[i])
                {
			char *time_string = postvars[i+1];

			char buf[128];
			snprintf(buf, sizeof(buf), "date +%%T -s %s", time_string); 
			system(buf);
			system("hwclock -w");
                }
}

void TimeSelect::ReadSetting(void)
{
        printf("<p class=\"hidden_operation_status_area\">");
        printf("</p>");
}

void TimeSelect::Show(void)
{
        ReadSetting();

	time_t now;
	time(&now);
	tm *ts = localtime(&now);

	char time_string[80];
	strftime(time_string, sizeof(time_string), "%H:%M", ts);

        printf("<tr>");
        printf("<td class=\"labelcell\" width=\"%d%%\">%s:</td>", m_left_proportion, m_name.c_str());
        printf("<td class=\"fieldcell\" width=\"%d%%\">", m_right_proportion);
        printf("<input type=\"time\" name=\"%s\" id=\"%s\" value=\"%s\">", m_ref_name.c_str(), m_ref_name.c_str(), time_string);
        printf("</td>");
        printf("</tr>");
}

void TimeSelect::ApplySetting(void)
{
}

