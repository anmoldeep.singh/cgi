
#include "widget_dateselect.h"

#include <stdlib.h>
#include <sys/time.h>
#include <time.h>


DateSelect::DateSelect(string name, string ref_name) : 
	Widget(name, NULL, ref_name)
{
}

void DateSelect::Set(char **postvars)
{
        for (int i = 0; postvars[i]; i += 2)
                if (m_ref_name == postvars[i])
                {
			char *date_string = postvars[i+1];

			char buf[64];
			snprintf(buf, 64, "date -s %s+`date +%%H:%%M:%%S`", date_string);
			system(buf);
			system("hwclock -w");
                }
}

void DateSelect::ReadSetting(void)
{
        printf("<p class=\"hidden_operation_status_area\">");
        printf("</p>");
}

void DateSelect::Show(void)
{
        ReadSetting();

	time_t now;
	time(&now);
	tm *ts = localtime(&now);

	char date_string[80];
	strftime(date_string, sizeof(date_string), "%Y-%m-%d", ts);

        printf("<tr>");
        printf("<td class=\"labelcell\" width=\"%d%%\">%s:</td>", m_left_proportion, m_name.c_str());
        printf("<td class=\"fieldcell\" width=\"%d%%\">", m_right_proportion);
        printf("<input type=\"date\" name=\"%s\" id=\"%s\" value=\"%s\">", m_ref_name.c_str(), m_ref_name.c_str(), date_string);
        printf("</td>");
        printf("</tr>");
}

void DateSelect::ApplySetting(void)
{
}

