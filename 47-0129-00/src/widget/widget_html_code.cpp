
#include "widget_html_code.h"

HTMLCode::HTMLCode(string code) : Widget("HTMLCode", NULL, "")
{
	m_code = code;
}

void HTMLCode::Show(void)
{
	printf(m_code.c_str());
}
