
#ifndef __WIDGET_HORIZONTAL_LINE_H
#define __WIDGET_HORIZONTAL_LINE_H

#include <stdio.h>
#include "common.h"
#include "widget.h"

#define LINE_WIDTH_PROPORTION	80

class HLine : public Widget
{
	public:
		HLine(int width_proportion = LINE_WIDTH_PROPORTION);

		void Show(void);
	protected:
		void ReadSetting(void) {};
		void ApplySetting(void) {};
		int m_width_proportion;
};


#endif