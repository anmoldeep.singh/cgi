
#include "widget_button.h"

Button::Button(string desc, string name, string ref) : Widget(name, NULL, "")
{
	m_desc = desc;
	m_ref = ref;
}

void Button::Show(void)
{
	printf("<tr>");
	printf("<td class=\"labelcell\" width=\"%d%%\">%s%s</td>", m_left_proportion, m_desc.c_str(), m_desc.size() ? ":" : "");
	printf("<td class=\"fieldcell\" width=\"%d%%\">", m_right_proportion);
	printf("<input type=submit name=\"%s\" value=\"%s\">", m_ref.c_str(), m_name.c_str());
	printf("</td>");
	printf("</tr>");
}


