
#include "widget_encryption_key.h"
#include "widget_encryption_key_js.h"


EncryptionKey::EncryptionKey(string name, Settings *settings, string ref_name) : Widget(name, settings, ref_name)
{
	m_box_size = 20;
	m_max_length = 64;
}

void EncryptionKey::Set(char **postvars)
{
	for (int i = 0; postvars[i]; i += 2)
		if (m_ref_name == postvars[i])
		{
			string value = postvars[i+1];
			if (value != GetMockKey())
			{
				if(m_settings)	m_settings->String(m_ref_name, value);
			}
		}
}

void EncryptionKey::Head(void)
{
	printf(javascript_code);
}

void EncryptionKey::Show(void)
{
	ReadSetting();

	printf("<tr>");
	printf("<td class=\"labelcell\" width=\"%d%%\">%s:</td>", m_left_proportion, m_name.c_str());
	printf("<td class=\"fieldcell\" width=\"%d%%\">", m_right_proportion);

	printf("<input type=\"text\" name=\"%s\" id=\"%s\" size=\"%d\" maxlength=\"%d\" value=\"%s\">", m_ref_name.c_str(), m_ref_name.c_str(), m_box_size, m_max_length, GetMockKey().c_str());

	printf("</td>");
	printf("</tr>");

	printf("<tr>");
	printf("<td class=\"labelcell\" width=\"%d%%\">&nbsp</td>", m_left_proportion);
	printf("<td class=\"fieldcell\" width=\"%d%%\">", m_right_proportion);
	printf("<input type=\"button\" value=\"Perform WPA Hash\" id=\"wpa_hash_button\" onclick=\"fcalc()\">");
	printf("</td>");
	printf("</tr>");
}

void EncryptionKey::SetEncryptionKey(string text)
{
	m_text = text;
}

string EncryptionKey::GetEncryptionKey(void)
{
	return m_text;
}

string EncryptionKey::GetMockKey(void)
{
	unsigned int key_length = 0;
	if(m_settings) key_length = m_settings->String(m_ref_name).length();
	string mock_key;

	for (unsigned int i = 0; i < key_length; i++)
		mock_key += "*";

	return mock_key;
}

void EncryptionKey::ReadSetting(void)
{
	printf("<p class=\"hidden_operation_status_area\">");
	if(m_settings) m_text = m_settings->String(m_ref_name);
	printf("</p>");
}

void EncryptionKey::ApplySetting(void)
{
	if (m_text != GetMockKey())
	{
		if(m_settings)	m_settings->String(m_ref_name, m_text);
	}
}
