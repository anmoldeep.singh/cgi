
#include "widget_ftpcreate.h"

FTPCreate::FTPCreate(string name, Settings *settings, string ref_name) : Widget(name, settings, ref_name)
{
	m_box_size = 20;
	m_max_length = 64;
}

void FTPCreate::Show(void)
{
	ReadSetting();

	printf("<tr>");
	printf("<td class=\"labelcell\" width=\"%d%%\">%s:</td>", m_left_proportion, m_name.c_str());
	printf("<td class=\"fieldcell\" width=\"%d%%\">", m_right_proportion);
	printf("<input type=\"text\" name=\"%s\" id=\"%s\" size=\"%d\" maxlength=\"%d\" value=\"%s\">", m_ref_name.c_str(), m_ref_name.c_str(), m_box_size, m_max_length, m_text.c_str());
	printf("</td>");
	printf("</tr>");
}

void FTPCreate::Set(char **postvars)
{
}

void FTPCreate::SetText(string text)
{
	m_text = text;
}


string FTPCreate::GetText(void)
{
	return m_text;
}


void FTPCreate::ReadSetting(void)
{
	printf("<p class=\"hidden_operation_status_area\">");
	//if(m_settings) m_text = m_settings->String(m_ref_name);
	printf("</p>");
}


void FTPCreate::ApplySetting(void)
{
	//if(m_settings) m_settings->String( m_ref_name, m_text );
}