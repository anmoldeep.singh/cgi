
#ifndef __WIDGET_EDIT_USERNAME_H
#define __WIDGET_EDIT_USERNAME_H

#include <stdio.h>
#include "common.h"
#include "widget.h"


class EditUsername : public Widget
{
	public:
		EditUsername(string name, string username);

		void Show(void);
		void SetUsername(string username);
		string GetUsername(void);
	protected:
		void ReadSetting(void) {};
		void ApplySetting(void) {};
		string m_username;
		int m_box_size;
		int m_max_length;
};


#endif