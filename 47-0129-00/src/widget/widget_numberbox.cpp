
#include "widget_numberbox.h"

NumberBox::NumberBox(string name, Settings *settings, string ref_name) : Widget(name, settings, ref_name)
{
	m_box_size = 20;
	m_max_length = 64;
	m_min = 0;
	m_step = 1;
}

void NumberBox::Show(void)
{
	ReadSetting();

	printf("<tr>");
	printf("<td class=\"labelcell\" width=\"%d%%\">%s:</td>", m_left_proportion, m_name.c_str());
	printf("<td class=\"fieldcell\" width=\"%d%%\">", m_right_proportion);
	printf("<input type=\"number\" name=\"%s\" id=\"%s\" size=\"%d\" maxlength=\"%d\" value=\"%d\" min=\"%d\" step=\"%d\">", m_ref_name.c_str(), m_ref_name.c_str(), m_box_size, m_max_length, m_num, m_min, m_step);
	printf("</td>");
	printf("</tr>");
}


void NumberBox::SetNumber(int num)
{
	m_num = num;
}


int NumberBox::GetNumber(void)
{
	return m_num;
}


void NumberBox::ReadSetting(void)
{
	printf("<p class=\"hidden_operation_status_area\">");
	if(m_settings) m_num = m_settings->Int(m_ref_name);
	printf("</p>");
}


void NumberBox::ApplySetting(void)
{
	if(m_settings) m_settings->Int( m_ref_name, m_num );
}
