
#ifndef __WIDGET_FILE_SELECT_H
#define __WIDGET_FILE_SELECT_H

#include <stdio.h>
#include "common.h"
#include "widget.h"


class FileSelect : public Widget
{
	public:
		FileSelect(string name, string ref);

		void Show(void);
	protected:
		void ReadSetting(void) {};
		void ApplySetting(void) {};
		string m_filename;
		string m_ref;
		int m_box_size;
		int m_max_length;
};


#endif