
#ifndef __WIDGET_STATIC_TEXT_H
#define __WIDGET_STATIC_TEXT_H

#include <stdio.h>
#include "common.h"
#include "widget.h"


class StaticTextBox : public Widget
{
	public:
		StaticTextBox(string name, string text);
		void SetText(string text);

		void Show(void);

	protected:
		void ReadSetting(void) {};
		void ApplySetting(void) {};
		string m_text;
};


#endif
