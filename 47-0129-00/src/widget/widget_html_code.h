
#ifndef __WIDGET_HTML_CODE_H
#define __WIDGET_HTML_CODE_H

#include <stdio.h>
#include "common.h"
#include "widget.h"


class HTMLCode : public Widget
{
	public:
		HTMLCode(string code);
		void Show(void);
	protected:
		void ReadSetting(void) {};
		void ApplySetting(void) {};
		string m_code;
};


#endif