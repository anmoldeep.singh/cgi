
#ifndef __WIDGET_TEXTBOX_H
#define __WIDGET_TEXTBOX_H

#include <stdio.h>
#include "common.h"
#include "widget.h"


class TextBox : public Widget
{
	public:
		TextBox(string name, Settings *settings, string ref_name);

		void Show(void);
		void SetText(string text);
		string GetText(void);
	protected:
		void ReadSetting(void);
		void ApplySetting(void);
		string m_text;
		int m_box_size;
		int m_max_length;
};


#endif