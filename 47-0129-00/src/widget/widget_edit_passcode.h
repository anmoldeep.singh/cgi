
#ifndef __WIDGET_EDIT_PASSCODE_H
#define __WIDGET_EDIT_PASSCODE_H

#include <stdio.h>
#include "common.h"
#include "widget.h"


class EditPasscode : public Widget
{
	public:
		EditPasscode(string name, Settings *settings, string m_ref_name, bool hide = false);

		void Show(void);
		void SetPasscode(unsigned int passcode);
		unsigned int GetPasscode(void);
	protected:
		void ReadSetting(void);
		void ApplySetting(void);
		unsigned int m_passcode;
		int m_box_size;
		int m_max_length;
		bool m_hide;
};


#endif