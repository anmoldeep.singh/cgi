
#ifndef __WIDGET_RADIO_H
#define __WIDGET_RADIO_H

#include <stdio.h>
#include "common.h"
#include "widget.h"


class Radio : public Widget
{
	public:
		Radio(string name, Settings *settings, string ref_name, string value);

		void Show(void);

		void Checked(bool select = true);
		bool IsChecked(void);

		void SetAdditional(string additional);

	protected:
		void ReadSetting(void);
		void ApplySetting(void);
		
		string m_value;
		bool m_checked;

		string m_additional;
};


#endif