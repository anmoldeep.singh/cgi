
#include "widget_edit_passcode.h"

EditPasscode::EditPasscode(string name, Settings *settings, string m_ref_name, bool hide) : Widget(name, settings, m_ref_name)
{
	m_passcode = 0;
	m_box_size = 20;
	m_max_length = 4;
	m_hide = hide;
}

void EditPasscode::Show(void)
{
	ReadSetting();

	printf("<tr>");
	printf("<td class=\"labelcell\" width=\"%d%%\">%s:</td>", m_left_proportion, m_name.c_str());
	printf("<td class=\"fieldcell\" width=\"%d%%\">", m_right_proportion);
	printf("<input type=\"%s\" name=\"%s\" size=\"%d\" maxlength=\"%d\" value=\"%04d\">", m_hide ? "passcode" : "text", m_ref_name.c_str(), m_box_size, m_max_length, m_passcode);
	printf("</td>");
	printf("</tr>");
}

void EditPasscode::SetPasscode(unsigned int passcode)
{
	m_passcode = passcode;
}

unsigned int EditPasscode::GetPasscode(void)
{
	return m_passcode;
}

void EditPasscode::ReadSetting(void)
{
	printf("<p class=\"hidden_operation_status_area\">");
	if(m_settings) m_passcode = m_settings->Int(m_ref_name);
	printf("</p>");
}

void EditPasscode::ApplySetting(void)
{
	if(m_settings) m_settings->Int( m_ref_name, m_passcode );
}