
#include "widget_edit_password.h"

EditPassword::EditPassword(string name, string ref_name, bool hide) : Widget(name, NULL, ref_name)
{
	m_box_size = 20;
	m_max_length = 64;
	m_hide = hide;
}

void EditPassword::Show(void)
{
	printf("<tr>");
	printf("<td class=\"labelcell\" width=\"%d%%\">%s:</td>", m_left_proportion, m_name.c_str());
	printf("<td class=\"fieldcell\" width=\"%d%%\">", m_right_proportion);
	printf("<input type=\"%s\" name=\"%s\" size=\"%d\" maxlength=\"%d\" value=\"\">", m_hide ? "password" : "text", m_ref_name.c_str(), m_box_size, m_max_length);
	printf("</td>");
	printf("</tr>");
}

