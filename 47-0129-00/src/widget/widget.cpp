
#include "widget.h"

Widget::Widget(string name, Settings *settings, string ref_name)
{
	m_name = name,
	m_settings = settings;
	m_ref_name = ref_name;
	m_left_proportion = WIDGET_LEFT_PROPORTION;
	m_right_proportion = WIDGET_RIGHT_PROPORTION;
}

void Widget::Set(char **postvars)
{
	for (int i = 0; postvars[i]; i += 2)
	{
		if (m_ref_name == postvars[i])
		{
			if(m_settings)
				m_settings->String( m_ref_name, postvars[i+1] );
			else
				printf("Set Failed for '%s'", m_ref_name.c_str());
		}
	}
}