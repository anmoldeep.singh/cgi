
#ifndef __WIDGET_NUMBERBOX_H
#define __WIDGET_NUMBERBox_H

#include <stdio.h>
#include "common.h"
#include "widget.h"


class NumberBox : public Widget
{
	public:
		NumberBox(string name, Settings *settings, string ref_name);

		void Show(void);
		void SetNumber(int num);
		int GetNumber(void);
	protected:
		void ReadSetting(void);
		void ApplySetting(void);
		int m_num;
		int m_box_size;
		int m_max_length;
		int m_min;
		int m_step;
};


#endif
