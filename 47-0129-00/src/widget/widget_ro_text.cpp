
#include "widget_ro_text.h"

ReadOnlyTextBox::ReadOnlyTextBox(string name, Settings *settings, string ref_name) : Widget(name, settings, ref_name)
{
	m_data_source = 0;
}

ReadOnlyTextBox::ReadOnlyTextBox(string name, bool(*func_ptr)(string&)) : Widget(name, NULL, "")
{
	m_data_source = 1;
	m_func_ptr = func_ptr;
}
	

void ReadOnlyTextBox::Show(void)
{
	ReadSetting();

	printf("<tr>");
	printf("<td class=\"labelcell\" width=\"%d%%\">%s:</td>", m_left_proportion, m_name.c_str());
	printf("<td class=\"fieldcell\" width=\"%d%%\">", m_right_proportion);
	printf("%s", m_text.c_str());
	printf("</td>");
	printf("</tr>");
}

void ReadOnlyTextBox::ReadSetting(void)
{
	printf("<p class=\"hidden_operation_status_area\">");
	if(m_data_source == 0)
	{
		if(m_settings) m_text = m_settings->String(m_ref_name);
	}
	else if (m_data_source == 1)
		(*m_func_ptr)(m_text);
	printf("</p>");
}

