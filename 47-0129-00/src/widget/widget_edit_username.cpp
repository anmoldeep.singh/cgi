
#include "widget_edit_username.h"

EditUsername::EditUsername(string name, string username) : Widget(name, NULL, "")
{
	m_username = username;
	m_box_size = 20;
	m_max_length = 64;
}

void EditUsername::Show(void)
{
	printf("<tr>");
	printf("<td class=\"labelcell\" width=\"%d%%\">%s:</td>", m_left_proportion, m_name.c_str());
	printf("<td class=\"fieldcell\" width=\"%d%%\">", m_right_proportion);
	printf("<input type=\"text\" name=\"%s\" size=\"%d\" maxlength=\"%d\" value=\"%s\">", m_ref_name.c_str(), m_box_size, m_max_length, m_username.c_str());
	printf("</td>");
	printf("</tr>");
}


void EditUsername::SetUsername(string username)
{
	m_username = username;
}


string EditUsername::GetUsername(void)
{
	return m_username;
}
