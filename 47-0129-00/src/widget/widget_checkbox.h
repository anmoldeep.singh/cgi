
#ifndef __WIDGET_CHECKBOX_H
#define __WIDGET_CHECKBOX_H

#include <stdio.h>
#include "common.h"
#include "widget.h"


class CheckBox : public Widget
{
	public:
		CheckBox(string name, Settings *settings, string ref_name);

		void Show(void);
		void Set(char **postvars);
		void Checked(bool select = true);
		void Toggle(void);
		bool IsChecked(void);

	protected:
		void ReadSetting(void);
		void ApplySetting(void);

		bool checked;
};


#endif