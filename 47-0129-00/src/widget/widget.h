
#ifndef __WIDGET_H
#define __WIDGET_H

#include "common.h"

class Widget
{
	public:
		Widget(string name, Settings *settings, string ref_name);
		virtual ~Widget(void) {}

		virtual void Head(void) {}
		virtual void Show(void) = 0;
		virtual void Set(char **postvars);

	protected:
		virtual void ReadSetting(void) = 0;
		virtual void ApplySetting(void) = 0;
		string m_name;
		string m_ref_name;
		unsigned int m_left_proportion;
		unsigned int m_right_proportion;
		Settings *m_settings;
};

typedef list<Widget*> widget_list_t;


#endif