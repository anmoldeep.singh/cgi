
#ifndef __WIDGET_COMBOBOX_H
#define __WIDGET_COMBOBOX_H

#include <stdio.h>
#include "common.h"
#include "widget.h"

class ComboBox : public Widget
{
	public:
		ComboBox(string name, Settings *settings, string ref_name, const char **string_list, int choices = 1);

		void Show(void);

		void SetSelection(string selection);
		string GetSelection(void);

	protected:
		virtual void ReadSetting(void);
		void ApplySetting(void);

		const char **m_string_list;
		string m_selection;
		int m_choices;
};

#endif
