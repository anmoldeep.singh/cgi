
#ifndef __WIDGET_FILE_ENTRY_H
#define __WIDGET_FILE_ENTRY_H

#include <stdio.h>
#include "common.h"
#include "widget.h"

#include <list>
#include <string>

using std::list;
using std::string;

struct Entry
{
	string filename;
	string size;
	string date;
	bool selected;
};

typedef list<Entry> EntryList;
typedef list<string> DeleteList;


class FileEntry : public Widget
{
	public:
		FileEntry(string name, string mount_point, Settings *settings, string ref_name);
		~FileEntry(void) {}

		virtual void Head(void);
		virtual void Show(void);
		virtual void Set(char **postvars);

	protected:
		virtual void ReadSetting(void);
		virtual void ApplySetting(void);

		virtual void Scan(string directory);

		EntryList entry_list;
		DeleteList delete_list;

	private:
		string mount_point;
};

#endif
