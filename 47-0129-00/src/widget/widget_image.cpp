
#include "widget_image.h"

Image::Image(string name, string filename) : Widget(name, NULL, "")
{
	m_filename = filename;
}

void Image::Show(void)
{
	printf("<tr>");
	printf("<td class=\"labelcell\" width=\"%d%%\">%s:</td>", m_left_proportion, m_name.c_str());
	printf("<td class=\"fieldcell\" width=\"%d%%\">", m_right_proportion);
	printf("<img src=\"%s\">", m_filename.c_str());
	printf("</td>");
	printf("</tr>");
}


