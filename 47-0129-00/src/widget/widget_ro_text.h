
#ifndef __WIDGET_RO_TEXT_H
#define __WIDGET_RO_TEXT_H

#include <stdio.h>
#include "common.h"
#include "widget.h"


class ReadOnlyTextBox : public Widget
{
	public:
		ReadOnlyTextBox(string name, Settings *settings, string ref_name);
		ReadOnlyTextBox(string name, bool(*func_ptr)(string&));

		void Show(void);

	protected:
		void ReadSetting(void);
		void ApplySetting(void) {};
		int m_data_source;
		bool (*m_func_ptr)(string&);
		string m_text;
};


#endif