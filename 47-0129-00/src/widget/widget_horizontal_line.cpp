
#include "widget_horizontal_line.h"

HLine::HLine(int width_proportion) : Widget("HLine", NULL, "")
{
	m_width_proportion = width_proportion;
}

void HLine::Show(void)
{
	printf("<tr>");
	printf("<td colspan=\"2\" class=\"labelcell\" width=\"100%%\"><hr width=\"%d%%\"></td>", m_width_proportion);
	printf("</tr>");
}
