
#include "widget_combobox_brightness.h"

#include <stdlib.h>


ComboBoxBrightness::ComboBoxBrightness(string name, Settings *settings, string ref_name, const char **string_list, int choices) : 
	ComboBox(name, settings, ref_name, string_list, choices)
{
}

void ComboBoxBrightness::Set(char **postvars)
{
        for (int i = 0; postvars[i]; i += 2)
                if (m_ref_name == postvars[i])
                {
			int brightness = ((atoi(postvars[i+1])*67)+473)/9;
                        if(m_settings)  m_settings->Int(m_ref_name, brightness);
                }
}

void ComboBoxBrightness::ReadSetting(void)
{
        printf("<p class=\"hidden_operation_status_area\">");
        if(m_settings)
	{
		unsigned int value = m_settings->Int(m_ref_name);

		if (value < 60) value = 60;
		if (value > 127) value = 127;

		m_selection = toString((value*9-473+33)/67);
	}
        printf("</p>");
}

