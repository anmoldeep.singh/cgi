
#include "widget_radio.h"

Radio::Radio(string name, Settings *settings, string ref_name, string value) : Widget(name, settings, ref_name)
{
	m_value = value;
}

void Radio::Show(void)
{
	ReadSetting();

	printf("<tr>");
	printf("<td class=\"labelcell\" width=\"%d%%\">%s:</td>", m_left_proportion, m_name.c_str());
	printf("<td class=\"fieldcell\" width=\"%d%%\">", m_right_proportion);
	printf("<input type=\"radio\" name=\"%s\" value=\"%s\" %s %s>", m_ref_name.c_str(), m_value.c_str(), (m_checked ? "checked" : ""), m_additional.c_str());
	printf("</td>");
	printf("</tr>");
}

void Radio::Checked(bool select)
{
	m_checked = select;
}


bool Radio::IsChecked(void)
{
	return m_checked;
}

void Radio::SetAdditional(string additional)
{
	m_additional = additional;
}

void Radio::ReadSetting(void)
{
	printf("<p class=\"hidden_operation_status_area\">");
	if(m_settings) m_checked = m_settings->String(m_ref_name) == m_value ? true : false;
	printf("</p>");
}


void Radio::ApplySetting(void)
{
	if(m_checked)
	{
		if(m_settings) m_settings->String( m_ref_name, m_value );
	}
}
