
#include "widget_file_entry.h"

#include "system_functions.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/dir.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/vfs.h>



FileEntry::FileEntry(string name, string mount_point, Settings *settings, string ref_name) : Widget(name, settings, ref_name)
{
	FileEntry::mount_point = mount_point;
}

void FileEntry::Set(char **postvars)
{
	string filename;
	bool download = false;
	bool checkbox = false;
	bool format = false;
	delete_list.clear();
	string this_mount_point, format_mount_point, download_mount_point, delete_mount_point;

	string fname = "/tmp/postvars_" + m_name + ".txt";

	FILE *out = fopen(fname.c_str(), "wb");

	for (int i = 0; postvars[i]; i += 2)
	{
		fprintf(out, "postvars[i]: %s\n", postvars[i]);
		fprintf(out, "postvars[i+1]: %s\n", postvars[i+1]);

		if (strcmp(postvars[i], REF_DOWNLOAD) == 0)
		{
			download = true;
			continue;
		}

		if (strcmp(postvars[i], REF_CHECKBOX) == 0)
		{
			checkbox = true;
			continue;
		}

		if (strcmp(postvars[i], REF_FILENAME) == 0 && download)
		{
			download_mount_point = this_mount_point;
			filename = postvars[i+1];
			break;
		}
		
		if (strcmp(postvars[i], REF_FILENAME) == 0 && checkbox)
		{
			checkbox = false;
			delete_mount_point = this_mount_point;
			delete_list.push_back(postvars[i+1]);
		}

		if (strcmp(postvars[i], REF_MOUNT_POINT) == 0)
			this_mount_point = postvars[i+1];

		if (strcmp(postvars[i], REF_ACTION) == 0 &&
			strcmp(postvars[i+1], REF_FORMAT) == 0)
		{
			format_mount_point = this_mount_point;
			format = true;
			break;
		}
	}

	fprintf(out, "mount_point: %s\n", mount_point.c_str());
	fprintf(out, "format_mount_point: %s\n", format_mount_point.c_str());
	fprintf(out, "download_mount_point: %s\n", download_mount_point.c_str());
	fprintf(out, "delete_mount_point: %s\n", delete_mount_point.c_str());
	fprintf(out, "filename: %s\n", filename.c_str());


	if (format && format_mount_point == mount_point)
	{
		system_functions.Format(format_mount_point);
	}
	else if (download && download_mount_point == mount_point)
	{
		string filepath = download_mount_point + "/" + filename;
		system_functions.SendFile(filepath.c_str(), filename.c_str());
	}
	else if (delete_list.size() && delete_mount_point == mount_point)
		system_functions.DeleteFiles(mount_point, delete_mount_point + "/", delete_list);

	fclose(out);
}

void FileEntry::Head(void)
{
	printf("\
<script language=\"javascript\">\
function checkAll(){\
	for (var i=0;i<document.forms[0].elements.length;i++)\
	{\
		var e=document.forms[0].elements[i];\
		if ((e.name != 'allbox') && (e.type=='checkbox'))\
		{\
			e.checked=document.forms[0].allbox.checked;\
		}\
	}\
}\
</script>\
	");

}

void FileEntry::Show(void)
{
	Scan(mount_point);
	Head();

	printf("<tr><td align=\"center\" width=\"100%%\" colspan=\"2\" class=\"fieldcell\">");

	printf("<table border=\"1\" cellpadding=\"2\" cellspacing=\"0\" class=\"fieldcell\" width=\"80%%\">");
	printf("<tr bgcolor=\"#DDDDDD\" align=\"center\">");
	printf("<td width=\"1%%\"><b>Select All</b><input type=checkbox name=\"allbox\" onclick=\"checkAll();\"></td>");
	printf("<td width=\"40%%\"><b>Name</b></td>");
	printf("<td width=\"8%%\"><b>Size</b></td>");
	printf("<td width=\"15%%\"><b>Date</b></td>");
	printf("<td width=\"1%%\">&nbsp</td>");
	printf("</tr>\n");

	EntryList::iterator it;

	for (it = entry_list.begin();
			it != entry_list.end();
			it++)
	{
		Entry& entry = *it;

		printf("<tr>");

		printf("<td class=\"fieldcell\">");
		printf("<input type=hidden name=\"%s\" value=\"%s\">", REF_MOUNT_POINT, mount_point.c_str());
		printf("<input type=checkbox name=\"%s\" value=\"checked\" false>", REF_CHECKBOX);
		printf("<input type=hidden name=\"%s\" value=\"%s\">", REF_FILENAME, entry.filename.c_str());
		printf("</td>");

//		printf("<td align=\"center\">&nbsp</td>");
		printf("<td align=\"left\">%s</td>", entry.filename.c_str());
		printf("<td align=\"right\">%s</td>", entry.size.c_str());
		printf("<td align=\"left\">%s</td>", entry.date.c_str());

		printf("<td>");
		printf("<input type=submit name=\"%s\" value=\"Download\">", REF_DOWNLOAD);
		printf("<input type=hidden name=\"%s\" value=\"%s\">", REF_FILENAME, entry.filename.c_str());
		printf("</td>");

		printf("</tr>\n");
	}

	printf("</table>");
	printf("<br>");

	printf("<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"fieldcell\" width=\"80%%\">");
	printf("<tr bgcolor=\"#DDDDDD\" align=\"center\">");
	printf("<tr class=\"fieldcell\">");
	printf("<td align=\"left\" width=\"50%%\">");
	printf("<input type=submit name=\"%s\" value=\"%s\"onClick=\"return confirm('Are you sure you want to delete these item(s)?');\">", REF_ACTION, REF_DELETE);
	printf("</td>");
	printf("<td align=\"right\" width=\"50%%\">");
	printf("<input type=submit name=\"%s\" value=\"%s\" onClick=\"return confirm('Are you sure you want to format the %s storage?');\">", REF_ACTION, REF_FORMAT, m_name.c_str());
	printf("</td>");
	printf("</tr>\n");
	printf("</table>");

	printf("</td>");
	printf("</tr>");
}

void FileEntry::ReadSetting(void)
{
}

void FileEntry::ApplySetting(void)
{
}

void FileEntry::Scan(string directory)
{
	DIR *DirPtr = opendir(directory.c_str());
	
	if (DirPtr == NULL)
	{
		printf("Opendir returned error: %s\n\r", directory.c_str());
		return;
	}

	while (true)
	{
		struct direct *DirEntryPtr = readdir(DirPtr);

		if (DirEntryPtr == 0)
		{
			closedir(DirPtr);
			break;
		}

		if (strcmp(DirEntryPtr->d_name,".") == 0 ||
			strcmp(DirEntryPtr->d_name,"..") == 0)
				continue;

		string path = directory + string("/") + string(DirEntryPtr->d_name);

		struct stat Stat;
		stat(path.c_str(), &Stat);

		if (S_ISREG(Stat.st_mode))
		{
			Entry entry;
			entry.filename = DirEntryPtr->d_name;

			if (Stat.st_size < 1024)
				entry.size = toString(Stat.st_size) + " B";
			else
				entry.size = toString(Stat.st_size/1024) + " KB";
			
			entry.date = ctime(&Stat.st_mtime);
			entry_list.push_back(entry);
		}
	}
}

