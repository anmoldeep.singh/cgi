
#ifndef __WIDGET_TIMESELECT_H
#define __WIDGET_TIMESELECT_H

#include "widget_combobox.h"


class TimeSelect : public Widget
{
	public:
		TimeSelect(string name, string ref_name);

		virtual void Set(char **postvars);
                void Show(void);

        protected:
		virtual void ReadSetting(void);
                virtual void ApplySetting(void);
};

#endif
