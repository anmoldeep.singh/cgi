
#ifndef __WIDGET_FTPRENAME_H
#define __WIDGET_FTPRENAME_H

#include <stdio.h>
#include "common.h"
#include "widget.h"


class FTPRename : public Widget
{
	public:
		FTPRename(string name, Settings *settings, string identifier);
		~FTPRename(void) {}

		virtual void Show(void);
		virtual void Set(char **postvars);

	protected:
		virtual void ReadSetting(void);
		virtual void ApplySetting(void){}

		string m_text;
		int m_box_size;
		int m_max_length;
};


#endif