
#ifndef __WIDGET_BUTTON_H
#define __WIDGET_BUTTON_H

#include <stdio.h>
#include "common.h"
#include "widget.h"


class Button : public Widget
{
	public:
		Button(string desc, string name, string ref = "button");

		void Show(void);

	protected:
		void ReadSetting(void) {};
		void ApplySetting(void) {};
		string m_desc;
		string m_ref;
};


#endif