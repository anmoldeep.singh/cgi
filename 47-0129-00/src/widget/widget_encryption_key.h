
#ifndef __WIDGET_ENCRYPTION_KEY_H
#define __WIDGET_ENCRYPTION_KEY_H

#include <stdio.h>
#include "common.h"
#include "widget.h"


class EncryptionKey : public Widget
{
	public:
		EncryptionKey(string name, Settings *settings, string ref_name);
		~EncryptionKey(void) {}

		virtual void Head(void);
		virtual void Show(void);
		virtual void Set(char **postvars);

		void SetEncryptionKey(string text);
		string GetEncryptionKey(void);
		string GetMockKey(void);

	protected:
		void ReadSetting(void);
		void ApplySetting(void);
		string m_text;
		int m_box_size;
		int m_max_length;
};

#endif
