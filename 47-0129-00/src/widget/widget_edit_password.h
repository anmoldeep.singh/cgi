
#ifndef __WIDGET_EDIT_PASSWORD_H
#define __WIDGET_EDIT_PASSWORD_H

#include <stdio.h>
#include "common.h"
#include "widget.h"


class EditPassword : public Widget
{
	public:
		EditPassword(string name, string refname, bool hide = true);

		void Show(void);
	protected:
		void ReadSetting(void) {};
		void ApplySetting(void) {};
		int m_box_size;
		int m_max_length;
		bool m_hide;
};


#endif