
#ifndef __WIDGET_SPACER_H
#define __WIDGET_SPACER_H

#include <stdio.h>
#include "common.h"
#include "widget.h"


class Spacer : public Widget
{
	public:
		Spacer(void);
		void Show(void);
	protected:
		void ReadSetting(void) {};
		void ApplySetting(void) {};
};


#endif