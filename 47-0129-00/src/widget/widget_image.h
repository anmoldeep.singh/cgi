
#ifndef __WIDGET_IMAGE_H
#define __WIDGET_IMAGE_H

#include <stdio.h>
#include "common.h"
#include "widget.h"


class Image : public Widget
{
	public:
		Image(string name, string text);

		void Show(void);

	protected:
		void ReadSetting(void) {};
		void ApplySetting(void) {};
		string m_filename;
};


#endif
