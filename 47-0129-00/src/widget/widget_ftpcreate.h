
#ifndef __WIDGET_FTPCREATE_H
#define __WIDGET_FTPCREATE_H

#include <stdio.h>
#include "common.h"
#include "widget.h"


class FTPCreate : public Widget
{
	public:
		FTPCreate(string name, Settings *settings, string identifier);
		~FTPCreate(void) {}

		virtual void Show(void);
		virtual void Set(char **postvars);

		virtual void SetText(string text);
		virtual string GetText(void);

	protected:
		virtual void ReadSetting(void);
		virtual void ApplySetting(void);

		string m_text;
		int m_box_size;
		int m_max_length;
};


#endif