
#include "widget_file_select.h"

FileSelect::FileSelect(string name, string ref) : Widget(name, NULL, "")
{
	m_ref = ref;
	m_box_size = 20;
	m_max_length = 64;
}

void FileSelect::Show(void)
{
	printf("<tr>");
	printf("<td class=\"labelcell\" width=\"%d%%\">%s:</td>", m_left_proportion, m_name.c_str());
	printf("<td class=\"fieldcell\" width=\"%d%%\">", m_right_proportion);
	printf("<input type=\"file\" name=\"%s\" size=\"%d\">", m_ref.c_str(), m_box_size);
	printf("</td>");
	printf("</tr>");
}


