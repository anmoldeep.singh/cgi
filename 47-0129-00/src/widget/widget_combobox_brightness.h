
#ifndef __WIDGET_COMBOBOX_BRIGHTNESS_H
#define __WIDGET_COMBOBOX_BRIGHTNESS_H

#include "widget_combobox.h"


class ComboBoxBrightness : public ComboBox 
{
	public:
		ComboBoxBrightness(string name, Settings *settings, string ref_name, const char **string_list, int choices = 1);

		virtual void Set(char **postvars);
		virtual void ReadSetting(void);
		
	protected:
};

#endif
