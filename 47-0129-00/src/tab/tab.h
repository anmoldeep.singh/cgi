
#ifndef __TAB_H
#define __TAB_H

#include "section.h"
#include "common.h"

#define BUTTONS_NONE		0x00
#define BUTTON_SAVE		0x01
#define	BUTTON_APPLY		0x02
#define BUTTON_CANCEL		0x04
#define BUTTONS_ABOVE		0x40
#define BUTTONS_BELOW		0x80

#define DEFAULT_BUTTONS ( BUTTON_SAVE | BUTTON_APPLY | BUTTON_CANCEL | BUTTONS_ABOVE | BUTTONS_BELOW )

class Tab
{
	public:
		Tab(string name, string ref_name, string menu_name, int button_flags );
		virtual ~Tab(void) {}

		virtual void Head(void);
		virtual void Show(char **postvars);
		virtual void Apply(char **postvars);
		virtual string GetName(void);
		virtual string GetReference(void);

	protected:
		string m_name;
		string m_ref_name;
		string m_menu_name;
		//void ShowButtons(void);
		section_list_t sections;
		bool m_shown;
		int m_button_flags;
};


typedef list<Tab*> tab_list_t;

#endif
