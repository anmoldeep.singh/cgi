
#include "tab_backup_restore.h"
#include "section.h"
#include "section_aircraft_list_file.h"
#include "section_configuration_file.h"
#include "section_sslcertificate_file.h"
#include "section_custom_encryption_key.h"
#include "section_sftpkey_file.h"


BackupRestoreTab::BackupRestoreTab(string menu_name, bool expanded) : Tab(BACKUP_RESTORE_TAB, REF_BACKUP_RESTORE_TAB, menu_name, (BUTTON_APPLY | BUTTONS_BELOW | BUTTONS_ABOVE ) )
{
	sections.push_back(new AircraftListFile(menu_name, expanded));
	sections.push_back(new ConfigurationFile(menu_name, expanded));
	sections.push_back(new SSLCertificateFile(menu_name, expanded));
	sections.push_back(new CustomEncryptionKey(menu_name, expanded));
	sections.push_back(new SFTPKeyFile(menu_name, expanded));
}
