
#ifndef __TAB_LICENCE_H
#define __TAB_LICENCE_H

#include "tab.h"
#include "common.h"

#define REF_LICENCE_TAB	"licence"

class LicenceTab : public Tab
{
	public:
		LicenceTab(string menu_name, bool expanded = false);
};


#endif