
#ifndef __TAB_SERVICE_H
#define __TAB_SERVICE_H

#include "tab.h"
#include "common.h"

#define REF_SERVICE_TAB	"service"

class ServiceTab : public Tab
{
	public:
		ServiceTab(string menu_name, bool expanded = false);
};


#endif