
#ifndef __TAB_DEVICE_CONFIGURATION_H
#define __TAB_DEVICE_CONFIGURATION_H

#include "tab.h"
#include "common.h"

#define REF_DEVICE_CONFIGURATION_TAB	"dev_conf"

class DeviceConfigurationTab : public Tab
{
	public:
		DeviceConfigurationTab(string menu_name, bool expanded = false);
};


#endif