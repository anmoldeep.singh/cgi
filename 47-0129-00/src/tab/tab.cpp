
#include "tab.h"
#include "section.h"
#include "page.h"
#include "lists.h"

Tab::Tab(string name, string ref_name, string menu_name, int button_flags)
{
	m_name = name;
	m_ref_name = ref_name;
	m_menu_name = menu_name;
	m_shown = false;
	m_button_flags = button_flags;
}

void Tab::Head(void)
{
	Lists<Section> section_list(sections);
	Section *section = section_list.FirstItem();
	while(section)
	{
		section->Head();
		section = section_list.NextItem();
	}
}

void Tab::Show(char **postvars)
{
	printf("<tr>");
	printf("<td class=\"cell_dark_heading\">%s</td>", m_name.c_str());
	printf("<td class=\"cell_dark_buttons\" align=\"right\">");
	if(m_button_flags & BUTTONS_ABOVE)
	{
		if(m_button_flags & BUTTON_SAVE)
			printf("<input type=submit name=\"" REF_ACTION "\" value=\"%s\" class=\"buttons\">", BTN_ACTION_SAVE);
		if(m_button_flags & BUTTON_APPLY)
			printf("<input type=submit name=\"" REF_ACTION "\" value=\"%s\" class=\"buttons\">", BTN_ACTION_APPLY);
		if(m_button_flags & BUTTON_CANCEL)
			printf("<input type=submit name=\"" REF_ACTION "\" value=\"%s\" class=\"buttons\">", BTN_ACTION_CANCEL);
	}
	printf("</td>");
	printf("</tr>");
	printf("<tr><td colspan=\"2\"class=\"cell_dark\" valign=\"center\" align=\"center\">");
	printf("<div class=\"tree\">");
	printf("<ul>");

	Lists<Section> section_list(sections);
	Section *section = section_list.FirstItem();
	while(section)
	{
		section->Show(postvars);
		section = section_list.NextItem();
	}
	printf("</ul>");
	printf("</div>");
	printf("</td></tr>");
	
	printf("<td colspan=\"2\" class=\"cell_dark_buttons\" align=\"right\">");
	if(m_button_flags & BUTTONS_BELOW)
	{
		if(m_button_flags & BUTTON_SAVE)
			printf("<input type=submit name=\"" REF_ACTION "\" value=\"%s\" class=\"buttons\">", BTN_ACTION_SAVE);
		if(m_button_flags & BUTTON_APPLY)
			printf("<input type=submit name=\"" REF_ACTION "\" value=\"%s\" class=\"buttons\">", BTN_ACTION_APPLY);
		if(m_button_flags & BUTTON_CANCEL)
			printf("<input type=submit name=\"" REF_ACTION "\" value=\"%s\" class=\"buttons\">", BTN_ACTION_CANCEL);
	}
	printf("</td>");
	printf("</tr>");
}


void Tab::Apply(char **postvars)
{
	Lists<Section> section_list(sections);
	Section *section = section_list.FirstItem();
	while(section)
	{
		section->Apply(postvars);
		section = section_list.NextItem();
	}
}

string Tab::GetName(void)
{
	return m_name;
}

string Tab::GetReference(void)
{
	return m_ref_name;
}

