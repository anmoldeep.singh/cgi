
#ifndef __TAB_FILE_BROWSER_H
#define __TAB_FILE_BROWSER_H

#include "tab.h"
#include "common.h"

#define REF_FILE_BROWSER_TAB	"file_browser"

class FileBrowserTab : public Tab
{
	public:
		FileBrowserTab(string menu_name, bool expanded = false);
};


#endif