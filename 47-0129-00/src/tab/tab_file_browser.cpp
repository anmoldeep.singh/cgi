
#include "tab_file_browser.h"
#include "section_file_browser.h"
#include "section.h"
#include "system_functions.h"


FileBrowserTab::FileBrowserTab(string menu_name, bool expanded) : Tab(FILE_BROWSER_TAB, REF_FILE_BROWSER_TAB, menu_name, BUTTONS_NONE)
{
	if (system_functions.CheckMounted("/mnt/internal"))
		sections.push_back(new FileBrowser("Internal", "/mnt/internal", menu_name, expanded));

	if (system_functions.CheckMounted("/mnt/usbmem"))
		sections.push_back(new FileBrowser("USB Drive", "/mnt/usbmem", menu_name, expanded));

	if (system_functions.CheckMounted("/mnt/sdcard"))
		sections.push_back(new FileBrowser("SD Card", "/mnt/sdcard", menu_name, expanded));
}
