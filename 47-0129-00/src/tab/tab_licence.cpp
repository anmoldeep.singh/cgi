
#include "tab_licence.h"
#include "section.h"
#include "section_security_key.h"


LicenceTab::LicenceTab(string menu_name, bool expanded) : Tab(LICENCE_TAB, REF_LICENCE_TAB, menu_name, DEFAULT_BUTTONS)
{
	sections.push_back(new SecurityKey(menu_name, expanded));
}
