
#ifndef __TAB_REMOTE_H
#define __TAB_REMOTE_H

#include "tab.h"
#include "common.h"

#define REF_REMOTE_TAB	"remote"

class RemoteTab : public Tab
{
	public:
		RemoteTab(string menu_name, bool expanded = false);
};


#endif
