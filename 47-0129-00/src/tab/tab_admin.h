
#ifndef __TAB_ADMIN_H
#define __TAB_ADMIN_H

#include "tab.h"
#include "common.h"

#define REF_ADMIN_TAB	"admin"
#define ADMIN_BUTTONS ( BUTTON_SAVE | BUTTON_APPLY | BUTTON_CANCEL | BUTTONS_ABOVE )

class AdminTab : public Tab
{
	public:
		AdminTab(string menu_name, bool expanded = false);
};


#endif
