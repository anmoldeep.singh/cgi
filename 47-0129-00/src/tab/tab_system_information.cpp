
#include "tab_system_information.h"
#include "section.h"
#include "section_device_information.h"
#include "section_version_information.h"
#include "section_flash_variables.h"


SystemInformationTab::SystemInformationTab(string menu_name, bool expanded) : Tab(SYSTEM_INFORMATION_TAB, REF_SYSTEM_INFORMATION_TAB, menu_name, BUTTONS_NONE)
{
	sections.push_back(new DeviceInformation(menu_name, expanded));
	sections.push_back(new VersionInformation(menu_name, expanded));
}
