
#include "tab_remote.h"
#include "section.h"
#include "section_remote.h"


RemoteTab::RemoteTab(string menu_name, bool expanded) : Tab(REMOTE_TAB, REF_REMOTE_TAB, menu_name, BUTTONS_NONE)
{
	sections.push_back(new Remote(menu_name, expanded));
}
