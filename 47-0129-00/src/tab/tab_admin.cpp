
#include "tab_admin.h"
#include "section.h"
#include "section_owner_information.h"
#include "section_access.h"
#include "section_users.h"
#include "section_tools.h"
#include "section_boot_variables.h"

AdminTab::AdminTab(string menu_name, bool expanded) : Tab(ADMIN_TAB, REF_ADMIN_TAB, menu_name, ADMIN_BUTTONS)
{
	sections.push_back(new OwnerInformation(menu_name, expanded));
	sections.push_back(new AccessRestrictions(menu_name, expanded));
	sections.push_back(new Users(menu_name, expanded));
	sections.push_back(new Tools(menu_name, expanded));
//	sections.push_back(new BootVariables(menu_name, expanded));
}
