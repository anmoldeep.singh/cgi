
#ifndef __TAB_BACKUP_RESTORE_H
#define __TAB_BACKUP_RESTORE_H

#include "tab.h"
#include "common.h"

#define REF_BACKUP_RESTORE_TAB	"bkup"

class BackupRestoreTab : public Tab
{
	public:
		BackupRestoreTab(string menu_name, bool expanded = false);
};


#endif