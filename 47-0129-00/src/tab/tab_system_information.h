
#ifndef __TAB_SYSTEM_INFORMATION_H
#define __TAB_SYSTEM_INFORMATION_H

#include "tab.h"
#include "common.h"

#define REF_SYSTEM_INFORMATION_TAB	"sys_info"

class SystemInformationTab : public Tab
{
	public:
		SystemInformationTab(string menu_name, bool expanded = false);
};


#endif