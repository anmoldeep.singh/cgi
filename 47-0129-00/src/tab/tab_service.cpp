
#include "tab_service.h"
#include "section.h"
#include "section_oem_options.h"
#include "section_recorder_support.h"
#include "section_software_options.h"
#include "section_hardware_options.h"
#include "section_version_variables.h"
#include "section_env_variables.h"
#include "section_file_encryption.h"


ServiceTab::ServiceTab(string menu_name, bool expanded) : Tab(SERVICE_TAB, REF_SERVICE_TAB, menu_name, DEFAULT_BUTTONS)
{
	sections.push_back(new OEMOptions(menu_name, expanded));
	sections.push_back(new RecorderSupport(menu_name, expanded));
	sections.push_back(new SoftwareOptions(menu_name, expanded));
	sections.push_back(new FileEncryption(menu_name, expanded));
	sections.push_back(new HardwareOptions(menu_name, expanded));
	sections.push_back(new VersionVariables(menu_name, expanded));
}
