
#include "tab_device_configuration.h"
#include "section.h"
#include "section_aircraft_settings.h"
#include "section_clock_settings.h"
#include "section_control_settings.h"
#include "section_display_settings.h"
#include "section_auto_upload_settings.h"
#include "section_auto_update_settings.h"
#include "section_memory_settings.h"
#include "section_media_download_settings.h"
#include "section_file_settings.h"
#include "section_network_settings.h"
#include "section_wired_settings.h"
#include "section_wireless_settings.h"
#include "section_usb_settings.h"
#include "section_live_data_settings_717.h"
#include "section_live_data_settings_429_standard.h"
#include "section_live_data_settings_429_periodic.h"
#include "section_live_data_settings_dlr.h"
#include "section_shortcut_key_settings.h"

DeviceConfigurationTab::DeviceConfigurationTab(string menu_name, bool expanded) : Tab(DEVICE_CONFIGURATION_TAB, REF_DEVICE_CONFIGURATION_TAB, menu_name, DEFAULT_BUTTONS)
{
	sections.push_back(new AircraftSettings(menu_name, expanded));
	sections.push_back(new AutoUploadSettings(menu_name, expanded));
	sections.push_back(new AutoUpdateSettings(menu_name, expanded));
	sections.push_back(new ClockSettings(menu_name, expanded));
//	sections.push_back(new ControlSettings(menu_name, expanded));
	sections.push_back(new DisplaySettings(menu_name, expanded));
	sections.push_back(new MemorySettings(menu_name, expanded));
	sections.push_back(new MediaDownloadSettings(menu_name, expanded));
	sections.push_back(new FileSettings(menu_name, expanded));
	sections.push_back(new NetworkSettings(menu_name, expanded));
	sections.push_back(new WiredSettings(menu_name, expanded));
	sections.push_back(new WirelessSettings(menu_name, expanded));
//	sections.push_back(new UsbSettings(menu_name, expanded));
	sections.push_back(new LiveDataSettings717(menu_name, expanded));
	sections.push_back(new LiveDataSettings429Standard(menu_name, expanded));
	sections.push_back(new LiveDataSettings429Periodic(menu_name, expanded));
	sections.push_back(new LiveDataSettingsDLR(menu_name, expanded));
	sections.push_back(new ShortcutKeySettings(menu_name, expanded));
}
