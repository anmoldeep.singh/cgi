
#include "access.h"

Access _access;

#define ACCESS_SESSION_TIMEOUT	(5*60)	// session timeout in seconds

string RandomStr(const int len)
{
	static const char alphanum[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	string s;
	srand(time(NULL));

	for (int i = 0; i < len; i++)
		s += alphanum[rand() % (sizeof(alphanum) - 1)];

	return s;
}


Access::Access(void)
{
	m_logged_in = false;
	m_attempted = false;
	m_username = "";
	m_password = "";
	admin_password = Settings::Instance().String("security.web_password");
	m_session_id = "";
}

void Access::SetUser(string username)
{
	m_username = username;
}

bool Access::Login(string username, string password)
{
	Settings::Instance().Load(ALL_SECTIONS);
	
	m_username = username;
	m_password = password;
	m_attempted = true;

	if ((username == "admin" && password == admin_password ) || (username == "fdsservice" && password == "gohibbal") )  //|| ( username == "root" && password == "pwpadmin" ))
	{
		m_attempted = false;
		CreateSessionID();
	}
	else
	{
		m_session_id = "";
		remove("/tmp/hhmpi_cgi.session");
	}

	return LoggedIn();
}

void Access::Logout(void)
{
	m_logged_in = false;
	m_attempted = false;
	m_username = "";
	m_password = "";
	m_session_id = "";
	remove("/tmp/hhmpi_cgi.session");
}

void Access::CreateSessionID(void)
{
	m_session_id = RandomStr(20); 
       
	FILE *fout = fopen("/tmp/hhmpi_cgi.session", "wb");

	if (fout)
	{       
		fprintf(fout, "%s\n", m_session_id.c_str());
		fprintf(fout, "%lu\n", (unsigned long)time(NULL));
		fclose(fout);
	}
}

void Access::UpdateSession(void)
{
	FILE *fin = fopen("/tmp/hhmpi_cgi.session", "rb");
	char scratch[128];
	unsigned long timestamp = 0;

	if (!fin)
		return;

	fscanf(fin, "%s\n", scratch);
	fscanf(fin, "%lu\n", &timestamp);
	fclose(fin);

	FILE *fout = fopen("/tmp/hhmpi_cgi.session", "wb");

	if (!fout)
		return;

	fprintf(fout, "%s\n", m_session_id.c_str());
	fprintf(fout, "%lu\n", (unsigned long)time(NULL));
	fclose(fout);
}

bool Access::CheckSessionID(string session_id)
{
	FILE *fin = fopen("/tmp/hhmpi_cgi.session", "rb");
	char scratch[128];
	unsigned long timestamp = 0;

	if (fin)
	{ 
		fscanf(fin, "%s\n", scratch);
		fscanf(fin, "%lu\n", &timestamp);
		fclose(fin);

		if (strcmp(m_session_id.c_str(), scratch) == 0 &&
			(unsigned long)time(NULL) - timestamp < ACCESS_SESSION_TIMEOUT)
			return true;
	}

	return false;
}

void Access::SetSessionID(string session_id)
{
	m_session_id = session_id;
}

bool Access::LoggedIn(void)
{
	return CheckSessionID(m_session_id);
}

bool Access::Attempted(void)
{
	return m_attempted;
}

string Access::GetUsername(void)
{
	return m_username;
}

string Access::GetPassword(void)
{
	return m_password;
}

string Access::GetSessionID(void)
{
	return m_session_id;
}

